<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	 
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );

	?></title>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/favicon.ico" />
<link href='https://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>

<?php if ($_SERVER['HTTP_HOST'] === 'dev.marketplace.xnat.org') :  	// dev site settings ?>
    <!-- google site verification link -->
<?php else : // live site settings ?>
    <meta name="google-site-verification" content="RVQOJ9qEDiX-tmJmTleCyBUznCCZ6KQly4-iQ7C4aFs" />
<?php endif; ?>

    <!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
	
?>
</head>
<body <?php body_class(); ?>>
<div id="wrapper">
<div id="page" class="hfeed">
	<header id="branding" role="banner">
			<hgroup>
				<div id="logo">
				    <a href="<?php echo home_url(); ?>" title="<?php bloginfo('description'); ?>">	       		     
	       		    <img src="<?php echo get_template_directory_uri(); ?>/images/xnat_logo.png" alt="<?php bloginfo('name'); ?>"/></a>
				</div> 	
				
				  <div id="top-menu">	
				    <!--#menu -->
						<ul id="menu">
                            <li><a href="javascript:" id="myCustomTrigger" style="color: #c00;">Find a Bug?</a></li>
                            <li><span> | </span></li>
                            <li><a href="/faq/">FAQ</a></li>
                            <li><span> | </span></li>
                            <li><a href="/contact-us/">Contact Us</a></li>
																	
								
						</ul>
					<!--end #menu-->
					
					<div id="register_login">
					 <div id="login_icon"><img src="<?php echo get_template_directory_uri();?>/images/user_icon.png"></div>
							<?php
							if ( is_user_logged_in() ) { ?>						
								
								<ul id="logged_status">
									<li><a class="userlink" href="/wp-admin/admin.php?page=xnat-welcome"><?php echo possessive(theusername()) . ' Dashboard'; ?></a></li>
									<li><span>|</span></li>	
									<li><a href="<?php echo wp_nonce_url( home_url().'/wp-login.php?action=logout&redirect_to='.home_url(), 'log-out' ); ?>">Log Out</a></li>	
									
									
								</ul>
							
							<?php
								
							} else { ?>							
								
								<ul id="logged_status">
									<li><div id="registration"><a href="<?php echo home_url().'/wp-register.php'; ?>">Register</a></div> </li>
									<li><span>|</span></li>	
									<li><a href="<?php echo home_url().'/wp-login.php?redirect_to='.urlencode(home_url().$_SERVER['REQUEST_URI']); ?>" title="Login">Login</a></li>
								</ul>
								<?php
											}
							?>
						</div> <!--#register_login-->		
						</div>
				</hgroup>
								
			

			<?php
				// Check to see if the header image has been removed
				$header_image = get_header_image();
				if ( ! empty( $header_image ) ) :
			?>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<?php
					// The header image
					// Check if this is a post or page, if it has a thumbnail, and if it's a big one
					if ( is_singular() &&
							has_post_thumbnail( $post->ID ) &&
							( /* $src, $width, $height */ $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array( HEADER_IMAGE_WIDTH, HEADER_IMAGE_WIDTH ) ) ) &&
							$image[1] >= HEADER_IMAGE_WIDTH ) :
						// Houston, we have a new header image!
					
					else : ?>
					
				<?php endif; // end check for featured image or standard header ?>
			</a>
			<?php endif; // end check for removed header image ?>

			<?php
				// Has the text been hidden?
				if ( 'blank' == get_header_textcolor() ) :
			?>
				<div class="only-search<?php if ( ! empty( $header_image ) ) : ?> with-image<?php endif; ?>">
				<?php get_search_form(); ?>
				</div>
			<?php
				else :
			?>
				<?php get_search_form(); ?>
			<?php endif; ?>

			
	</header><!-- #branding -->


	<div id="main">
	<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
