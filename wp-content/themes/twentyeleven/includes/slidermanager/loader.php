<?php

define('MANAGER_URI', get_bloginfo('stylesheet_directory') . '/functions/slidermanager');

add_action('admin_menu', 'manager_admin_menu');
add_action('admin_init', 'manager_init');

global $slides;

if(get_option('slides')) {
	$slides = get_option('slides');
} else {
	$slides = false;	
}

// admin menu
function manager_admin_menu() {
global $themename, $shortname, $options;
	
	if(isset($_GET['page']) && $_GET['page'] == 'slidermanager') {
		
		if(isset($_POST['action']) && $_POST['action'] == 'save') {
			
			$slides = array();
			
			foreach($_POST['src'] as $k => $v) {
				$slides[] = array(
					'src' => $v,
					'link' => $_POST['link'][$k],
					'title' => $_POST['title'][$k], 
					'caption' => $_POST['caption'][$k]
				);
			}
			
			update_option('slides', $slides);
			
		}
		
	}
	
	if(function_exists('add_object_page')) {
		add_submenu_page('custom-admin-menu.php', 'Slider Image Manager','Slider Image Manager', 'edit_themes', 'slidermanager', 'manager_wrap');		
	}
	else {
		add_menu_page('Slider Image Manager', 'Slider Image Manager', 'edit_themes', 'slidermanager', 'manager_wrap');
	}	
	
}


// slider manager wrapper
function manager_wrap() {
	global $slides;
	
?>

	<div class="wrap" id="manager_wrap">
	
		<h2>Slider Manager</h2>
		
		<form action="" id="manager_form" method="post">
		
			<ul id="manager_form_wrap">
			
			<?php if(get_option('slides')) : ?>
				
				<?php foreach($slides as $k => $slide) : ?>
			
				
				<li class="slide">
					
					<label>Image Source <span>(required)</span></label>
					<input type="text" name="src[]" class="slide_src upload_field" value="<?php echo $slide['src'] ?>">
					<button class="upload_button"> Browse </button>
					
					<label>Image Link</label>
					<input type="text" name="link[]" id="slide_link" value="<?php echo $slide['link'] ?>">
					
					<label>Image Title</label>
					<input type="text" name="title[]" id="slide_title" value="<?php echo $slide['title'] ?>">

					
					<label>Image Caption</label>
					<textarea name="caption[]" cols="20" rows="2" class="slide_caption"><?php echo $slide['caption'] ?></textarea>
					
					<button class="remove_slide button-secondary">Remove This Slide</button>
					
				</li>
				
				<?php endforeach; ?>
				
			<?php else : ?>
			
				<li class="slide">
					
					<label>Image Source <span>(required)</span></label>
										
					<input type="text" name="src[]" class="upload_field slide_src" value="" />
					<button class="upload_button"> Browse </button>
					
					<label>Image Link</label>
					<input type="text" name="link[]" id="slide_link">
					
					<label>Slide Title</label>
					<input type="text" name="title[]" id="slide_title">
					
					<label>Image Caption</label>
					<textarea name="caption[]" cols="20" rows="2" class="slide_caption"></textarea>
					
					<button class="remove_slide button-secondary">Remove This Slide</button>
					
				</li>
				
			<?php endif; ?>
			
			</ul>
			
			<input type="submit" value="Save Changes" id="manager_submit" class="button-primary">
			<input type="hidden" name="action" value="save">
			
		</form>
		
	</div>

<?php
	
}


// slider manager init
function manager_init() {
	
	if(isset($_GET['page']) && $_GET['page'] == 'slidermanager') {
	
		// scripts
		wp_enqueue_script('jquery-ui-core');
		wp_enqueue_script('jquery-ui-sortable');
		wp_enqueue_script('jquery-appendo', MANAGER_URI . '/js/jquery.appendo.js', false, '1.0', false);
		wp_enqueue_script('slider-manager', MANAGER_URI . '/js/manager.js', false, '1.0', false);
		
		// styles
		wp_enqueue_style('slider-manager', MANAGER_URI . '/css/manager.css', false, '1.0', 'all');
		
	}

}

?>