jQuery(function($) {
  
  if ($('#screen_captures,#add_docs,#add_icon,#default_icon').length > 0) {
      $('form').attr('enctype', 'multipart/form-data');
  }
	
	 /*******************************************************************
/	jQuery for the user selected icon
/*******************************************************************/
	
	
	$('#default-iconset ul li img').click(function(){	
		var theimg = $(this);	
		var imgval = $(this).attr('src');
		var imgtitle = $(this).attr('title');
		$('#default-iconset ul li').removeClass('selected');
		$(theimg).parent().addClass('selected');	
		$('#default_icon').val(imgval);
		$.uniform.update($("#add_icon").val(imgval));
		$.uniform.update($("#uniform-add_icon .filename").html(imgtitle +'.png'));
	});
	
	
	$("#add_icon").click(function(){
		$('#default-iconset ul li').removeClass('selected');
		$.uniform.update("#add_icon");
		$.uniform.update("#uniform-add_icon .filename");
	});
  
  //hide the quicktags toolbar when editing plugins
  $('.quicktags-toolbar').hide();
		
	//remove "Personal Options" from user profiles
  $('form#your-profile > h3:first').hide();
  $('form#your-profile > table:first').hide();
  $('form#your-profile').show();
  
  //and hide some other fields that we don't like
  var hideFields = ["url", "avatar"];
  $.each($("form#your-profile tr"), function() {
  	var field = $(this).find("input, textarea, select").attr("id");
  	if (hideFields.indexOf(field) != -1) {
  		jQuery(this).remove();
  	}
  });
  
  //hide some admin bar items
  $('#wp-admin-bar-comments').hide();
  $('#wp-admin-bar-new-content').hide();

});