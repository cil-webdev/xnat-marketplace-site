<?php

add_filter( 'manage_posts_columns', 'create_columns' );
add_action( 'manage_plugin_posts_custom_column', 'add_custom_column_meta', 10, 2);


 
function create_columns($defaults) {
    $defaults['status'] = __('Post Status');
	
    return $defaults;
	}  

     
 
function add_custom_column_meta($column_name, $post_id) {
  global $wpdb,$post;
    $taxonomy = $column_name;
	$post_type = get_post_type($post_id);
    $terms = get_the_terms($post_id, $taxonomy);
	 $custom_fields = get_post_custom( $post_id );
	           
            if ("status" == $column_name){
                echo '<div id="psatus">';
                switch ($post->post_status) {
                    case 'publish':
                        echo '<a href="#" class="pb" change_to="pending" pid="'.$post_id.'">Published</a>';
                        break;
                    case 'draft':
                        echo '<a href="#" class="pb" change_to="publish" pid="'.$post_id.'">Draft</a>';
                        break;
                    case 'pending':
                        echo '<a href="#" class="pb" change_to="publish" pid="'.$post_id.'">Pending</a>';
                        break;
                    default:
                        echo 'unknown';
                        break;
                } // end switch
                echo '</div>';
            }	
	
}

add_action('quick_edit_custom_box', 'show_quickedit_meta', 10, 2);
add_action('edit_post','save_quickedit_metabox', 10, 3);
add_action('admin_head-edit.php', 'update_quickedit_values');
//add_action('bulk_edit_custom_box', 'ilc_quickedit_bulk', 10, 2);



function show_quickedit_meta( $col, $type ) {	
	if ("column-meta-2" == $col){
	?>
	<fieldset class="inline-edit-col-left featured-sliderpost-col-right">
		<div class="inline-edit-col">
			<div class="inline-edit-group">		
			<label for="_featured" style="font: italic 12px Georgia, serif;">Featured Homepage Plugin</label>		
			<span class="input-text-wrap">
			<p style="text-align:center"><input type="checkbox" name="_featured" id="featured" size="10" value="featured"></p>		
			</span>
			<input type="hidden" name="is_quickedit" value="true" />	
		</div>
		</div>
		
	</fieldset>
	<?php break;
	
	}
}



/* Not in use per client - MAY 4/5/12

function show_quickedit_bulk_meta( $col, $type ) {
switch ($col) {
        case 'featured' : ?>
<fieldset class="inline-edit-col-left">
<div class="inline-edit-col">
	<div class="inline-edit-group">
		<label for="featuredbulk" style="font: italic 12px Georgia, serif;">Featured Blog Post</label>
		<span class="input-text-wrap">
			<input type="checkbox" name="eventdatebulk" id="featuredbulk" size="10" value="">
		</span>
	</div>
</div>
<p class="featured-updated">Featured Post Updated</p>
<br/><a href="#" class="featured-bulk-update button-secondary">Update Featured Blog Posts</a>
</fieldset>
<?php break;
	 case 'hfeatured' : ?> 
<fieldset class="inline-edit-col-left">
<div class="inline-edit-col">
	<div class="inline-edit-group">
		<label for="hfeaturedbulk" style="font: italic 12px Georgia, serif;">Featured on Homepage </label>
		<span class="input-text-wrap">
			<input type="checkbox" name="hfeaturedbulk" id="hfeaturedbulk" size="10" value="">
		</span>
	</div>
</div>
<p class="featured-updated">Featured Homepage</p>
<br/><a href="#" class="featured-bulk-update button-secondary">Update Featured Homepage Posts </a>
</fieldset>
<?php 
	break;
	}
}*/	

function save_quickedit_metabox($post_id, $post) {
	if( $post->post_type != 'plugin' ) return;
	if (isset($_POST['is_quickedit']))
		update_post_meta($post_id, '_featured', $_POST['_featured']);
		//update_post_meta($post_id, '_hfeatured', $_POST['_hfeatured']);
}


function update_quickedit_values() {?>
   <script type="text/javascript">
   jQuery(document).ready(function() {
        jQuery(".editinline").live("click", function() {        
		var tag_id = jQuery(this).parents('tr').attr('id');
		var featured_item = jQuery(this).parentsUntil('.iedit').find(".featured .featured-text").text();		
		var hfeatured_item = jQuery(this).parentsUntil('.iedit').find(".hfeatured .hfeatured-text").text();		
		 var featured = jQuery('.featured .featured-text', '#'+tag_id).text();  
        var hfeatured = jQuery('.hfeatured .hfeatured-text', '#'+tag_id).text();  
		if(featured == 'Featured Blog Post') {
        jQuery(':input[name="_featured"]', '.inline-edit-row').attr("checked",true); }
		else {jQuery(':input[name="_featured"]', '.inline-edit-row').attr("checked",false); }
		if(hfeatured == 'Featured Homepage Slide') {
        jQuery(':input[name="_hfeatured"]', '.inline-edit-row').attr("checked","true"); }
		else { jQuery(':input[name="_hfeatured"]', '.inline-edit-row').attr("checked",false); }
        return false;  
	});
	});
	</script>
	<?php
}

?>