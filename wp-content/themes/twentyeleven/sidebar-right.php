<?php 
//query for nrg tagged items
   $d = 0;
  $totalnrg = array(                      
			'posts_per_page' => -1,
			'post_type' => 'plugin',		
            'tag' => 'nrg'    
			);		
  $totalnrgcount = query_posts($totalnrg);
  
   if ( $totalnrgcount ) :  				
		while ( have_posts() ) : the_post(); 
		   $d++;		  
			endwhile; endif;			 
	        wp_reset_query(); 


?>

<?php 
//query for condor tagged items  
	$e = 0;
  $totalcondor = array(                      
			'posts_per_page' => -1,
			'post_type' => 'plugin',			
            'tag' => 'condor'    
			);		
  $totalcondorcount = query_posts($totalcondor);
  
   if ( $totalcondorcount ) :  				
		while ( have_posts() ) : the_post(); 
		   $e++;		  
			endwhile; endif;			 
	        wp_reset_query();

?>

<?php 
//query for neuro tagged items
	$f = 0;
  $totalneuro = array(                      
			'posts_per_page' => -1,
			'post_type' => 'plugin',			
            'tag' => 'neurology'    
			);		
  $totalneurocount = query_posts($totalneuro);
  
   if ( $totalneurocount ) :  				
		while ( have_posts() ) : the_post(); 
		   $f++;		  
			endwhile; endif;			 
	        wp_reset_query();

?>

<?php 
//query for 
	$g = 0;
  $totalcancer = array(                      
			'posts_per_page' => -1,
			'post_type' => 'plugin',			
            'tag' => 'cancer'    
			);		
  $totalcancercount = query_posts($totalcancer);
  
   if ( $totalcancercount ) :  				
		while ( have_posts() ) : the_post(); 
		   $g++;		  
			endwhile; endif;			 
	        wp_reset_query();

?>
<?php

	$args_list_package = array(
			'taxonomy' => 'packages', 		
			'show_count' => true,
			'hierarchical' => false,
			'order' => 'DESC',
			'hide_empty' => false,
			'echo' => '1',	
			'exclude' => '13'
			
		);
		
		$args_list_package_two = array(
			'taxonomy' => 'packages', // Registered tax name			
			'show_count' => true,
			'hierarchical' => false,
			'hide_empty' => false,
			'echo' => '1',
			'include' =>  '13'
		);	 
?>

 <div id="sidebar_right">     
	  <h3>Packages</h3>	
	 <div class="sidebar_divider"></div> 
		<ul id="packages">	
		
		<?php $thepackages_two = get_categories($args_list_package_two);
			
			 foreach($thepackages_two as $package_two) { 	
			 	$category_id = $package_two->cat_ID;				
				echo '<li><a href="'. get_term_link($package_two->slug, 'packages').'">'. $package_two->name . ' ('.$package_two->count.') <img src="'. get_template_directory_uri().'/images/approved.png"> </a> </li>';
			 } ?>

		<?php $thepackages = get_categories($args_list_package);
			
			 foreach($thepackages as $package) { 	
			 	$category_id = $package->cat_ID;				
				// &nbsp('.$package->count.')
			 	echo '<li><a href="'. get_term_link($package->slug, 'packages').'">'. $package->name . ' </a> </li>';
			} ?>
			
			
			
		</ul>
			</div>