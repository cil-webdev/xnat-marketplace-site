<?php
/**
 * Template Name: Custom Default Page
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */
 get_header(); ?>
 
 <div id="primary">
		<!-- this is the left sidebar-->

		<?php include('sidebar-left.php'); ?>
			<div id="content" role="main">
			  <div id="content-container">
			<?php the_post(); ?>
			
				<?php
				//PULL POSTS FROM THE CANCER GROUP
				if (is_page (array( 252 , 'cancer' ))) { ?>
				<h1>Cancer</h1>
				<div class="intro-box">
					<img src="<?php echo get_template_directory_uri(); ?>/images/info_icon.png" alt=""/>
					<div>
						<h3>About This Package</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sapien nisi vehicula lectus, ac commodo nisi ipsum nec magna. Duis vitae neque elit, eget sollicitudin ipsum. Vestibulum nec porttitor lectus. Aenean id sapien et tellus commodo elementum varius dapibus odio. Sed sapien elit, sodales non pretium sit amet, laoreet eget justo. Quisque consequat eros quis elit accumsan sit amet interdum quam tristique. Morbi mattis tempor arcu, vitae imperdiet nulla scelerisque commodo.</p>
					</div>
				</div>
				<?php $acs->query_posts(array('group_name' => 'cancer')); 
					while ( have_posts() ) : the_post(); ?>	
						<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						  <h6 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
						  <?php the_title(); ?></a></h6>
							<div class="entry-content">
							<?php the_excerpt(); ?>							
							</div><!-- .entry-content -->

							</div><!-- #post-## -->
				<?php endwhile;  

    

				}
				//PULL POSTS FROM THE CONDOR GROUP
					if (is_page (array( 250 ,  'condor' ))) { ?>
					<h1>Condor</h1>
				<div class="intro-box">
					<img src="<?php echo get_template_directory_uri(); ?>/images/info_icon.png" alt=""/>
					<div>
						<h3>About This Package</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sapien nisi vehicula lectus, ac commodo nisi ipsum nec magna. Duis vitae neque elit, eget sollicitudin ipsum. Vestibulum nec porttitor lectus. Aenean id sapien et tellus commodo elementum varius dapibus odio. Sed sapien elit, sodales non pretium sit amet, laoreet eget justo. Quisque consequat eros quis elit accumsan sit amet interdum quam tristique. Morbi mattis tempor arcu, vitae imperdiet nulla scelerisque commodo.</p>
					</div>
				</div>
				<?php $acs->query_posts(array('group_name' => 'Condor'));
					while ( have_posts() ) : the_post(); ?>	
						<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						  <h6 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
						  <?php the_title(); ?></a></h6>
							<div class="entry-content">
							<?php the_excerpt(); ?>							
							</div><!-- .entry-content -->

							</div><!-- #post-## -->
				<?php endwhile;  
				}
				//PULL POSTS FROM THE ENDORSED GROUP
					if (is_page (array( 245,'endorsed' ))) { ?>
					<h1>Endorsed</h1>
				<div class="intro-box">
					<img src="<?php echo get_template_directory_uri(); ?>/images/info_icon.png" alt=""/>
					<div>
						<h3>About This Package</h3>
						 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sapien nisi vehicula lectus, ac commodo nisi ipsum nec magna. Duis vitae neque elit, eget sollicitudin ipsum. Vestibulum nec porttitor lectus. Aenean id sapien et tellus commodo elementum varius dapibus odio. Sed sapien elit, sodales non pretium sit amet, laoreet eget justo. Quisque consequat eros quis elit accumsan sit amet interdum quam tristique. Morbi mattis tempor arcu, vitae imperdiet nulla scelerisque commodo.</p>
					</div>
				</div>
				<?php $acs->query_posts(array('group_name' => 'Endorsed by NRG'));
					while ( have_posts() ) : the_post(); ?>	
						<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						  <h6 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
						  <?php the_title(); ?></a></h6>
							<div class="entry-content">
							<?php the_excerpt(); ?>							
							</div><!-- .entry-content -->

							</div><!-- #post-## -->
				<?php endwhile;  
				}
				//PULL POSTS FROM NEUROLOGY				
					if (is_page (array( 254, 'neurology' ))) { ?>
					<h1>Neurology</h1>
				<div class="intro-box">
					<img src="<?php echo get_template_directory_uri(); ?>/images/info_icon.png" alt=""/>
					<div>
						<h3>About This Package</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sapien nisi vehicula lectus, ac commodo nisi ipsum nec magna. Duis vitae neque elit, eget sollicitudin ipsum. Vestibulum nec porttitor lectus. Aenean id sapien et tellus commodo elementum varius dapibus odio. Sed sapien elit, sodales non pretium sit amet, laoreet eget justo. Quisque consequat eros quis elit accumsan sit amet interdum quam tristique. Morbi mattis tempor arcu, vitae imperdiet nulla scelerisque commodo.</p>
					</div>
				</div>
				<?php $acs->query_posts(array('group_name' => 'Neurology'));	
					while ( have_posts() ) : the_post(); ?>	
						<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						  <h6 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
						  <?php the_title(); ?></a></h6>
							<div class="entry-content">
							<?php the_excerpt(); ?>							
							</div><!-- .entry-content -->

							</div><!-- #post-## -->
				<?php endwhile;  
				//CATCHATLL
				
				}   else {
				 get_template_part( 'content', 'page' ); 
				} ?>
			
				</div><!-- #content-container-->
			</div><!-- #content -->
			<!-- this is the left sidebar-->

		<?php include('sidebar-right.php'); ?>			
		</div><!-- #primary -->
		
		<?php get_footer(); ?>