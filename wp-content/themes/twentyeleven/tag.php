<?php
/**
 * The template used to display Tag Archive pages
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
 
 global $query_string;
	$params = $_GET;
	unset($params['orderby']);
	unset($params['order']);		
	$url = get_pagenum_link();
	$url = strtok($url, '?');
	$url =$url.'?'.http_build_query($params);
	$i = 0;
	$z = 0;
	$tagj = 0;
	$tagk = 0;
	global $paged,$paged_two;
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$paged_two = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$bytype = $_GET['orderby'];
	
get_header(); ?>

<?php 

 // Perform query to display the number of returned posts
	$querytags1 = query_posts($query_string . '&plugins=admin,datatype,display,other,pipeline&posts_per_page=-1&post_type=plugin'); ?>

	
	<?php if ( $querytags1 ) : ?>    
				
		<?php while ( have_posts() ) : the_post(); ?>
			<?php $tagj++; ?>
		 <?php 	 
			endwhile; endif; ?>		
			 
	<?php wp_reset_query(); ?> 
	
	<?php 
	$querytags2 = query_posts($query_string . '&tools=apps,integration,other,scripts&posts_per_page=-1&post_type=plugin'); ?>

	
	<?php if ( $querytags2 ) : ?>    
				
		<?php while ( have_posts() ) : the_post(); ?>
			<?php $tagk++; ?>
		 <?php 	 
			endwhile; endif; ?>	
			<?php wp_reset_query(); ?> 
	<!-- end query -->

		<section id="primary">
		<!-- this is the left sidebar-->
		<?php include('sidebar-left.php'); ?>
			<div id="content" role="main">
           
			<?php if ( have_posts() ) : ?>

				<header class="page-header">
					<h1 class="page-title"><?php
						printf( __( 'Tag Archives for: %s', 'twentyeleven' ), '<span><b>' . single_tag_title( '', false ) . '</b></span>' );
					?></h1>

					<?php
						$tag_description = tag_description();
						if ( ! empty( $tag_description ) )
							echo apply_filters( 'tag_archive_meta', '<div class="tag-archive-meta">' . $tag_description . '</div>' );
					?>
				</header>
				<ul class="tabs">					
						<li><a href="#"  class="plugintab" data-id="Plugins" data-val="<?php  echo $tagj; ?>">Plugins (<?php  echo $tagj; ?>)</a></li>
						<li><a href="#"  class="tooltab" data-id="Tools" data-val="<?php  echo $tagk; ?>">Tools (<?php echo $tagk; ?>)</a></li>
				</ul>
				  <div class="tabsubmenu">	
					<div class="sort_by">Sort:  <a href="<?php echo $url;?>&orderby=date&order=DESC" <?php if($bytype =='date' || $bytype== '') {echo 'class="currentlyActive"';} ?>>By Date</a><span>|</span><a href="<?php echo $url ;?>&orderby=title&order=ASC" <?php if($bytype =='title') {echo 'class = "currentlyActive"';} ?> >By Name</a></div>							
					<div class="filter-tag">
					<label for="filter" class="filterlabel">Filter By:</label>					
					<select name='filter' id='filter-postform' class='postform'>	
					
					</select>	
					</div>
					<div class="nrg-filter">
					<input type="checkbox" class="nrgsorted" value="nrg-approved" name="nrgsorted">
					<label for="nrgsorted"> NRG Approved</label>
					</div>	
				  </div>
				  <div class="clear"></div>

				<?php				
				
				$query_tag_one = query_posts($query_string . '&plugins=admin,datatype,display,other,pipeline&posts_per_page=100&post_type=plugin&order=DESC&paged='.$paged); 
				
				?>
				<div class="search_results">
							<div>						
				<?php if ( $query_tag_one ) : ?>    
					
				<?php while ( have_posts() ) : the_post(); 
					
					
						/* Include the Post-Format-specific template for the content.
						 * If you want to overload this in a child theme then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'content', get_post_format() ); ?> 								
					
				 <?php endwhile; ?> 	
				
				<?php twentyeleven_content_nav( 'nav-below' ); ?>
						

			<?php else: ?>
				
				<p class="no-filter-results">Sorry, there are no matches for that selection.</p>
				
				
			<?php endif; ?></div>
			<?php wp_reset_query(); ?> 
			<!-- now query for the tools taxonomy -->
			
			<!---------------------------------->
			
			<?php 
				
			$query_tag_two = query_posts($query_string . '&tools=apps,integration,other,scripts&posts_per_page=100&post_type=plugin&order=DESC&paged='.$paged); ?>
				<div>
				
				<?php if ( $query_tag_two ) : ?>    
					
				<?php while ( have_posts() ) : the_post(); ?>
					
					
					<?php	/* Include the Post-Format-specific template for the content.
						 * If you want to overload this in a child theme then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'content', get_post_format() ); ?>					

				<?php endwhile; ?>
				<?php twentyeleven_content_nav( 'nav-below' ); ?>
						
					  
				
				
			

			<?php endif;?> </div>
			<?php wp_reset_query(); ?> 
			<?php endif;?>	
					
				</div>
			<?php if (! have_posts()) : ?>

				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Nothing Found', 'twentyeleven' ); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'twentyeleven' ); ?></p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</article><!-- #post-0 -->

			<?php endif; ?>

			</div><!-- #content -->
			<?php include('sidebar-right.php'); ?>	
		</section><!-- #primary -->

<!-- A quick hack to fix the filter by select box -->
<script type="text/javascript">
	jQuery(function(){
		jQuery('ul.tabs li a.current').click();
	});
</script>

<?php get_footer(); ?>
