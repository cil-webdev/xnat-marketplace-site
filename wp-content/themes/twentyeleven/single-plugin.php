<?php
/**
 * Template Name: single plugin posts page
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); 
 wp_enqueue_script( 'mosaic', get_template_directory_uri(). '/js/mosaic.1.0.1.min.js', array('jquery')); ?>
<?php wp_enqueue_script( 'colorbox', get_template_directory_uri() . '/js/jquery.colorbox-min.js/.js', array( 'jquery' )); ?>


		<div id="primary">		
			<!-- this is the left sidebar-->
		<?php include('sidebar-left.php'); ?>
				
			<div id="content" role="main">

				<?php while ( have_posts() ) : the_post(); ?>
               		<!-- main content-part -->	
					<?php get_template_part( 'content-plugin' ); ?>
					<!-- screenshot template part -->
					<?php get_template_part( 'screenshot', 'plugin' ); ?>	

					<?php  comments_template('', true); ?>					

				<?php endwhile; // end of the loop. ?>
				

			</div><!-- #content -->
			 <?php include('sidebar-right.php'); ?>				
		</div><!-- #primary -->
		
		
		<!-- this is the right sidebar-->

		
	  
	
<?php get_footer(); ?>