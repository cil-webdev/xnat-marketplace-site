<?php
/*
Template Name: Add Plugin Form
*/
?>
<?php
// Enqueue showcase script for the slider
//wp_enqueue_script('media-upload');
//wp_enqueue_script('thickbox');
//wp_enqueue_style('thickbox');

// if the user isn't logged in and still made it to this page by typing in the url then redirect them home
if ( !is_user_logged_in() ) {
    wp_redirect( home_url() ); exit;
}

if($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['action']) && $_POST['action'] == "new_post") {

    $error = array();
    // Do some minor form validation to make sure there is content
    if (isset ($_POST['title'])) {
        $title =  $_POST['title'];
    } else {
        $error[] = 'Please enter a name for this add-on';
    }
    if (($_POST['license']=="--Select--") && (strlen($_POST['license_custom']) == 0)) {
        $error[] = 'Please select a license to distribute this add-on under. (Default for XNAT is BSD)';
    }
    if (isset ($_POST['description']) && strlen($_POST['description']) > 0) {
        $description = $_POST['description'];
    } else {
        $error[] = 'Please enter a description of this add-on';
    }
    if (isset ($_POST['credits'])) {
        $credits = $_POST['credits'];
    } else {
        // $error[] = 'Please enter the plugin credits';
    }
    if (isset ($_POST['addon_category'])) {
        $category = $_POST['addon_category'];
    } else {
        $error[] = 'Please select a category';
    }
    if (isset ($_POST['subcategory'])) {
        $subcat = $_POST['subcategory'];
    } else {
        $error[] = 'Please select a subcategory';
    }
    if (isset ($_POST['add_icon'])) {
        $icon = $_POST['add_icon'];
    } else {
        if(isset($_POST['default_icon'])){
            $icon = $_POST['default_icon'];
        } else {
            $error[] = 'Please select an icon';
        }
    }
    if (isset ($_POST['compatible'])) {
        $compatible = $_POST['compatible'];
    } else {
        $error[] = 'Please select a compatibility version';
    }
    if (count($_POST['item']) == 0) {
        $error[] = 'Please list which XNAT features this add-on affects';
    }
    if (strlen($_FILES['upload_addon']['name']) == 0 && !isset($_POST['download_from_repo']) && !isset($_POST['repo']))	 {
        $error[] = 'Please select a file to upload or a repo to download from';
    }

    if(empty($error)){

        $tags = mysql_real_escape_string(implode(',',$_POST['post_tags']));
        $credits = stripslashes($_POST['credits']);
        $version = stripslashes($_POST['version']);
        $license = stripslashes($_POST['license']);
        $license_custom = stripslashes($_POST['license_custom']);
        //$compatible = stripslashes($_POST['compatible']);
        $compatible = mysql_real_escape_string(implode(',', $_POST['compatible']));
        $alpha = stripslashes($_POST['alpha']);
        if(isset($_FILES['add_icon'])) {
            $icon = $_FILES['add_icon'];
        }
        //$screencaptures = mysql_real_escape_string(implode(',',$_POST['screen_captures']));
        $documentation = stripslashes($_POST['documentation']);
        $repo = stripslashes($_POST['repo']);
        $downloadFromRepo = (isset($_POST['download_from_repo'])) ? "true" : false;
        $addon = stripslashes($_POST['upload_addon']);
        $upload_addon = stripslashes($_POST['upload_addon']);
        $environment = stripslashes($_POST['env_description']);
        $submit = stripslashes($_POST['submit-post']);
        $tax = $_POST['addon_category'];
        //$item = stripslashes($_POST['item']);
        $item = mysql_real_escape_string(implode(',', $_POST['item']));
        $term = mysql_real_escape_string(implode(',', $_POST['subcategory']));
        //$screenshots = mysql_real_escape_string(implode(',',$_FILE['screen_captures']));
        $screenshot_one = mysql_real_escape_string($_POST['screen_captures']);
        $screenshot_two = mysql_real_escape_string($_POST['screen_captures_two']);
        $screenshot_three = mysql_real_escape_string($_POST['screen_captures_three']);
        $upload = $_POST['upload_addon'];
        $defaulticon = $_POST['default_icon'];

        global $user_ID;

        // ADD THE FORM INPUT TO $new_post ARRAY
        $new_post = array(
            'post_title'	=>	$title,
            'post_tags'     =>  array($tags),
            'post_icon'     =>  $icon,
            'post_item'	    =>  array($item),
            'post_compatible' => array($compatible),
            'post_content'	=>	$description,
            'post_date' => date('Y-m-d H:i:s'),
            'post_author' => $user_ID,
            'post_credits'  =>  $credits,
            'post_environment'   =>  $environment,
            'post_version'  =>  $version,
            'post_alpha'    =>  $alpha,
            'tax_input'	=>	array($tax => $term),  // Usable for custom taxonomies too
            'tags_input'	=>	array($tags),
            'post_status'	=>	'draft',           // Choose: publish, preview, future, draft, etc.
            'comment_status' => 'open',
            'post_type'	=>	'plugin',  //'post',page' or use a custom post type if you want to
            'post_captures_one' => $_FILES['screen_captures'],
            'post_captures_two' => $_FILES['add_docs'],
            'post_captures_three' => $screenshot_three,
            'post_default_icon' => $defaulticon

        );

        //SAVE THE POST
        $pid = wp_insert_post($new_post);

        //KEEPS OUR COMMA SEPARATED TAGS AS INDIVIDUAL
        wp_set_post_tags($pid, $_POST['post_tags']);
        wp_set_post_terms($pid, $tax, $term, true);

        //REDIRECT TO THE NEW POST ON SAVE
        $link = get_permalink( $pid );
        wp_redirect( $link );


        //INSERT OUR MEDIA ATTACHMENTS
        if ($_FILES) {
            $i = 0;
            foreach ($_FILES as $file => $array) {
                $i++;
                $name = $i;
                $newupload = insert_attachment($file,$pid,$i);
                //set the title
                //update the attachment with the new title.

                // $newupload returns the attachment id of the file that
                // was just uploaded. Do whatever you want with that now.
            }
        }
        // END THE IF STATEMENT FOR FILES

        //New Statement for files

        /*if ( $_FILES['screen_captures'] ) {
            $files = $_FILES['screen_captures'];
            foreach ($files['name'] as $key => $value) {
                if ($files['name'][$key]) {
                    $file = array(
                        'name'     => $files['name'][$key],
                        'type'     => $files['type'][$key],
                        'tmp_name' => $files['tmp_name'][$key],
                        'error'    => $files['error'][$key],
                        'size'     => $files['size'][$key]
                    );

                    $_FILES = array("screen_captures" => $file);

                    foreach ($_FILES as $file => $array) {
                        $newupload = insert_attachment($file,$pid);
                    }
                }
            }
        }*/



        //ADD OUR CUSTOM FIELDS to our post type
        add_post_meta($pid, 'version', $version, true);
        add_post_meta($pid, 'license', $license, true);
        add_post_meta($pid, 'license_custom', $license_custom, true);
        add_post_meta($pid, 'credits', $credits, true);
        //add_post_meta($pid, 'post_tags', $tags, true);
        add_post_meta($pid, 'alpha', $alpha, true);
        add_post_meta($pid, 'add_icon', $icon, true);
        add_post_meta($pid, 'env_description', $environment, true);
        //add_post_meta($pid, 'screen_captures', $screenshots, true);
        //add_post_meta($pid, 'screen_captures_two', $screenshots_two, true);
        //add_post_meta($pid, 'screen_captures_three', $screenshots_three, true);
        add_post_meta($pid, 'documentation', $documentation, true);
        add_post_meta($pid, 'repo', $repo, true);
        add_post_meta($pid, 'download_from_repo', $downloadFromRepo, true);
        add_post_meta($pid, 'compatible', $compatible, true);
        //add_post_meta($pid, 'upload-addon', $upload, true);
        add_post_meta($pid, 'item', $item, true);
        add_post_meta($pid, 'tax', $tax, true);
        add_post_meta($pid, 'term', $term, true);
        add_post_meta($pid, 'defaulticon', $defaulticon, true);

    } // END of check for errors
    else{
//		echo $error;
//		die();
    }

} // END THE IF STATEMENT THAT STARTED THE WHOLE FORM

//POST THE POST YO
if ( is_user_logged_in() ) {
    do_action('wp_insert_post', 'wp_insert_post');
}

?>

<?php get_header(); ?>

    <div id="container">
        <!-- this is the left sidebar-->

        <?php include('sidebar-left.php'); ?>
        <div id="content" class="plugin_form" role="main">
            <!-- this is the main content area-->


            <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <?php
                    if (!empty($error)) :
                        ?>
                        <div id="the_form_errors"><h3><strong>The following errors must be corrected before continuing:</strong></h3>
                            <ul>
                                <?php
                                foreach ($error as $key => $value) :
                                    echo "<li>".$value."</li>";
                                endforeach;
                                ?>
                            </ul>
                            <p class="debug hidden">$_POST contents:
                                <?php
                                foreach ($_POST as $key => $value):
                                    echo "<span>(".$key.": ".$value.") </span>";
                                endforeach;
                                ?>
                            </p>
                            <p class="debug hidden">$_FILES contents:
                                <?php
                                foreach ($_FILES as $key => $value):
                                    echo "<span>(".$key.": ".$value.") </span>";
                                endforeach;
                                ?>
                            </p>
                        </div>
                        <?php
                    endif;
                    ?>
                    <?php if ( is_front_page() ) { ?>
                        <h3 class="entry-title"><?php the_title(); ?></h3>
                    <?php } else { ?>
                        <h1 id="addon-page-title"><?php the_title(); ?></h1>
                    <?php } ?>

                    <div class="form-content">
                        <?php the_content(); ?>

                        <div class="wpcf7 <?php if ($error) echo "error" ?>">

                            <p style="margin-bottom: 0;">*Required Fields</p>

                            <form id="new_post" name="new_post" method="post" action="#" class="wpcf7-form" enctype="multipart/form-data">
                                <!-- plugin name -->
                                <fieldset name="name">
                                    <label for="title" class="mainLabel">Title of Add-On* <span>(if multiple versions of your Add-On appear in the marketplace, include version in title)</span></label>
                                    <input type="text" class="longer required" id="title" value="<?php echo $_POST['title'] ?>" name="title" title="Please provided a title for the Add-On" />
                                </fieldset>

                                <!-- plugin version -->
                                <fieldset name="version">
                                    <label for="version" class="mainLabel">Add-On Version*</label>
                                    <input type="text" id="version" class="required" value="<?php echo $_POST['version'] ?>" name="version" title="Please specify a version" />
                                </fieldset>

                                <!-- license agreement -->
                                <fieldset name="license">
                                    <label for="license" class="mainLabel">License Agreement* <span>(Default for XNAT is BSD)</span></label>
                                    <select name="license" id="license" class="required" title="Please select a license agreement">
                                        <option>--Select--</option>
                                        <?php
                                        $licenseValArray = array("Apache License 2.0","BSD 2-Clause","BSD 3-Clause","GPL","LGPL","MIT","Mozilla Public License 2.0","CDDL","Eclipse Public License");
                                        $licenseNameArray = array ("Apache License 2.0","BSD 2-Clause \"Simplified\" or \"FreeBSD\"","BSD 3-Clause \"New\" or \"Revised\"","GNU General Public License (GPL)","GNU Library / \"Lesser\" General Public License (LGPL)","MIT License","Mozilla Public License 2.0","Common Development and Distribution License (CDDL)","Eclipse Public License");
                                        foreach ($licenseValArray as $key => $value ) :
                                            ?>
                                            <option value="<?php echo $value ?>" <?php if ($_POST['license'] == $value) echo "selected" ?>><?php echo $licenseNameArray[$key] ?></option>
                                            <?php
                                        endforeach;
                                        ?>
                                    </select>
                                    <label for="license_custom"><span>Or: Enter Custom License</span></label>
                                    <input type="text" name="license_custom" id="license_custom" class="longer" value="<?php echo $_POST['license_custom'] ?>" />
                                </fieldset>

                                <!-- Alpha/Beta -->
                                <fieldset class="alpha-beta" name="alpha" style="clear:both;">
                                    <label for="alpha">Release State</label>
                                    <select name='alpha' id='alpha' class=''>
                                        <option>--Select--</option>
                                        <?php
                                        $releaseStateArray = array("production","alpha","beta");
                                        foreach ($releaseStateArray as $key => $value) :
                                            ?>
                                            <option value="<?php echo $value ?>" <?php if ($_POST['alpha'] == $value) echo "selected" ?>><?php echo ucfirst($value) ?></option>
                                            <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </fieldset>

                                <!-- post Category -->
                                <fieldset class="category">
                                    <label for="addon_category" class="singlerow floatLabel">Select a Category*</label>
                                    <label for="plugin" class="normal_label qtip-tip floatLabel" rel="cornerValue">
                                        <input type="radio" class="radio" id="category_plugin" name="addon_category" value="plugins" validate="required:true" title="Please select a category" <?php if ($_POST['addon_category'] == "plugins") echo "checked" ?>/>Plugin <a href="javascript:">?<span> A plugin directly modifies the XNAT web application in some way (for example, a new UI element or a new data type that modifies the database structure).</span></a></label>
                                    <label for="tool" class="normal_label qtip-tip floatLabel" rel="cornerValue">
                                        <input type="radio"  class="radio" id="category_tool" name="addon_category" value="tools" <?php if ($_POST['addon_category'] == "tools") echo "checked" ?>/> Tool <a href="javascript:">?<span>A tool runs independently of the XNAT web application (for example an external DICOM handler or a command-line script), though it will likely access XNAT data in some fashion.</span></a></label>
                                </fieldset>

                                <!-- Sub-categories for plugin category -->
                                <fieldset id="subcategory-plugin" name="subcategory-plugin" <?php if ($_POST['addon_category'] != 'plugins') echo "style=\"display:none;\"" ?>>
                                    <label for="subcategory[]" style="width:100%;" class="mainLabel">Select one or more sub-categories*</label><br />
                                    <?php
                                    //cateogries code
                                    $categories = get_terms( 'plugins', 'orderby=ASC&hide_empty=0');
                                    foreach($categories as $category) :
                                        ?>
                                        <label for='<?php echo $category->name ?>' class='normal_label'>
                                            <input type='checkbox' name='subcategory[]' title="Please select at least 1 sub-category" value='<?php echo $category->term_id ?>' <?php if (in_array($category->term_id,$_POST['subcategory'])) echo "checked" ?>/><?php echo $category->name ?></label>
                                        <?php
                                    endforeach;
                                    ?>
                                </fieldset>

                                <!-- Sub-categories for tools category -->
                                <fieldset id="subcategory-tool" name="subcategory-tool" <?php if ($_POST['addon_category'] != 'tools') echo "style=\"display:none;\"" ?>>
                                    <label for="subcategory-tool" style="width:100%;" class="mainLabel">Select one or more sub-categories*</label><br />
                                    <?php //cateogries code
                                    $categoriestool = get_terms( 'tools', 'orderby=ASC&hide_empty=0');
                                    foreach($categoriestool as $category) :
                                        ?>
                                        <label for='<?php echo $category->name ?>' class='normal_label'>
                                            <input type='checkbox' name='subcategory[]' title=\"Please select at least 1 sub-category\" value='<?php echo $category->term_id ?>' <?php if (in_array($category->term_id,$_POST['subcategory'])) echo "checked" ?>/><?php echo $category->name ?></label>
                                        <?php
                                    endforeach;
                                    ?>
                                </fieldset>

                                <!-- post Content -->
                                <fieldset class="content">
                                    <label for="description" class="mainLabel">Please provide a description for this Add-On*</label>
                                    <textarea id="description" class="required" name="description" cols="80" rows="10" title="Please give a description of the Add-On"><?php echo $_POST['description'] ?></textarea>
                                </fieldset>

                                <!-- Credits -->
                                <fieldset class="credits">
                                    <label for="credits">Credits <span class="clarify">- (Enter names of others who helped build this Add-On)</span><span>(separate multiple names with a comma)</span></label>
                                    <input type="text" class="longest" value="<?php echo $_POST['credits'] ?>" class="required" id="credits" name="credits" />
                                </fieldset>

                                <!-- post Content -->
                                <fieldset class="environment_description">
                                    <label for="environment_description">Describe the environment where the Add-On is currently running:</label>
                                    <textarea id="env_description" class="" name="env_description" cols="80" rows="10"><?php echo $_POST['environment_description'] ?></textarea>
                                </fieldset>

                                <!-- post Tags -->
                                <fieldset class="tags">
                                    <label for="tags">Tags <span>(separate multiple names with a comma)</span></label>
                                    <input type="text" class="longest" id="tags" class="required" name="post_tags" value="<?php echo $_POST['tags'] ?>"/>

                                </fieldset>

                                <!-- post Screen Captures-->
                                <fieldset class="captures">
                                    <label class="fileupload floatLabel" for="screen_captures_one">Screen Captures <span class="clarify">(up to three)</span><span class="numbering">1.</span></label>
                                    <input type="file" class="text-input id screen_captures" id="screen_captures" accept="image/png, image/jpg, image/bmp"  value="Choose file" name="screen_captures_one" title="Screen captures can only be png, jpg, jpeg, or bmp's"/>
                                    <button id="clone" class="clone">+ Add additional screen</button>
                                </fieldset>

                                <fieldset id="captures2" class="captures" style="display:none; margin-top: -10px;">
                                    <label class="fileupload floatLabel" for="screen_captures_two"><!--Screen Capture Two--><span class="numbering">2.</span></label>
                                    <input type="file" class="text-input id screen_captures" id="screen_captures_two"  accept="image/png, image/jpg, image/bmp"  value="Choose file" name="screen_captures_two" title="Screen captures can only be png, jpg, jpeg, or bmp's"/>
                                    <button id="clone2" class="clone">+ Add additional screen</button>
                                </fieldset>
                                <fieldset id="captures3" class="captures" style="display:none; margin-top: -10px;">
                                    <label class="fileupload floatLabel" for="screen_captures_three"><!--Screen Capture Three--><span class="numbering">3.</span></label>
                                    <input type="file" class="text-input id screen_captures" id="screen_captures_three" accept="image/png, image/jpg, image/bmp"  value="Choose file" name="screen_captures_three" title="Screen captures can only be png, jpg, jpeg, or bmp's"/>
                                </fieldset>

                                <!-- post Documentation-->
                                <fieldset class="documentation">
                                    <label class="fileupload floatLabel" for="documentation">Documentation <span class="clarify">(URL)</span></label>
                                    <input type="text" id="documentation" class="longest" name="documentation" value="<?php echo $_POST['documentation'] ?>"/>
                                    <input type="file" class="text-input" id="add_docs"  accept="application/msword, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/pdf, application/txt, text/plain" value="Choose file" name="add_docs" title="Documentation can only be a pdf, doc, xls, xlsx or txt file" style="opacity:0"/>
                                </fieldset>

                                <!-- post Icon-->
                                <fieldset class="icon fileupload">
                                    <label class="fileupload floatLabel">Icon</label>
                                    <div id="default-iconset">
                                        <ul>
                                            <?php $iconPath = get_stylesheet_directory_uri()."/images/icons/"; ?>
                                            <div class="clearfix">
                                                <li id="icon-brain" <?php if ($_POST['default_icon'] == $iconPath."Brain.png") echo "class=\"selected\"" ?>><img src="<?php echo $iconPath ?>Brain.png" alt="Brain" title="Brain" /></li>
                                                <li id="icon-heart" <?php if ($_POST['default_icon'] == $iconPath."Heart.png") echo "class=\"selected\"" ?>><img src="<?php echo $iconPath ?>Heart.png" alt="Heart" title="Heart" /></li>
                                                <li id="icon-internal" <?php if ($_POST['default_icon'] == $iconPath."Internal.png") echo "class=\"selected\"" ?>><img src="<?php echo $iconPath ?>Internal.png" alt="Internal" title="Internal" /></li>
                                            </div><div class="clearfix">
                                                <li id="icon-lungs" <?php if ($_POST['default_icon'] == $iconPath."Lungs.png") echo "class=\"selected\"" ?>><img src="<?php echo $iconPath ?>Lungs.png" alt="Lungs" title="Lungs" /></li>
                                                <li id="icon-mental" <?php if ($_POST['default_icon'] == $iconPath."Mental-Health.png") echo "class=\"selected\"" ?>><img src="<?php echo $iconPath ?>Mental-Health.png" alt="Mental" title="Mental" /></li>
                                                <li id="icon-MRI" <?php if ($_POST['default_icon'] == $iconPath."MRI.png") echo "class=\"selected\"" ?>><img src="<?php echo $iconPath ?>MRI.png" alt="MRI" title="MRI" /></li>
                                            </div>
                                        </ul>
                                    </div>
                                    <p>Or, upload your own. <span>Icon should be 57px x 57px<br/>(acceptable formats: .png,.jpg)</span></p>
                                    <input type="file" id="add_icon" accept="image/png, image/jpg, image/bmp"  value="Choose file" name="add_icon" title="Icon can only be a png or jpg/jpeg file"/>
                                    <input type="hidden" class="text-input" id="default_icon" name="default_icon" value="<?php echo $_POST['default_icon'] ?>">


                                </fieldset>


                                <!-- Compatibility -->
                                <fieldset class="compatability fileupload" name="compatablility">
                                    <div class="clearfix">
                                        <label class="fileupload floatLabel" for="compatible[]">Compatible with XNAT version*<span> (select all that apply)</span></label>
                                    </div>
                                    <div class="clearfix addon_access_checkboxes">
                                        <strong>XNAT Modules (1.6.x only)</strong><br>
                                        <label class="checkboxlabel">
                                            <input type="checkbox" class="validate[minCheckbox[1]] checkbox" id="item2" value="1.6" name="compatible[]" <?php if (in_array("1.6",$_POST['compatible'])) echo "checked" ?>/>XNAT 1.6.x</label>
                                        <label class="checkboxlabel">
                                            <input type="checkbox" class="validate[minCheckbox[1]] checkbox" id="item2" value="1.6.2.1" name="compatible[]" <?php if (in_array("1.6.2.1",$_POST['compatible'])) echo "checked" ?>/>XNAT 1.6.2.1</label>
                                        <label class="checkboxlabel">
                                            <input type="checkbox" class="validate[minCheckbox[1]] checkbox" id="item2" value="1.6.3" name="compatible[]" <?php if (in_array("1.6.3",$_POST['compatible'])) echo "checked" ?>/>XNAT 1.6.3</label>
                                        <label class="checkboxlabel">
                                            <input type="checkbox" class="validate[minCheckbox[1]] checkbox" id="item2" value="1.6.4" name="compatible[]" <?php if (in_array("1.6.4",$_POST['compatible'])) echo "checked" ?>/>XNAT 1.6.4</label>
                                        <label class="checkboxlabel">
                                            <input type="checkbox" class="validate[minCheckbox[1]] checkbox" id="item2" value="1.6.5" name="compatible[]" <?php if (in_array("1.6.5",$_POST['compatible'])) echo "checked" ?>/>XNAT 1.6.5</label>
                                        <br><strong>XNAT Plugins (1.7.x and later)</strong><br>
                                        <label class="checkboxlabel">
                                            <input type="checkbox" class="validate[minCheckbox[1]] checkbox" id="item2" value="1.7" name="compatible[]" <?php if (in_array("1.7",$_POST['compatible'])) echo "checked" ?>/>XNAT 1.7</label>
                                        <label class="checkboxlabel">
                                            <input type="checkbox" class="validate[minCheckbox[1]] checkbox" id="item2" value="1.7.1" name="compatible[]" <?php if (in_array("1.7.1",$_POST['compatible'])) echo "checked" ?>/>XNAT 1.7.1</label>
                                        <label class="checkboxlabel">
                                            <input type="checkbox" class="validate[minCheckbox[1]] checkbox" id="item2" value="1.7.2" name="compatible[]" <?php if (in_array("1.7.2",$_POST['compatible'])) echo "checked" ?>/>XNAT 1.7.2</label>
                                        <label class="checkboxlabel"><input type="checkbox" class="validate[minCheckbox[1]] checkbox" id="item2" value="1.7.3" name="compatible[]" <?php if (in_array("1.7.3",$_POST['compatible'])) echo "checked" ?>/>XNAT 1.7.3</label>
                                        <label class="checkboxlabel"><input type="checkbox" class="validate[minCheckbox[1]] checkbox" id="item2" value="1.7.4" name="compatible[]" <?php if (in_array("1.7.4",$_POST['compatible'])) echo "checked" ?>/>XNAT 1.7.4</label>
                                    </div>
                                </fieldset>

                                <!-- Add On Access -->
                                <fieldset class="fileupload" name="addon_access" id="addon_access">
                                    <div class="clearfix">
                                        <label for="addon_access">This Add-On Affects:<span>(check all that apply)</span></label>
                                    </div>
                                    <div class="clearfix addon_access_checkboxes">
                                        <?php
                                        $affectsLabels = array("Velocity/HTML Template","Database Schema","Java Classes","Javascript Functions","CSS Style Sheet","Project Dependencies","Spring Configuration Files (advanced)","REST API");
                                        $affectsValues = array("velocity-html","database","java","javascript","css","project-dependencies","spring","rest");
                                        foreach ($affectsValues as $key => $value) :
                                            ?>
                                            <label class="checkboxlabel">
                                                <input type="checkbox" class="validate[minCheckbox[1]] checkbox" value="<?php echo $value ?>" name="item[]" <?php if (in_array($value,$_POST['item'])) echo "checked" ?>/> <?php echo $affectsLabels[$key] ?></label>
                                            <?php
                                        endforeach;
                                        ?>
                                    </div>
                                </fieldset>

                                <fieldset class="repo">
                                    <label for="repo">Source Repository  <span class="clarify">(URL)</span></label>
                                    <input type="text" class="longest" id="repo" name="repo" value="<?php echo $_POST['repo'] ?>" />
                                    <label class="checkboxlabel download-from-repo"><input type="checkbox" name="download_from_repo" class="download-from-repo" value="true" /> Direct users to my source repository for downloads</label>
                                </fieldset>

                                <fieldset class="upload-addon" id="addon-file-uploader">
                                    <label for="upload_addon" class="fileupload floatLabel">Upload Add-On File<span>(acceptable format: .zip)</span></label>
                                    <input type="file" accept="application/zip"  value="Choose file" class="text-input" id="upload_addon" name="upload_addon" title="The Add-On file can only be a zip file, and is required" />
                                    <input type="hidden" value="<?php echo $upload; ?>" name="fileupload"/>
                                    <?php wp_nonce_field('client-file-upload'); ?>
                                </fieldset>

                                <fieldset class="submit">
                                    <label for="submit_addon">Submit</label>
                                    <input  class="" type="submit" value="Submit Add-On" id="submit-post" name="submit-post" />
                                </fieldset>

                                <input type="hidden" name="action" value="new_post" />
                                <?php wp_nonce_field( 'new-post' ); ?>
                            </form>
                        </div> <!-- END WPCF7 -->

                        <!-- END OF FORM -->
                        <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>

                    </div><!-- .entry-content -->
                </div><!-- #post-## -->



            <?php endwhile; // end of the loop. ?>

        </div><!-- #content -->

        <!-- this is the right sidebar-->

        <?php include('sidebar-right.php'); ?>

    </div><!-- #container -->

<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/add_plugin.js"></script>
<?php get_footer(); ?>