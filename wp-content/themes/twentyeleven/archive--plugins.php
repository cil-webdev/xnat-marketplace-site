<!-- template: archive--plugins.php -->

<?php
/* Plugins taxonomy Page - list posts for the plugins taxonomy */

$taxonomy = 'plugins';
$term = get_query_var($taxonomy);
$prod_term = get_terms($taxonomy, 'slug='.$term.''); 
$params = $_GET;
	unset($params['orderby']);
	unset($params['order']);		
	$url = get_pagenum_link();
	$url = strtok($url, '?');
	$url =$url.'?'.http_build_query($params);
global $paged,$paged_two;
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
get_header();				
?>
<?php wp_enqueue_script( 'cycle', get_template_directory_uri() . '/js/jquery.cycle.all.js', array( 'jquery'));?>

    <!-- Page navigation-->
     
    <!-- /Page navigation-->
   
    <!--  page contents -->
	
	
	<section id="primary">
		  <!-- this is the left sidebar-->
		
	
   
		<?php include('sidebar-left.php'); ?>
			
			<div id="content" role="main" class="archiver">
			  <div id="content-container">			  
			  <h1 class="title"><?php echo $term; ?></h1>
		<!--<div id="slider">
			<a id="prev2" class="leftbtn" href="#"></a>
			<a id="next2" class="rightbtn" href="#"></a>
		  <div id="slides-wrap">	
		  
			<ul id="slides">
			
			   <?php $slides = $data['plugins-slider']; 
					foreach ($slides as $slide) { ?>
				<li> 
					<img src="<?php echo $slide['url'];?>" alt="<?php echo $slide['title']?>"/> </a>
					<div id="slides-overlay">	
						<h3><a href="<?php echo $slide['link'];?>"><?php echo $slide['title'];?></a></h3>
						<p><?php echo $slide['description']; ?></p>
						<?php if($slide['link'] !== '' ){
						
						} 
												}?>
					</div>
				</li>
				
			</ul>
		  </div>
		</div>
		<div id="slider_shadow"></div>-->
		<div class="clear"></div>
			   <div class="tabsubmenu">	
					<div class="sort_by">Sort:  <a href="<?php echo $url;?>&orderby=date&order=ASC">By Date</a><span>|</span><a href="<?php echo $url;?>&orderby=title&order=ASC">By Name</a></div>				
					<select name='filter' id='filter-categoryform' class='postform'>	
						<option value="admin">Admin</option>
						<option value="datatype">Datatype</option>
						<option value="display">Display</option>
						<option value="other">Other</option>
						<option value="pipeline">Pipeline</option>
						
					</select>
				  </div>
				  <div class="clear"></div>

				<?php //$querynum1 = query_posts('posts_per_page=3&post_type=plugin&plugins=admin,datatype,display,other,pipeline&paged='.$paged); ?>

	
				<?php //if ( $querynum1 ) : ?>    
							
					<?php while ( have_posts() ) : the_post(); ?>
					<?php	/* Include the Post-Format-specific template for the content.
						 * If you want to overload this in a child theme then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'content-package' ); ?>		
						
					 <?php 	 
						endwhile;?>
						<?php twentyeleven_content_nav( 'nav-below' ); ?>
						<?php // endif; ?>	
				</div>
  

		</div>
			  <!-- this is the right sidebar-->

		<?php include('sidebar-right.php'); ?>
	</section>
    
<?php get_footer();?>

<!-- end template: archive--plugins.php -->