<?php global $wpalchemy_media_access; ?>
<?php 
	global $post;

?>
<?php 
	 $icon = get_post_meta($post->ID, "upload_5", true); 
	 $credit = get_post_meta($post->ID, "credits", true);
	 $download = get_post_meta($post->ID, "upload_6", true);
	 $compatible = get_post_meta($post->ID, "compatible", true);
	 $version = get_post_meta($post->ID, "version", true);
	 $documentation = get_post_meta($post->ID, "upload_4", true);
	 $accesses = get_post_meta($post->ID, "item", true);
	 $defaulticon = get_post_meta($post->ID, "defaulticon", true);
	 $icon = get_post_meta($post->ID, "upload_5", true);
	 $environment = get_post_meta($post->ID, "env_description", true);
	 $alphatype = get_post_meta($post->ID, "alpha", true);
	 
?>
<div class="plugin_form">	
	
	<label><strong>Alpha/Beta</strong></label>
	
	<p>
		<br/>	

	
	<label>Is the plugin Alpha or Beta</label><br/>

	<?php $alphas = array('alpha','beta'); ?>

	<?php foreach ($alphas as $i => $alpha): ?>
		<?php $mb->the_field('alpha'); ?>
		<input type="radio" name="<?php $mb->the_name(); ?>" value="<?php echo $alpha; ?>"<?php $mb->the_radio_state($alpha);  if ($alphatype == $mb->the_radio_state($alpha)) {echo "checked = 'checked'"; } ?>/> <?php echo $alpha; ?>
	<?php endforeach; ?>

	
	</p>
	<hr></hr>
	<label><strong>Compatible</strong></label>
	
	<p>
		<br/>		
	

	<label>Plugin Compatibility</label><br/>

	<?php $items = array('version 1.0', 'version 1.5', 'version 2.0', 'version 2.5'); ?>

	<?php while ($mb->have_fields('cb_ex', count($items))): ?>
	
		<?php $item = $items[$mb->get_the_index()]; ?>

		<input type="checkbox" name="<?php $mb->the_name(); ?>" value="<?php echo $item; ?>"<?php $mb->the_checkbox_state($item); ?>/> <?php echo $item; ?>

	<?php endwhile; ?>

	
	</p>
	<hr></hr>
	
	<p>
		<br/>		
	

	<label>Credits</label><br/>	

	<?php $mb->the_field('credits'); ?>
		<input type="text" name="<?php $mb->the_name(); ?>" value="<?php echo $credit; ?>"/>
		<span>Enter the credits as a comma seperated list.</span>

	
	</p>
	<hr></hr>	
	
	
	<p>
		<br/>		
	

	<label>Environment Description</label><br/>	

	<?php $mb->the_field('desc'); ?>
		<input type="textarea" name="<?php $mb->the_name(); ?>" value="<?php echo $environment; ?>"/>
		<span>Enter the credits as a comma seperated list.</span>

	
	</p>
	<hr></hr>	
	
	
		<br/>		
	

	<label>Default Icon</label><br/>	

	<?php $mb->the_field('imgurl'); ?>
    <?php $wpalchemy_media_access->setGroupName('nn')->setInsertButtonLabel('Insert'); ?>
 
    <p>
        <?php echo $wpalchemy_media_access->getField(array('name' => $mb->get_the_name(), 'value' => $mb->get_the_value())); $image;?>
        <?php echo $wpalchemy_media_access->getButton(); ?>
    </p>

	
	
	<hr></hr>
		<p>
		<br/>		
	

	<label>Addon Accesses</label><br/>	

	<?php $items_two = array('Item One', 'Item Two', 'Item Three', 'Item Four'); ?>

	<?php while ($mb->have_fields('cb_ex', count($items_two))): ?>
	
		<?php $item_two = $items_two[$mb->get_the_index()]; ?>

		<input type="checkbox" name="<?php $mb->the_name(); ?>" value="<?php echo $item_two; ?>"<?php $mb->the_checkbox_state($item_two); ?>/> <?php echo $item_two; ?>

	<?php endwhile; ?>

	
	</p>
	<hr></hr>	
	<label>Screenshots</label><br/>	

	 <p class="remove-screen"><a href="#" class="dodelete-screenshots button">Remove All</a></p>
 
    <?php while($mb->have_fields_and_multi('screenshots')): ?>
    <?php $mb->the_group_open(); ?>
 
        <a href="#" class="dodelete button">Remove</a>
 
        <?php $mb->the_field('screenshot'); ?>
        <?php $wpalchemy_media_access->setGroupName('img-n'. $mb->get_the_index())->setInsertButtonLabel('Insert'); ?>
 
        <p>
            <?php echo $wpalchemy_media_access->getField(array('name' => $mb->get_the_name(), 'value' => $mb->get_the_value())); ?>
            <?php echo $wpalchemy_media_access->getButton(); ?>
        </p> 
       
 
    <?php $mb->the_group_close(); ?>
    <?php endwhile; ?>
 
    <p style="margin-bottom:15px; padding-top:5px;"><a href="#" class="docopy-screenshots button">Add</a></p>

	
	
	<hr></hr>
</div>