jQuery(document).ready(function($) {


/*******************************************************************
/	jQuery for hiding and showing the correct checkboxes in the form
/*******************************************************************/
	$('#category_plugin').click(function() {
		$('#subcategory-tool').css("display", "none");
		$('#subcategory-plugin').fadeIn();
	});
	$('#category_tool').click(function() {
		$('#subcategory-plugin').css("display", "none");
		$('#subcategory-tool').fadeIn();
	});

	//	End checkbox code

/*****************************************************************
/	Code to enable colorbox
/*****************************************************************/

	if ($('body.single-plugin').length > 0) {

		$('.inline').colorbox({
			href: $(this).attr("href"),
			rel: "group2",
			transition: "elastic"
		});

	}

	$('a#upload_addon').colorbox({
		width: "890px",
		height: "700px",
		transition: "elastic"
	});
/*****************************************************************
/ validation for the add plugin form
/ binds form submission and fields to the validation engine
/*****************************************************************/

	if ($('#version').length > 0) {
		$('#submit-post').click(function() {
			//$("#new_post").validationEngine({promptPosition : "centerLeft"});
		});
		$('[name="addon_category"]').change(function() {
			$('#subcategory-plugin input[type="checkbox"], #subcategory-tool input[type="checkbox"]').attr('checked', false);
//			$.uniform.update();
		});

		$.metadata.setType("attr", "validate");
		var container = $('#the_form_errors');
		/* $('#new_post').validate({
			errorContainer: container,
			errorLabelContainer: jQuery('ul', container),
			highlight: function(element, errorClass, validClass) {
				//$(element).addClass(errorClass).removeClass(validClass);
				$(element.form).find("label[for=" + $(element).attr('name') + "].mainLabel").addClass(errorClass);
			},
			unhighlight: function(element, errorClass, validClass) {
				//$(element).removeClass(errorClass).addClass(validClass);
				$(element.form).find("label[for=" + $(element).attr('name') + "].mainLabel").removeClass(errorClass);
			},
			wrapper: 'li',
			rules: {
				screen_captures_one: {
					accept: 'png|jpe?g|bmp'
				},
				screen_captures_two: {
					accept: 'png|jpe?g|bmp'
				},
				screen_captures_three: {
					accept: 'png|jpe?g|bmp'
				},
				add_docs: {
					accept: 'pdf|doc|xlsx?|txt'
				},
				add_icon: {
					accept: 'png|jpe?g|bmp'
				},
				upload_addon: {
					accept: 'zip'
				}
			}
		}); */
	}

	//enable jQuery Cycle
	if ($('#slider').length > 0) {
		$('#slides').after('<div id="nav">').cycle({
			fx: 'scrollHorz',
			//delay:  -2000,
			speedOut: 600,
			speedIn: 600,
			timeout: 40000,
			next: '#next2',
			prev: '#prev2',
			easing: 'easeOutSine',
			pager: '#nav'
		});
	}
/*****************************************************************
/	code for the jquery tabs
/	setup ul.tabs to work as tabs for each div directly under div.panes
/*****************************************************************/
	var tabresults = new Array();
	var tester;
	var hash = window.location.hash;
	var toolsresults = $(".tools-tab").attr("data-val");
	var pluginsresults = $(".plugins-tab").attr("data-val");
	tabresults.push(toolsresults);
	tabresults.push(pluginsresults);
	if (toolsresults == '0') {
		tester = 0;
	}
	if (pluginsresults == '0') {
		tester = 1;
	}
	if (pluginsresults > '0') {
		tester = 0;
	}

	$("ul.tabs:first").tabs(".search_results:first > div", {
		initialIndex: tester,
		history: false
	});
	//set anchor cookie
/*****************************************************************
/	mosaic overlay
/*****************************************************************/

	if ($('body.single-plugin').length > 0) {
		$('.circle').mosaic({
			opacity: 0.9 //Opacity for overlay (0-1)
		});
	}



/*****************************************************************
/	dynamic search code
/*****************************************************************/

	$(".tabs a").click(function() {
		var pathname = window.location.pathname;
		var data = $(this).attr("data-id");
		$('#content-container article').show();
		$('.search_results div article').show();
		$(".tabsubmenu #uniform-filter-postform span").html("-- Select --");
		$("#filter-postform").load("/wp-content/themes/twentyeleven/includes/" + data + ".txt");
		$(".nrgsorted").removeAttr('checked');
		$.uniform.update($(".nrgsorted").val(''));
		$(".no-filter-results").remove();
		if (!$("#content-container article:visible").length) {
			$('#content-container').append('<p class="no-filter-results">Sorry, there are no matches for that selection.</p>');
		}
		if (!$(".search_results article:visible").length) {
			$('.search_results').append('<p class="no-filter-results">Sorry, there are no matches for that selection.</p>');
		}
	});
/*****************************************************************
/	search results filter
/*****************************************************************/
	var added = false;
	$('.search_results article').addClass('select');
	$('#content-container article').addClass('select');

	$('#filter-postform').change(function(e) {

		if ($('.nrgsorted').is(':checked')) {
			var nrg = '.nrg-approved';
		} else {
			var nrg = '';
		}

		e.preventDefault();

		var filter = $('#filter-postform option:selected').val();
		if (typeof filter == 'undefined' || filter == '') {
			filter = '';
		} else {
			filter = '.' + filter;
		}

		$('.search_results div article').show();

		var blob = nrg + filter
		blob = $.trim(blob);
		if (blob != '' && blob != '.' && blob != '..') {
			$('.search_results div article:not(' + blob + ')').hide();
		}

		if ($(".search_results article:visible").length) {
			$(".no-filter-results").remove();
			if (!$(".search_results article:visible").length) {
				$('.search_results').append('<p class="no-filter-results">Sorry, there are no matches for that selection.</p>');
			}
		} else {
			if (!$(".no-filter-results").length) {
				$('.search_results').append('<p class="no-filter-results">Sorry, there are no matches for that selection.</p>');
				var added = true;
				return false;
			}
		}


	});

/*****************************************************************
/	filter form by nrg approved
/******************************************************************/
	var addednrg = false;
	$('.nrgsorted').change(function() {

		if ($(this).is(':checked')) {
			var filter = $('#filter-categoryform option:selected').val();
			if (typeof filter == 'undefined' || filter == '') {
				filter = '';
			} else {
				filter = '.' + filter;
			}
			var nrg = '.nrg-approved';
		} else {
			var filter = $('#filter-categoryform option:selected').val();
			if (typeof filter == 'undefined' || filter == '') {
				filter = '';
			} else {
				filter = '.' + filter;
			}
			var nrg = '';
		}
		$('#content-container article').show();
		$('.search_results div article').show();

		var blob = nrg + filter
		blob = $.trim(blob);
		if (blob != '' && blob != '.' && blob != '..') {
			$('#content-container article:not(' + blob + ')').hide();
			$('.search_results div article:not(' + blob + ')').hide();
		}

		if ($("#content-container article:visible").length) {
			$(".no-filter-results").remove();
			if (!$("#content-container article:visible").length) {
				$('#content-container').append('<p class="no-filter-results">Sorry, there are no matches for that selection.</p>');
			}
			if (!$(".search_results article:visible").length) {
				$('.search_results').append('<p class="no-filter-results">Sorry, there are no matches for that selection.</p>');
			}
		} else {
			if (!$(".no-filter-results").length) {
				$('#content-container').append('<p class="no-filter-results">Sorry, there are no matches for that selection.</p>');
				$('.search_results').append('<p class="no-filter-results">Sorry, there are no matches for that selection.</p>');
				var addednrg = true;
				return true;
			}
		}

		return true;
	});
/*****************************************************************
/	search results filter
/*****************************************************************/

	$('#filter-categoryform').change(function(e) {

		if ($('.nrgsorted').is(':checked')) {
			var nrg = '.nrg-approved';
		} else {
			var nrg = '';
		}

		e.preventDefault();

		var filter = $('#filter-categoryform option:selected').val();
		if (typeof filter == 'undefined' || filter == '') {
			filter = '';
		} else {
			filter = '.' + filter;
		}

		$('#content-container article').show();

		var blob = nrg + filter
		blob = $.trim(blob);
		if (blob != '' && blob != '.' && blob != '..') {
			$('#content-container article:not(' + blob + ')').hide();
		}

		if ($("#content-container article:visible").length) {
			$(".no-filter-results").remove();
			if (!$("#content-container article:visible").length) {
				$('#content-container').append('<p class="no-filter-results">Sorry, there are no matches for that selection.</p>');
			}
		} else {
			if (!$(".no-filter-results").length) {
				$('#content-container').append('<p class="no-filter-results">Sorry, there are no matches for that selection.</p>');
				var added = true;
				return false;
			}
		}

	});
/*****************************************************************
/	UNIFORM -FOR FORMS
/*****************************************************************/

//	$('.page-template-add_plugin-php select,.tabsubmenu select, input:checkbox, input:radio').uniform();

/******************************************************************
	This allows us to have a pointer cursor with the file uploads
*******************************************************************/

	$(".uploader").mousemove(function(e) {
		var offL, offT;
		offL = $(this).offset().left;
		offT = $(this).offset().top;
		$(this).find("input").css({
			left: e.pageX - offL - 450,
			top: e.pageY - offT - 10,
			width: 'auto'
		});
	});

/*****************************************************************
/	add classes to style the login form
/*****************************************************************/

	var container = $('body').find('#simplemodal-login-container-osx');
	$('.simplemodal-login').click(function() {

		$('.simplemodal-container').width('338');
		if ($.cookie('login_email')) {
			$('#loginform .user_login').val($.cookie('login_email'));
		}
	});

	//"Remember me" functionality
	$('#loginform').submit(function() {
		if ($('#loginform #rememberme').is(':checked')) {
			$.cookie('login_email', $('#loginform .user_login').val(), {
				expires: 365,
				path: '/'
			});
		} else {
			$.cookie('login_email', null);
		}
	});

/*****************************************************************
/	Clone screenshot button on upload addin page
/*****************************************************************/

	$("button#clone").click(function() {
		$(this).hide();
		$('#captures2').fadeIn();

		return false;
	});
	$("button#clone2").click(function() {
		$(this).hide();
		$('#captures3').fadeIn();

		return false;
	});
/******************************************************************
/	wp search blur and focus
/*****************************************************************/
/*$('input[name=s]').focus(function(){
	if ($(this).val() == 'Search')
		$(this).val('');
});
$('input[name=s]').blur(function(){
	if ($(this).val() == '')
		$(this).val('Search');
});*/
/*****************************************************************
/	ANALYZE URLS
/*****************************************************************/

	function getUrlVars() {
		var vars = [],
			hash;
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

		for (var i = 0; i < hashes.length; i++) {
			hash = hashes[i].split('=');
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}

		return vars;
	}

	var id = getUrlVars();
	var page_id = id['page_id'];
/*******************************************************************
/	GET LINK PARAMS
/*******************************************************************/

	function getLinkVars(link) {
		var vars = [],
			hash;
		var hashes = link.slice(link.indexOf('?') + 1).split('&');

		for (var i = 0; i < hashes.length; i++) {
			hash = hashes[i].split('=');
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}

		return vars;
	}
/*****************************************************************
/	add active class to right sidebar menu items
/	store url for current page as global variable
/*****************************************************************/

	current_page = document.location.href

	// apply selected states depending on current page
	var check = current_page;

	$('ul#packages li a,#sidebar_left ul li a,#sidebar_left h3 a').each(function() {
		var checker = $(this).attr('href');
		if (current_page == checker) {
			$(this).addClass('selected');
		} else {
			$(this).removeClass('selected');
		}
	});
/*****************************************************************
/	Hack for wp-pagenavi previous
/*****************************************************************/

	function wpnavihack() {
		var currentpage = $(".wp-pagenavi .current").html();

		if (currentpage == '1') {
			$(".previouspostslink").addClass("dormant");
		} else {
			$(".previouspostslink").addClass("using");
		}

		//Hack for wp-pagenavi next
		var nextpage = $(".larger").html
		if ($('.larger').length > 0) {
			$(".nextpostslink").addClass("using");
		} else {
			$(".nextpostslink").addClass("dormant");
		}

	}
	wpnavihack();
	if ($('body.single-plugin').length > 0) {
		$("#addon_shots ul li").each(function() {
			if ($(this).html() == '') {
				$(this).hide();
			}
		});
	}
/*****************************************************************
/	registration code
/*****************************************************************/

	$('.user_login').change(function() { // works when input will be blured and the value was changed
	});
	$('[name="user_login"]').keyup(function() { // works immediately when user press button inside of the input
		var test = jQuery(this).val();
		$('.user_email').val(test); // simulate "change" event
	});




/*****************************************************************
/	Grab posts with AJAX
/*****************************************************************/
/*if ($('body.page-id-298,body.page-id-302').length > 0) {
	  var page = 1;
	  var pagetype;
	  var taxtype;
	  var sort = 'date';
	 var posts = getUrlVars();
	var page_id = posts['page_id'];
	var page_uri = window.location.pathname.substr(1);
	   if (page_uri == 'plugins/'){pagetype='admin,datatype,display,other,pipeline';taxtype='plugins';}
	  if (page_uri == 'tools/') {pagetype='apps,integration,other,scripts';taxtype='tools';}
	var loading = true;
	var $window = $(window);
	var $content = $("body.page-id-298 #content #content-container #ajaxposts,body.page-id-302 #content #content-container #ajaxposts");
	var load_posts = function(){
			$.ajax({
				type	   : "GET",
				data	   : {numPosts : -1, pager: page, orderby: sort,pagearray : pagetype,tax: taxtype},
				dataType   : "html",
				url		   : "http://dev.marketplace.xnat.org/wp-content/themes/twentyeleven/loopHandler.php",
				beforeSend : function(){
					if(page != 1){
						$content.append('<div id="temp_load" style="text-align:center">\
							<img src="../images/ajax-loader.gif" />\
							</div>');
					}
				},
				success	   : function(data){
					$data = $(data);
					if($data.length){
						$data.hide();
						$content.html($data);
						$data.fadeIn(500, function(){
							$("#temp_load").remove();
							loading = false;
						});
						wpnavihack();
					} else {
						$("#temp_load").remove();
					}
				},
				error	  : function(jqXHR, textStatus, errorThrown) {
					$("#temp_load").remove();
					alert(jqXHR + " :: " + textStatus + " :: " + errorThrown);
				}
		});
	}
	$window.scroll(function() {
		var content_offset = $content.offset();
		console.log(content_offset.top);
		if(!loading && ($window.scrollTop() +
			$window.height()) > ($content.scrollTop() + $content.height() + content_offset.top)) {
				loading = true;
				page++;
				//load_posts();
		}
	});
	load_posts();
 }

 function order_by_name() {
  jQuery(".byname").click(function(e) {
	e.preventDefault();
   var page = 1;
	var sort = 'title';
	var pagetype;
	  var taxtype;
	  var posts = getUrlVars();
	var page_id = posts['page_id'];
	var page_uri = window.location.pathname.substr(1);
	   if (page_uri == 'plugins/'){pagetype='admin,datatype,display,other,pipeline';taxtype='plugins';}
	  if (page_uri == 'tools/') {pagetype='apps,integration,other,scripts';taxtype='tools';}
	var loading = true;
	var $window = $(window);
	var $content = $("body.page-id-298 #content #content-container #ajaxposts,body.page-id-302 #content #content-container #ajaxposts");
	var load_posts = function(){

			$.ajax({
				type	   : "GET",
				data	   : { orderby : sort,pagearray : pagetype,tax: taxtype},
				dataType   : "html",
				url		   : "http://dev.marketplace.xnat.org/wp-content/themes/twentyeleven/loopHandler.php",
				beforeSend : function(){
					if(page != 1){
						$content.append('<div id="temp_load" style="text-align:center">\
							<img src="../images/ajax-loader.gif" />\
							</div>');
					}
				},
				success	   : function(data){
					$data = $(data);
					if($data.length){
						$data.hide();
						$content.html($data);
						$data.fadeIn(500, function(){
							$("#temp_load").remove();
							loading = false;
						});
					} else {
						$("#temp_load").remove();
					}
				},
				error	  : function(jqXHR, textStatus, errorThrown) {
					$("#temp_load").remove();
					alert(jqXHR + " :: " + textStatus + " :: " + errorThrown);
				}
		});
	}
	$window.scroll(function() {
		var content_offset = $content.offset();
		console.log(content_offset.top);
		if(!loading && ($window.scrollTop() +
			$window.height()) > ($content.scrollTop() + $content.height() + content_offset.top)) {
				loading = true;
				page++;
			   // load_posts();
		}
	});
	load_posts();


  });
  }

  function order_by_date(e) {
  jQuery(".bynewest").click(function(e) {
	 e.preventDefault();
   var page = 1;
	var sort = 'date';
	var pagetype;
	  var taxtype;
	  var posts = getUrlVars();
	var page_id = posts['page_id'];
	var page_uri = window.location.pathname.substr(1);
	  //if (page_id == '298'){pagetype='admin,datatype,display,other,pipeline';taxtype='plugins';}
	  //if (page_id == '302') {pagetype='apps,integration,other,scripts';taxtype='tools';}
	  if (page_uri == 'plugins/'){pagetype='admin,datatype,display,other,pipeline';taxtype='plugins';}
	  if (page_uri == 'tools/') {pagetype='apps,integration,other,scripts';taxtype='tools';}
	var loading = true;
	var $window = $(window);
	var $content = $("body.page-id-298 #content #content-container #ajaxposts,body.page-id-302 #content #content-container #ajaxposts");
	var load_posts = function(){
			$.ajax({
				type	   : "GET",
				data	   : {orderby : sort,pagearray : pagetype,tax: taxtype},
				dataType   : "html",
				url		   : "http://dev.marketplace.xnat.org/wp-content/themes/twentyeleven/loopHandler.php",
				beforeSend : function(){
					if(page != 1){
						$content.append('<div id="temp_load" style="text-align:center">\
							<img src="../images/ajax-loader.gif" />\
							</div>');
					}
				},
				success	   : function(data){
					$data = $(data);
					if($data.length){
						$data.hide();
						$content.html($data);
						$data.fadeIn(500, function(){
							$("#temp_load").remove();
							loading = false;
						});
					} else {
						$("#temp_load").remove();
					}
				},
				error	  : function(jqXHR, textStatus, errorThrown) {
					$("#temp_load").remove();
					alert(jqXHR + " :: " + textStatus + " :: " + errorThrown);
				}
		});
	}
	$window.scroll(function() {
		var content_offset = $content.offset();
		console.log(content_offset.top);
		if(!loading && ($window.scrollTop() +
			$window.height()) > ($content.scrollTop() + $content.height() + content_offset.top)) {
				loading = true;
				page++;
				//load_posts();
		}
	});
	load_posts();


  });
  }
  order_by_name();
  order_by_date();
 */

	/*****************************************************************
	 /	//registration upload
	 //	 Deals with calling the WordPress Media popup box
	 //	cc_user_data_media_popup_handler();
	 /*****************************************************************/

	function cc_user_data_media_popup_handler() {
		jQuery('.user_avatar').click(function() {
			//set id for this to prev element (the field before the button).
			formfield = jQuery(this).prev().attr('id');
			tb_show('', ' <?php echo admin_url(); ?>media-upload.php?type=image&TB_iframe=1&width=640&height=290');
			return false;
		});
		window.send_to_editor = function(html) {
			imgurl = jQuery('img', html).attr('src');
			jQuery("#" + formfield).val(imgurl);
			tb_remove();
		}
	}
/* function ajax_nav() {
	if ($('body.page-id-298,body.page-id-302').length > 0) {
	// ajax pagination
	jQuery('.wp-pagenavi a').live('click', function(e){ // if not using wp-page-numbers, change this to correct ID
	 e.preventDefault();
		 var link = jQuery(this).attr('href');
		 var thepage = link.match(/([0-9]+)/)[1];
		 var paged = getLinkVars(link);
		 var page_num = paged['paged'];
			// #main is the ID of the outer div wrapping your posts
		jQuery('#ajaxposts').html('Loading...');
			// #entries is the ID of the inner div wrapping your posts

			var loading = true;
	var $window = $(window);
	var $content = $("body.page-id-298 #content #content-container #ajaxposts,body.page-id-302 #content #content-container #ajaxposts,body.author #content #content-container #ajaxposts");
	var load_posts = function(){
			$.ajax({
				type	   : "GET",
				data	   : {orderby : sort,pagearray : pagetype,tax: taxtype, pageNumber: thepage},
				dataType   : "html",
				url		   : "http://dev.marketplace.xnat.org/wp-content/themes/twentyeleven/loopHandler.php",
				beforeSend : function(){
					if(page != 1){
						$content.append('<div id="temp_load" style="text-align:center">\
							<img src="/wp-content/themes/twentyeleven/images/ajax-loader.gif" />\
							</div>');
					}
				},
				success	   : function(data){
					$data = $(data);
					if($data.length){
						$data.hide();
						$content.html($data);
						$data.fadeIn(500, function(){
							$("#temp_load").remove();
							loading = false;
							wpnavihack();
						});
					} else {
						$("#temp_load").remove();
					}

				},
				error	  : function(jqXHR, textStatus, errorThrown) {
					$("#temp_load").remove();
					alert(jqXHR + " :: " + textStatus + " :: " + errorThrown);
				}
		});

	}

	load_posts();
});// end ready function
	}
	}
	ajax_nav();
	*/
/*****************************************************************
/	Sortby add css underline
/*****************************************************************/
	jQuery('.sort_by a').click(function() {
		$('.sort_by a').removeClass('currentlyActive');
		$(this).addClass('currentlyActive');
	});


/*******************************************************************
/	jQuery for the user selected icon
/*******************************************************************/
	if ($('body.page-template-add_plugin-php').length > 0) {

		$('#default-iconset ul li img').click(function() {
			var theimg = $(this);
			var imgval = $(this).attr('src');
			var imgtitle = $(this).attr('title');
			$('#default-iconset ul li').removeClass('selected');
			$(theimg).parent().addClass('selected');
			$('#default_icon').val(imgval);
			//$('#uniform-add_icon .filename').text(imgtitle + '.png' );
			//$("#add_icon").html(imgval);
//			$.uniform.update($("#add_icon").val(imgval));
//			$.uniform.update($("#uniform-add_icon .filename").html(imgtitle + '.png'));


			$("#add_icon").click(function() {
				$('#default-iconset ul li').removeClass('selected');
				//$("#add_icon").replaceWith(inputclone);
//				$.uniform.update("#add_icon");
//				$.uniform.update("#uniform-add_icon .filename");
			});

		});


	}


	//Fix issue with nested comments
	$('ul.children').each(function() {
		$(this).appendTo($(this).prev().prev());
	});

	//Display plugin tooltips on click
	$('#container div.plugin_form .page .category a').click(function(e) {
		$(this).addClass('clicked');
		e.preventDefault();
	});

	//And hide them on mouseout
	$('#container div.plugin_form .page .category a').mouseout(function(e) {
		$(this).removeClass('clicked');


	});

	//Display plugin tooltips on click
	$('#container div.plugin_form .compatability a').click(function(e) {
		$(this).addClass('clicked');
		e.preventDefault();
	});

	//And hide them on mouseout
	$('#container div.plugin_form .compatability a').mouseout(function(e) {
		$(this).removeClass('clicked');


	});

	//enhancement for the "Edit Add-on" link on plugin pages
	$('#plugin-inner-right .post-edit-link').wrapInner('<span class="link-text" />').append('<div class="edit-icon"></div>');

	//Footer links that need to open in new window
	$('#menu-item-238 a').attr('target', '_blank');
	$('#menu-item-237 a').attr('target', '_blank');

});
