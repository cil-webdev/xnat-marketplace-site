<?php
/* 
Template Name: Plugins taxonomy Page
*list posts for the plugins taxonomy
*/
//taxonomy
$taxonomy = 'plugins';
$term = get_query_var($taxonomy);
$prod_term = get_terms($taxonomy, 'slug='.$term.''); 
$params = $_GET;
	unset($params['orderby']);
	unset($params['order']);		
	$url = get_pagenum_link();
	$url = strtok($url, '?');
	$url =$url.'?'.http_build_query($params);
global $paged,$paged_two;
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$bytype = $_GET['orderby'];
get_header();				
?>
<?php wp_enqueue_script( 'cycle', get_template_directory_uri() . '/js/jquery.cycle.all.js', array( 'jquery'));?>

<section id="primary">
	<!-- this is the left sidebar-->
	<?php include('sidebar-left.php'); ?>
			
	<div id="content" role="main" class="archiver">
        <div id="content-container">			  
	        <h1 class="title page-title"><?php echo $term; ?></h1>
    
    	    <div class="clear"></div>
            
			<?php if ( have_posts() ) : ?>
			<div class="tabsubmenu">	
                <div class="sort_by">
                	Sort:
					<?php
						echo $_GET['orderby'];
					?>
                    <a href="<?php echo $url;?>&orderby=date" <?php if($bytype =='date'|| $bytype== '') {echo 'class="currentlyActive"';} ?>>By Date</a>
                    <span>|</span>
                    <a href="<?php echo $url;?>&orderby=title" <?php if($bytype =='title') {echo 'class = "currentlyActive"';} ?>>By Name</a>
                </div>
                
                <div class="nrg-filter">
                	<input type="checkbox" class="nrgsorted" value="nrg-approved" name="nrgsorted">
	                <label for="nrgsorted"> NRG Approved</label>
                </div>
            </div>
            
            <div class="clear"></div>
			<?php 
			while ( have_posts() ) : the_post();
				get_template_part( 'content-package' );
					
			endwhile;
			twentyeleven_content_nav( 'nav-below' );
                        
            else : ?>
                
                <div class="entry-content">
	                <p><?php echo __('Sorry, there are no matches for that selection.','twentyeleven'); ?></p>
                </div>
					
			<?php
			endif; ?>	
		</div><!-- #content-container -->
	</div><!-- #content -->
	<!-- this is the right sidebar-->

<?php include('sidebar-right.php'); ?>
</section>
    
<?php get_footer();?>