<?php
/* 
Template Name: Taxonomy Page
*list posts for the taxonomy
*/
//taxonomy
$params = $_GET;
	unset($params['orderby']);
	unset($params['order']);	
	$url = get_pagenum_link();
	$url = strtok($url, '?');
	$url =$url.'?'.http_build_query($params);
	global $paged;
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$bytype = $_GET['orderby'];
	
get_header();				
?>
<?php wp_enqueue_script( 'cycle', get_template_directory_uri() . '/js/jquery.cycle.all.js', array( 'jquery'));?>

    <!-- Page navigation-->
     
    <!-- /Page navigation-->
   
    <!--  page contents -->
	
	
	<section id="primary">
		  <!-- this is the left sidebar-->
		
	
   
		<?php include('sidebar-left.php'); ?>
			
			<div id="content" role="main" class="archiver">
			  <div id="content-container">			  
			  <h1 class="title"><?php if(is_page('plugins')){ echo 'Plugins';} else { echo 'Tools';}; ?></h1>
			  <div id="slider">
                  <?php // define slides
                  if (is_page('plugins')) {  $slides = $data['plugins-slider']; } else { $slides = $data['tools-slider']; }
                  ?>
			<a id="prev2" class="leftbtn" href="#" <?php if (empty($slides)) { echo "style='display:none;'"; } ?>></a>
			<a id="next2" class="rightbtn" href="#" <?php if (empty($slides)) { echo "style='display:none;'"; } ?>></a>
		  <div id="slides-wrap">	
		  
			<ul id="slides">
			      
			    <?php
				if ($slides) :
                foreach ($slides as $slide) { ?>
				<li> 
					<img src="<?php echo ($slide['url']) ? $slide['url'] : get_template_directory_uri().'/images/slider-bg.png';?>" alt="<?php echo $slide['title']?>"/>
					<div id="slides-overlay">	
						<h3><a href="<?php echo $slide['link'];?>"><?php echo new_slide_length($slide['title'],17);?></a></h3>
						<p><?php echo new_slide_length($slide['description'],148); ?></p>

					</div>
				</li>

                <?php }

                else : ?>
                <li>
                    <img src="<?php echo get_template_directory_uri().'/images/slider-bg.png';?>" />
                    <div id="slides-overlay">
                        <?php if (is_page('plugins')) : ?>
                        <h3>XNAT Modules</h3>
                        <p>Modules and Plugins contain new features that can be installed directly into XNAT. <a href="https://wiki.xnat.org/display/Marketplace/XNAT+Add-Ons%3A+Modules%2C+Plugins+and+Tools">Documentation</a></p>
                        <?php else : ?>
                        <h3>XNAT Tools</h3>
                        <p>These scripts and stand-alone applications allow you to access or manipulate XNAT data remotely, but do not alter XNAT itself. <a href="https://wiki.xnat.org/display/Marketplace/XNAT+Add-Ons%3A+Modules%2C+Plugins+and+Tools">Documentation</a></p>
                        <?php endif; ?>
                    </div>
                </li>

                    <?php
                endif; ?>
				
			</ul>
		  </div><!--end #slides-wrap-->	
		</div>
		<div id="slider_shadow"></div>
		<div class="clear"></div>
			   <div class="tabsubmenu">	
					<div class="sort_by">Sort: <a href="<?php echo $url;?>&orderby=date" <?php if($bytype =='date' || $bytype== '') {echo 'class="currentlyActive"';} ?>>By Date</a><span>|</span><a href="<?php echo $url ;?>&orderby=title" <?php if($bytype =='title') {echo 'class = "currentlyActive"';} ?> >By Name</a></div>
					<div class="filter-tag">
					<label for="filter" class="filterlabel">Filter By:</label>
					<select name='filter' id='filter-categoryform' class='postform'>	
					<?php if (is_page('plugins')) { echo '
					    <option></option>
						<option value="admin">Admin</option>
						<option value="datatype">Datatype</option>
						<option value="display">Display</option>
						<option value="other">Other</option>
						<option value="pipeline">Pipeline</option>'; } else 
						{ echo
						'<option></option>
						<option value="apps">Apps</option>
						<option value="integration">Integration</option>
						<option value="other">Other</option>
						<option value="scripts">Scripts</option>';} ?>
						
					</select>
					</div>
					<div class="nrg-filter">
					<input type="checkbox" class="nrgsorted" value="nrg-approved" name="nrgsorted">
					<label for="nrgsorted"> NRG Approved</label>
					</div>	
				  </div>
				  <div class="clear"></div>
				 <!-- <div id="ajaxposts"></div>-->	
				 
				<?php 
					//check to see if the plugins link was clicked
				
				if (is_page('plugins')) {
					if($bytype == 'date'){
						$order = 'DESC';
					} else {
						$order = 'ASC';
					}
					 $querytaxtest = query_posts('plugins=admin,datatype,display,other,pipeline&posts_per_page=100&post_type=plugin&orderby='.$bytype.'&order='.$order.'&paged='.$paged); }
					//check to see if the tools link was clicked 
				 
				 elseif (is_page('tools')) {
					if($bytype == 'date'){
						$order = 'DESC';
					} else {
						$order = 'ASC';
					}
				 $querytaxtest = query_posts('posts_per_page=100&post_type=plugin&tools=apps,integration,other,scripts&orderby='.$bytype.'&order='.$order.'&paged='.$paged);
				 }?>
				
					<?php if (have_posts())  while (have_posts()):  the_post();
				get_template_part( 'content-package' );
			  
				   endwhile; ?>
				  
				 <?php  twentyeleven_content_nav( 'nav-below' );  ?>

				<?php wp_reset_query(); ?>
				</div>
  

		</div>
			  <!-- this is the right sidebar-->

		<?php include('sidebar-right.php'); ?>
	</section>
    
<?php get_footer();?>