<?php
/**
 * Template Name: Admin Tagged Content
 * The template used for displaying Endorsed by NRG group content in custom_default_page.php
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>
<?php get_header(); ?>
<?php $b = 0; $c = 0; 
	global $paged,$paged_two;
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$paged_two = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$term = $_GET['term'];
	//$term_two = $_GET['term'];
	
?>
<?php $title = get_the_title(); ?>



	<!-- end query -->

		<div id="primary">
			<!-- this is the left sidebar-->

		<?php include('sidebar-left.php'); ?>
		
			<div id="content" role="main">
				<div id="content-container">
				<?php if ( have_posts() ) : ?>				
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>				
					<header class="entry-header">
						<h1 class="entry-title"><?php the_title(); ?></h1>
						<div id="package-wrap">
						<div id="package-inner">
						<img src="<?php echo get_template_directory_uri(); ?>/images/info.png">
						<p><b style="font-size:20px;color:#000;font-weight: bold;">About this package</b><br />
						Aenean lacinia bibendum nulla sed consectetur. Etiam porta sem malesuada magna mollis euismod. 
						Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit 
						amet risus. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>						
						</div>
						</div>
					</header><!-- .entry-header -->

					<div class="entry-content">
					
					<ul class="tabs">					
						<li><a href="#"  class="plugintab" data-id="Plugins" data-val="<?php  echo termcountplugin($term); ?>">Plugins (<?php echo termcountplugin($term); ?>)</a></li>
						<li><a href="#"  class="tooltab" data-id="Tools" data-val="<?php  echo termcounttool($term); ?>">Tools (<?php echo termcounttool($term); ?>)</a></li>
					</ul>
					
					<?php
				if (is_tax('plugins') || is_tax('tools')):
				$tagged_one = query_posts('posts_per_page=100&post_type=plugin&plugins=admin,datatype,display,other,pipeline&paged='.$paged.'&tag='.$term); 
				
				?>
						<div class="search_results">
						<div>
							<?php if ( $tagged_one ) : ?>    
					
				<?php while ( have_posts() ) : the_post(); ?>
					
					
					<?php	/* Include the Post-Format-specific template for the content.
						 * If you want to overload this in a child theme then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'content-package' ); ?>								
					
				<?php  endwhile; ?>	
				
				<?php twentyeleven_content_nav( 'nav-below' ); ?>
						
					
				
				

			<?php endif; ?></div>
			<?php wp_reset_query(); ?> 
			<!-- now query for the tools taxonomy -->
			
			<!---------------------------------->
			
			<?php $tagged_two = query_posts('posts_per_page=100&post_type=plugin&tools=apps,integration,other,scripts&paged='.$paged_two.'&tag='.$term); ?>
				
				<div>
				<?php if ( $tagged_two ) : ?>    
					
				<?php while ( have_posts() ) : the_post(); ?>
					
					
					<?php	/* Include the Post-Format-specific template for the content.
						 * If you want to overload this in a child theme then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'content-package' ); ?>					

				<?php endwhile; ?>
				<?php twentyeleven_content_nav( 'nav-below' ); ?>
						
					  
				
				
			

			<?php endif;?> </div> <?php endif;?>
			<?php wp_reset_query(); ?> 
						</div>
				<?php endif; ?>		
						<?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>
					</div><!-- .entry-content -->
					<footer class="entry-meta">
						
	</footer><!-- .entry-meta -->
</article><!-- #post-<?php the_ID(); ?> -->
		
		</div>
	</div>
	<!-- this is the right sidebar-->

		<?php include('sidebar-right.php'); ?>
  </div>
<?php get_footer(); ?>

