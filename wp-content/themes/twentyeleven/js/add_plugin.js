jQuery(document).ready(function($) {	

	//jQuery for hiding and showing the correct checkboxes in the form	
	
	    $('#category_plugin').click(function() {
		  $('#subcategory-tool').css("display", "none");
	        $('#subcategory-plugin').fadeIn();	            
	        });
	    $('#category_tool').click(function() {
	       $('#subcategory-plugin').css("display", "none");       
	        $('#subcategory-tool').fadeIn();        
	    });

	// toggle upload method if checkbox is toggled
	$('input.download-from-repo').on('change',function(){
		if ($('#addon-file-uploader').css('display') == 'none') {
			$('#addon-file-uploader').slideDown();
		} else {
			$('#addon-file-uploader').slideUp();
		}
	});
		
		

	//Code to enable colorbox
	$('.colorbox').colorbox();

 });
 
 