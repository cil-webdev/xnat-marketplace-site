<?php
/**
 * Template Name: About Page
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */
 get_header(); ?>
 
 <div id="primary">
		<div id="sidebar_left">
	<?php	echo '<ul>';
		$args_list = array(
			'taxonomy' => 'plugins', // Registered tax name
			'title_li' => __( 'Plugins' ),
			'show_count' => true,
			'hierarchical' => true,
			'hide_empty' => false,
			'echo' => '1',
		);	 
		echo wp_list_categories($args_list);
		echo '</ul>'; ?>
		
		<?php	echo '<ul>';
		$args_list_two = array(
			'taxonomy' => 'tools', // Registered tax name
			'title_li' => __( 'Tools' ),
			'show_count' => true,
			'hierarchical' => false,
			'hide_empty' => false,
			'echo' => '1',
		);	 
		echo wp_list_categories($args_list_two);
		echo '</ul>'; ?>
			</div>		
			<div id="content" role="main">
			<?php the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>
			
			

			</div><!-- #content -->
			<div id="sidebar_right">
		<ul>
			<li><h3>Explore</h3></li>
			<li><a href="#">Endorsed by NRG(10)</a></li>
			<li><a href="#">Condor</a></li>
			<li><a href="#">Neurology</a></li>
			<li><a href="#">Cancer</a></li>
		</ul>
			</div>				
		</div><!-- #primary -->
		
		<?php get_footer(); ?>