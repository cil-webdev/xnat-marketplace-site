<?php

$custom_metabox = $simple_mb = new WPAlchemy_MetaBox(array
(
	'id' => '_custom_meta',
	'title' => 'Featured Homepage Addon ?',
	'template' => STYLESHEETPATH . '/metaboxes/plugin.php',
	'types' => array('plugin'),
	'context' => 'side',
	'priority' => 'default',
	'mode' => WPALCHEMY_MODE_EXTRACT,
    'prefix' => '_my_',
	'autosave' => TRUE
));


/* eof */
?>