Recent Activity - all entries grouped through the event

FOr audit trail
Each entry can be expanded to show all the edits to data and their time points
Columns for the expanded view: Time point, Action taken

Reasons typically not of interest

Examples of events that would not have any associated modifications to data
Locked, unlocked

At the project level:
shows changes to subsets of data and project-level modifications (e.g. project configuration options)

TO see "audit trail" modificaitons, you have to navigate to the session level.
Check terminology of "Audit Trail"... may not be necessary

No "date range" for this widget at this point

Project Level
Data modifications
Configuration
Accessibility

All the other levels:
Data modifications:
1. Pipeline Events
2. 

Individual data objects (links to what was modified) will actually appear in the expanded view of the data.

Build out Visit Sequence workflow
Need to update Ungrouped modal popup with the revisions approved for the other "pool" columsn
Build out the My Account page and the My Settings page where users can "block" access
