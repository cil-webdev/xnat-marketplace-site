<?php

$custom_metabox = $simple_mb = new WPAlchemy_MetaBox(array
(
	'id' => '_custom_meta',
	'title' => 'Modify Plugin Details',
	'template' => STYLESHEETPATH . '/metaboxes/plugin-fields.php',
	'types' => array('plugin'),
	'context' => 'normal',
	'priority' => 'default',
	'mode' => WPALCHEMY_MODE_EXTRACT,
    'prefix' => '_my_',
	'autosave' => TRUE
));


/* eof */
?>