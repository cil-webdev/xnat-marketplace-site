<?php
/* 
Template Name: Packages taxonomy Page
*list posts for the plugins taxonomy
*/
//taxonomy
/*$taxonomy = 'plugins';
$term = get_query_var($taxonomy);
$prod_term = get_terms($taxonomy, 'slug='.$term.''); 
$term_slug = $prod_term[0]->slug;
$term_id = $prod_term[0]->term_id;*/ 
$params = $_GET;
unset($params['orderby']);
unset($params['order']);		
$url = get_pagenum_link();
$url = strtok($url, '?');
$url =$url.'?'.http_build_query($params);
global $paged,$paged_two;
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$bytype = $_GET['orderby'];
//$term = $_GET['term'];
//$terms = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
$id = get_queried_object_id($term);
$saved_data = get_tax_meta($id,'image_field_id');

get_header();				


$term =	$wp_query->queried_object;
$term = $term->slug;
//echo '<h1>'.$term->slug.'</h1>';die();

?>


<!-- Page navigation-->

<!-- /Page navigation-->

<!--  page contents -->



<div id="primary">
<!-- this is the left sidebar-->



	<?php include('sidebar-left.php'); ?>
	<div id="content" role="main">
		<div id="content-container">				

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>				
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
					<div id="package-wrap">
						<div id="package-inner">					
							<img src="<?php echo $saved_data['src']; ?>">
							<div class="term-description">
								<h1><b style="font-size:20px;color:#000;font-weight: bold;">About this package</b></h1>
								<?php echo term_description(); ?>
							</div>
							<div class="clear"></div>
						</div>
					</div>
				</header><!-- .entry-header -->

				<div class="entry-content clear">

					<ul class="tabs">					
						<li>
							<a href="#"	 class="plugintab" data-id="Plugins" data-val="<?php  echo term_count_package_plugin($term); ?>">Plugins (<?php echo term_count_package_plugin($term); ?>)</a>
						</li>
						<li>
							<a href="#"	 class="tooltab" data-id="Tools" data-val="<?php  echo term_count_package_tool($term); ?>">Tools (<?php echo term_count_package_tool($term); ?>)</a>
						</li>
					</ul>
					<?php //if ( have_posts() ) : ?>	
						<?php
						$myquery = wp_parse_args($query_string);
						$myquery = array(
							'paged'=>$paged,
							'numberposts'=> -1,
							'tax_query' => array(						
								'relation' => 'AND',
								array(
									'taxonomy' => 'plugins',
									'terms' => array('admin','datatype','display','pipeline','other'),
									'field' => 'slug'
								),
								array(
									'taxonomy' => 'packages',
									'terms' => $term,
									'field' => 'slug'
								)
							)
						);

						$tagged_one = query_posts($myquery);	

						?>
						<div class="search_results">
							<div>
								<?php if ( $tagged_one ) : ?>	 

									<?php while ( have_posts() ) : the_post(); ?>

										<?php
										/* Include the Post-Format-specific template for the content.
										* If you want to overload this in a child theme then include a file
										* called content-___.php (where ___ is the Post Format name) and that will be used instead.
										*/
										get_template_part( 'content-package' ); ?>								

									<?php  endwhile; ?> 

									<?php twentyeleven_content_nav( 'nav-below' ); ?>
								<?php else: ?>
									Sorry, there are no matches for that selection.
								<?php endif; ?>
							</div>
							<?php wp_reset_query(); ?> 
							<!-- now query for the tools taxonomy -->

							<!---------------------------------->

							<?php
							$myquery = wp_parse_args($query_string);
							$myquery = array(
								'paged'=>$paged,
								'numberposts'=> -1,
								'tax_query' => array(						
									'relation' => 'AND',
									array(
										'taxonomy' => 'tools',
										'terms' => array('apps','integration','scripts','other'),
										'field' => 'slug'
									),
									array(
										'taxonomy' => 'packages',
										'terms' => $term,
										'field' => 'slug'
									)
								)
							);

							$tagged_two = query_posts($myquery);?>

							<div>
								<?php if ( $tagged_two ) : ?>	 

									<?php while ( have_posts() ) : the_post(); ?>

										<?php
										/* Include the Post-Format-specific template for the content.
										* If you want to overload this in a child theme then include a file
										* called content-___.php (where ___ is the Post Format name) and that will be used instead.
										*/
										get_template_part( 'content-package' ); ?>					
                            
									<?php endwhile; ?>
									<?php twentyeleven_content_nav( 'nav-below' ); ?>
								<?php else: ?>
									Sorry, there are no matches for that selection.
								<?php endif;?>
							</div> 
							<?php wp_reset_query(); ?> 
						</div>
					<?php //endif; ?>	
					
					<?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>
				</div><!-- .entry-content -->
				<footer class="entry-meta"></footer><!-- .entry-meta -->
			</article><!-- #post-<?php the_ID(); ?> -->

		</div>
	</div>
	<!-- this is the right sidebar-->

	<?php include('sidebar-right.php'); ?>
</div>
<?php get_footer(); ?>