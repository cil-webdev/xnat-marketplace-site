<!-- template: content-plugin.php -->

<?php /* The template for displaying content in the plugin single.php template */

$filesize = get_post_meta($post->ID, "upload_6", true); 
$kbsize = strlen(file_get_contents($filesize));
$truesize = cleansize($kbsize);
$icon = get_post_meta($post->ID, "upload_5", true);
$defaulticon = get_post_meta($post->ID, "defaulticon", true);
$item = get_post_meta($post->ID, "documentation");
$download = wp_get_attachment_link( $attachment->ID, false ); ?>

<h1 class="title page-title single-plugin-title" style="line-height:27px;"><?php the_title();  ?></h1>

<?php if ($post->post_status != 'publish') : ?>
<div class="plugin-pending">
    This add-on is still pending review by an XNAT admin. If you want to make any changes, click on the “Edit” link.
</div>
<?php endif; ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="articlewrap">
	    <div id="plugin-inner">
    	<div id="plugin-inner-left">
			<header class="entry-header">		
				<div class="downloadwrap clearfix">
					<?php if ( get_post_meta($post->ID, "download_from_repo", true) == "yes" ) : ?>
                        <p><a class="download" href="<?php echo get_post_meta($post->ID, "repo", true); ?>">Download From Repo</a></p>
                    <?php else : ?>
                        <?php
                        if( get_post_meta($post->ID, "upload_6", true) ):
                            $url = str_replace('http:','https:',get_post_meta($post->ID, "upload_6", true))
                            ?>
                            <p><a class="download" href="<?php echo $url ?>">Download Now</a></p>
                        <?php
                        endif;
                        ?>
                    <?php endif; ?>
				</div><!-- .downloadwrap -->
				<div class="entry-primary-details">
					<?php if ( 'post' == get_post_type() ) : ?>
                    <div class="entry-meta">
	                    <?php twentyeleven_posted_on(); ?>
                    </div><!-- .entry-meta -->
					<?php endif; ?>
                
					<h2 class="details-title" style="text-transform:none;line-height:15px;padding:0px 0px 10px 0px;">Works with XNAT versions:</h2>
                    <div class="access plugin-details">
                        <ul>
                        <?php
                        if( get_post_meta($post->ID, "compatible", true) ):
                            $compatible = array();
                            $compatible = get_post_meta($post->ID, "compatible", true);
                            $compatible_two = explode(',', $compatible);

                            foreach($compatible_two as $compatible_object) {
                                echo "<li>".$compatible_object."</li>";
                            }

                        endif; ?>
                        </ul>
                    </div>
                    <h2 class="details-title details-header">Details</h2>
                    <!-- <h2><strong>File Size: </strong><?php echo $truesize ?></h2> -->
                    <h2><strong>Uploaded By: </strong><?php the_author_posts_link(); ?></h2>
                    <h2>
                    	<strong>Category: </strong>
                        <span>
                        <?php 
                        $thecattool = get_post_meta($post->ID, "tax", true);
                        $toolterms = get_the_terms($post->ID, 'tools');
                        $pluginterms = get_the_terms($post->ID, 'plugins');
                        
                        if ($toolterms != false) {
                            echo get_the_term_list($post->ID, 'tools', '', ', ', '');
                        } else if ($pluginterms != false) {
                            echo get_the_term_list($post->ID, 'plugins', '', ', ', '');
                        } else {
                        }
                        //echo get_the_term_list( $post->ID, $thecattool,'', ', ', '' ); ?>
                        </span>
                    </h2>
                    <h2><strong>Tags: </strong><?php the_tags('', ', ', '<br />'); ?></h2>
                
                    <div class="details plugin-details">				
                        <ul>
                            <li>
                                <strong>Version: </strong>
                                <?php if( get_post_meta($post->ID, "version", true) ): echo get_post_meta($post->ID, "version", true); endif; ?>
                                <?php if( get_post_meta($post->ID, "alpha", true) ): echo ' ('.get_post_meta($post->ID, "alpha", true).')'; endif; ?>
                            </li>
                            <li>
                                <strong>License: </strong>
                                <?php
                                if( get_post_meta($post->ID, "license_custom", true) ): echo get_post_meta($post->ID, "license_custom", true);
                                elseif (get_post_meta($post->ID, "license",true)) : echo get_post_meta($post->ID, "license",true);
                                endif;
                                ?>
                            </li>

                            <li><strong>Upload Date: </strong><?php echo the_date();?></li>
                            <?php 
							$doc = get_post_meta($post->ID, "documentation", true);
                            if (!empty($doc)) { ?>
	                            <li><strong>View Documentation: </strong><a class="pdfdl" href="<?php echo $doc; ?>" target="_blank">LINK</a></li>
                            <?php } ?>
                        </ul>
                    </div><!-- .plugin-details -->
					<div class="access plugin-details">			
						<h2><strong>This Plugin Accesses:</strong></h2>
                        <ul>								
                        <?php
							if( get_post_meta($post->ID, "item", true) ):
								$item = array();				
								$item = get_post_meta($post->ID, "item", true);
								$item_two = explode(',', $item);
								$object_lookup = array(
								  'velocity-html' => 'Velocity/HTML Template',
								  'database' => 'Database',
								  'java' => 'Java Classes',
								  'javascript' => 'Javascript Functions',
								  'css' => 'CSS Style Sheet',
								  'project-dependencies' => 'Project Dependencies',
								  'spring' => 'Spring Configuration Files'
								);
								foreach($item_two as $object) {
								  $object = $object_lookup[$object];
								  echo '<li> '. $object .'</li> '; 
								}
							endif; ?>
                        </ul>
					</div><!-- .plugin-details -->
                </div>
			</header><!-- .entry-header -->
		</div>
     <div id="plugin-inner-right">
     
     <?php edit_post_link('Edit Add-On', '<p class="edit-wrapper">', '</p>'); ?>
     
     
     
	 <div id="author-avatar">
         <?php
         $avatar = ($icon !== '')? $icon : $defaulticon;
         $avatar = str_replace('http:','https:',$avatar);
         ?>
      <img class="icons" src="<?php echo $avatar ?> ">
    </div><!-- #author-avatar -->
	<div class="entry-content">	
		
		<ul id="plugin-details-array">		
		<!-- List post credits -->	
		<li>
			<div class="credits plugin-details">
				<h2><strong>Credits:</strong></h2>	
				<?php if( get_post_meta($post->ID, "credits", true) ): 
					  echo get_post_meta($post->ID, "credits", true);
				endif; ?>
			</div>
		</li>
		<!-- List post Description -->
		<li>
			<div class="description plugin-details">
				<h2><strong>Description:</strong></h2>
				<?php the_content(); ?>
			</div>
		</li>
		<!-- List post environment -->
		<?php if( get_post_meta($post->ID, "env_description", true) ): ?>
		<li>
			<div class="environment plugin-details">
				<h2><strong>Environment where add-on is currently running:</strong></h2>
				
					<?php  echo get_post_meta($post->ID, "env_description", true); ?>
					
			</div>
		</li>
		<?php endif; ?>

        <?php if (get_post_meta($post->ID, "repo", true)): ?>
        <li>
            <div class="repo plugin-details">
                <h2><strong>Link to Source Repository</strong></h2>
                <p><a href="<?php echo get_post_meta($post->ID, "repo", true); ?>" target="_blank"><?php echo get_post_meta($post->ID, "repo", true); ?></a></p>
            </div>
        </li>
        <?php endif; ?>
		<!-- List Details -->
		
	  <!-- List what this plugin accesses -->
	 
	  </ul>
	 
	  
		<?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->

	<footer class="entry-meta">
		<?php
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( __( ', ', 'twentyeleven' ) );

			/* translators: used between list items, there is a space after the comma */
			$tag_list = get_the_tag_list( '', __( ', ', 'twentyeleven' ) );
			if ( '' != $tag_list ) {
				$utility_text = __( 'This entry was posted in %1$s and tagged %2$s by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyeleven' );
			} elseif ( '' != $categories_list ) {
				$utility_text = __( 'This entry was posted in %1$s by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyeleven' );
			} else {
				$utility_text = __( 'This entry was posted by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyeleven' );
			}

			/*printf(
				$utility_text,
				$categories_list,
				$tag_list,
				esc_url( get_permalink() ),
				the_title_attribute( 'echo=0' ),
				get_the_author(),
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) )
			);*/
		?>		

		<?php if ( get_the_author_meta( 'description' ) && is_multi_author() ) : // If a user has filled out their description and this is a multi-author blog, show a bio on their entries ?>
		<div id="author-info">
			
			<div id="author-description">
				<h2><?php printf( esc_attr__( 'About %s', 'twentyeleven' ), get_the_author() ); ?></h2>
				<?php the_author_meta( 'description' ); ?>
				<div id="author-link">
					<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">
						<?php printf( __( 'View all posts by %s <span class="meta-nav">&rarr;</span>', 'twentyeleven' ), get_the_author() ); ?>
					</a>
				</div><!-- #author-link	-->
			</div><!-- #author-description -->
		</div><!-- #entry-author-info -->
		<?php endif; ?>
	</footer><!-- .entry-meta -->
	</div>
		</div> <!--#plugininner-->
	</div><!-- .articlewrap -->
</article>
<script type="text/javascript">
  (function($) {
     // this corrects a CSS issue with the left side of the details
     $('article.type-plugin.plugin, article.plugin>div, article.plugin>div>div').height($('article.type-plugin.plugin').height() + 40);
   })(jQuery);
</script>

<!-- end template: content-plugin.php -->