<div id="sidebar_left">
<div id="add_plugin"><a class="submit-addon <?php if ( is_user_logged_in() ) {echo "";} else {echo "simplemodal-login";} ?>" href="<?php if ( is_user_logged_in() ) { echo home_url('/add-plugin/'); } else { echo wp_login_url( home_url( '/add-plugin/' ) ); } ?>"></a></div>
			
			<?php $plugins_terms = get_terms( 'plugins','fields=names' ); ?>
		
			<?php $tools_terms = get_terms( 'tools', 'orderby=ASC&hide_empty=0'); ?>
			
			
			<?php
			$args_two = array(
			'post_type' => 'plugin',
			'post_status' => 'published',			
			'plugins' => array($plugins_terms)
			);
			//$num_two = count( query_posts( $args_two ) );	
			
			?>
			
			<!-- the above content displays the number of tags found in the taxonomy, we need the number of posts in the taxonomy -->
			<?php 

 // Perform query to display the number of returned posts
	$checknum1 = query_posts('posts_per_page=-1&post_type=plugin&plugins=admin,datatype,display,other,pipeline'); ?>

	
	<?php 
		$i = 0;
		if ( $checknum1 ) : 
	 		while ( have_posts() ) :
				the_post(); 
	 			$i++;
			endwhile;
		endif; 
	wp_reset_query(); ?>
			
			<h3 id="sidebar-plugins">
            <a href="<?php bloginfo('siteurl'); ?>/plugins/" title="Plugins">Plugins (<?php echo $i;?>)</a>
            </h3>
		
			<div class="sidebar_divider"></div>
		<ul>
		<?php 
		$args_list = array(
			'taxonomy' => 'plugins', // Registered tax name			
			'show_count' => true,
			'hierarchical' => false,
			'hide_empty' => false,
			'echo' => '1',	
			'exclude' =>  '6'
		);
		$args_list_other = array(
			'taxonomy' => 'plugins', // Registered tax name			
			'show_count' => true,
			'hierarchical' => false,
			'hide_empty' => false,
			'echo' => '1',	
			'include' =>  '6'
		);
		
		
		 
			 $categories = get_categories($args_list);
			 foreach($categories as $category) { 	
  			 $category_id = $category->cat_ID;	
  			 echo '<li><a href="'. get_term_link($category->slug, 'plugins').'">'. $category->name . '&nbsp('.$category->count.') </a> </li>';
			 }
			
			 $categories = get_categories($args_list_other);			
			 foreach($categories as $category) { 	
			   $category_id = $category->cat_ID;				
			   echo '<li><a href="'. get_term_link($category->slug, 'plugins').'">'. $category->name . '&nbsp('.$category->count.') </a> </li>';
			 }
		?>
			 
		
		</ul>
			<div class="sidebar_divider"></div>
		
			
			<?php 
			$checknum2 = query_posts('&posts_per_page=-1&post_type=plugin&tools=apps,integration,other,scripts'); ?>

			
			<?php
				$z = 0;
				if ( $checknum2 ) :
			 		while ( have_posts() ) : 
						the_post(); 
						$z++;
					endwhile;
				endif; 
				wp_reset_query();
			?> 
			<h3 id="sidebar-tools"><a href="<?php bloginfo('siteurl'); ?>/tools/">Tools (<?php echo $z;?>)</a></h3>
			<div class="sidebar_divider"></div>
		<ul>
		<?php
		
		//*********** get the categories**********************//
		
		$args_list_two = array(
			'taxonomy' => 'tools', // Registered tax name
			'title_li' => __( 'Tools' ),
			'show_count' => true,
			'hierarchical' => false,
			'hide_empty' => false,
			'echo' => '1',
			'exclude' =>  '6'
		);	 
		$args_list_two_other = array(
			'taxonomy' => 'tools', // Registered tax name
			'title_li' => __( 'Tools' ),
			'show_count' => true,
			'hierarchical' => false,
			'hide_empty' => false,
			'echo' => '1',
			'include' =>  '6'
		);	 
		$categories = get_categories($args_list_two);
			 foreach($categories as $category) { 
			 echo '<li><a href="'. get_term_link($category->slug, 'tools').'">'. $category->name . '&nbsp('.$category->count.')</a></li>'; }
			//now pull the other category
			$categories = get_categories($args_list_two_other);
			 foreach($categories as $category) { 
			 echo '<li><a href="'. get_term_link($category->slug, 'tools').'">'. $category->name . '&nbsp('.$category->count.')</a></li>'; } 
			 ?>
		</ul>
			</div>