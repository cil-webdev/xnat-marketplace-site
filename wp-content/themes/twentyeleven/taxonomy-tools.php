<?php
/* 
Template Name: Tools taxonomy Page
*list posts for the plugins taxonomy
*/
//taxonomy
/*$taxonomy = 'plugins';
$term = get_query_var($taxonomy);
$prod_term = get_terms($taxonomy, 'slug='.$term.''); 
$term_slug = $prod_term[0]->slug;
$term_id = $prod_term[0]->term_id;*/ 
$params = $_GET;
	unset($params['orderby']);
	unset($params['order']);		
	$url = get_pagenum_link();
	$url = strtok($url, '?');
	$url =$url.'?'.http_build_query($params);
global $paged,$paged_two;
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$bytype = $_GET['orderby'];
get_header();				
?>
<?php wp_enqueue_script( 'cycle', get_template_directory_uri() . '/js/jquery.cycle.all.js', array( 'jquery'));?>

    <!-- Page navigation-->
     
    <!-- /Page navigation-->
   
    <!--  page contents -->
	
	
	<section id="primary">
		  <!-- this is the left sidebar-->

		<?php include('sidebar-left.php'); ?>
			<div id="content" role="main">
                <div id="content-container" class="clearfix">
                    <h1 class="title page-title clearfix"><?php echo $term; ?></h1>
                    
                    <?php if ( have_posts() ) : ?>
                    <div class="tabsubmenu clearfix">	
                        <div class="sort_by">
                            Sort: 
                            <a href="<?php echo $url;?>&orderby=date&order=ASC" <?php if($bytype =='date' || $bytype== '') {echo 'class="currentlyActive"';} ?>>By Date</a>
                            <span>|</span>
                            <a href="<?php echo $url ;?>&orderby=title&order=ASC" <?php if($bytype =='title') {echo 'class = "currentlyActive"';} ?>>By Name</a>
                        </div><!-- .tabsubmenu -->
                        
                        <div class="nrg-filter">
                            <input type="checkbox" class="nrgsorted" value="nrg-approved" name="nrgsorted" />
                            <label for="nrgsorted"> NRG Approved</label>
                        </div><!-- .nrg-filter -->
	                </div><!-- -->
                    
                    <div class="clear"></div>
                    
					<?php 
                    while ( have_posts() ) : the_post();
                        get_template_part( 'content-package' );
                    endwhile;
                    else : ?>
                
                    <div class="entry-content">
                        <p><?php echo __('Sorry, there are no matches for that selection.','twentyeleven'); ?></p>
                    </div>
                        
                <?php
                endif; ?>	
                </div><!-- #content-container -->
            </div><!-- #content -->
    
	<!-- this is the right sidebar-->

<?php include('sidebar-right.php'); ?>
</section>
    
<?php get_footer();?>