<?php
/**
 * The template used for displaying SLIDER content 
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>
			
			<?php $slides = $data['primary-slider']; 
					foreach ($slides as $slide) { ?>
				<li> 
					<img src="<?php echo $slide['url'];?>" alt="<?php echo $slide['title']?>"/>
					<div>	
						<h3><?php echo $slide['title'];?></h3>
						<p><?php echo $slide['description']; ?></p>
						<?php if($slide['link'] !== '' ){
						echo '<p>'. $slide['link'] . '</p>'; 
						} 
												}?>
					</div>
				</li>
				
				