<?php
/**
 * Template Name: Contact Page
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>
<?php wp_enqueue_script( 'watermark', get_template_directory_uri() . '/js/jquery.watermark.min.js', array('jquery'));  ?>

		<div id="primary">
		<?php include('sidebar-left.php'); ?>
		
			<div id="content" role="main">
			
			<div id="contact_form">
			<header class="entry-header">
			<h1 class="entry-title"><?php the_title(); ?></h1>
			</header>
			<div class="entry-content">
			 <?php the_content(); ?>
			
				<?php echo do_shortcode('[contact-form-7 id="104" title="Contact form 1"]'); ?>
			</div>
			</div>
			

			</div><!-- #content -->
		
		
	 <!-- this is sidebar right -->	
	<?php include('sidebar-right.php'); ?>	
	</div><!-- #primary -->		

<?php get_footer(); ?>