<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>

	</div><!-- #main -->
		<div class="clear"></div>
	<footer id="colophon" role="contentinfo">

			<?php
				/* A sidebar in the footer? Yep. You can can customize
				 * your footer with three columns of widgets.
				 */
				get_sidebar( 'footer' );
			?>
			<div class="footer-divider"></div>
			<div id="site-footer">
			<?php
        if (function_exists('wp_nav_menu')) {	
          wp_nav_menu(array('theme_location' => 'footer-menu', 'container' => false, 'menu' => 'footer-menu', 'after' => ' <span>|</span> '));
        }
      ?>
			</div>
			<div class="footer-divider"></div>
			
			<div id="copyright">			

				<p><?php global $data; echo stripslashes($data['copyright-text']);?></p>
			</div>
	</footer><!-- #colophon -->
  </div><!-- #page -->
</div><!-- #wrapper -->
<?php wp_footer(); ?>
<!--<script type="text/javascript" src="https://issues.xnat.org/s/en_US-im1s0q-1988229788/6100/3/1.4.0-m3/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?collectorId=9a779a93"></script>-->
<!--<script type="text/javascript">window.ATL_JQ_PAGE_PROPS =  {-->
<!--        "triggerFunction": function(showCollectorDialog) {-->
<!--            //Requries that jQuery is available!-->
<!--            jQuery("#myCustomTrigger").click(function(e) {-->
<!--                e.preventDefault();-->
<!--                showCollectorDialog();-->
<!--            });-->
<!--        }};</script>-->
<script>
    // Requires jQuery!
    jQuery.ajax({
        url: "https://issues.xnat.org/s/7dd84e0039c8e4077982b07388626e34-T/en_USu101to/64017/124/1.4.25/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs.js?locale=en-US&collectorId=9a779a93",
        type: "get",
        cache: true,
        dataType: "script"
    });

    window.ATL_JQ_PAGE_PROPS =  {
        "triggerFunction": function(showCollectorDialog) {
            //Requires that jQuery is available!
            jQuery("#myCustomTrigger").click(function(e) {
                e.preventDefault();
                showCollectorDialog();
            });
        }};

</script>

<!-- Google Analytics -->
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-1775166-8', 'auto');
	ga('send', 'pageview');

</script>

</body>
</html>