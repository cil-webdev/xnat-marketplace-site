=== Front End Upload ===
Contributors: jchristopher
Donate link: http://mondaybynoon.com/donate/
Tags: upload, front end, front, end, media, picture, pictures
Requires at least: 3.2
Tested up to: 3.3.1
Stable tag: 0.4

Provides the most basic implementation allowing site visitors to upload files to the Media library and notify admin

== Description ==

Front End Upload aims to make it easier to accept file uploads from website visitors without needing to deal with user accounts or any overbearing requirements. It's built upon Plupload and the WordPress Media library.

== Installation ==

1. Upload the `front-end-upload` folder to your `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Edit your options under **Settings > Front End Upload**
1. Place `[front-end-upload]` in the editor where you would like the uploader to appear
1. Customize your CSS

== Screenshots ==

1. The Front End Upload Options screen

== Changelog ==

= 0.4 =
* Added {@ip} to the available message tags
* Added more opportunities for localization

= 0.3 =
* Completely changed the way uploads are handled. They are no longer sent to the WordPress Media library and are instead stored in a subdirectory of `~/wp-content/uploads/`
* If a passcode is set, it must now be entered before any uploading can take place
* Email validation takes place via JavaScript prior to the upload beginning
* Support for large files has been added

= 0.2 =
* Added custom mime types to the options screen
* Fixed potential issue with unset max file size

= 0.1 =
* Initial release
