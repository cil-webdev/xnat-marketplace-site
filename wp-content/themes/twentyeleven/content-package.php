<!-- template: content-package.php -->

<?php

$filesize = get_post_meta($post->ID, "upload_6", true); 
$kbsize = strlen(file_get_contents($filesize));
$truesize = cleansize($post->ID);
$truesize = ($truesize) ? $truesize : 'Not Found';

if(get_post_meta( $post->ID, 'nrgapproved',true )) {
	$has_term = 'nrg-approved';
} else {
	$has_term = "not-nrg";
} ?>

<article id="post-<?php the_ID(); ?>" <?php post_class($has_term); ?>>
    <span style="display:none">
        <?php the_terms( $post->ID, '' ); ?>
    </span>
	<div class="search-divider"></div>
	<!--<header class="entry-header"></header>-->
		<div class="entry-summary">		 
			<ul class="clearfix">
				<li> 
					<div class="featured_post_wrap">
						<div class="featured-image">
							<?php
								$icon = get_post_meta($post->ID, "upload_5", true);
								$defaulticon = get_post_meta($post->ID, "defaulticon", true);
                                $avatar = ($icon !== '')? $icon : $defaulticon;
                                $avatar = str_replace('http:','https:',$avatar);
							?>
							<img class="icons" src="<?php echo $avatar; ?>"/>
							<p style="font-size: 10px;color: #3D3D3D;">Works With: XNAT 
								<?php
									if( get_post_meta($post->ID, "compatible", true) ):
										$compatible = array();				
										$compatible = get_post_meta($post->ID, "compatible", true);
										$compatible_two = explode(',', $compatible);
										$i = 0;
										foreach($compatible_two as $compatible_object) {
											if($i > 0){
												echo '<span class="arraydelimiter">, </span> ';
											}
											echo $compatible_object;
											++$i;
										}
									endif;
								?>
							</p>						 
						</div><!-- #author-avatar --> 
						<div class="featured-content">
							<?php
								$link = get_permalink();
								if(is_tax('tools')){
									$link = str_replace('plugin/', 'tool/', $link);
								}
							?>
							<a class="featured_title" href="<?php echo $link; ?>" rel="bookmark"><?php the_title(); ?><?php if( has_term( 'nrg-approved','packages')) {echo '<img src="'.get_template_directory_uri().'/images/approved.png">';}?></a>
							<?php print_excerpt(140); ?>
							<div class="featured-content-left">
								<p class="author">Uploaded By: <span>	 <?php the_author_posts_link(); ?></span></p>
								<p class="category">
                                	Category: 
                                    <span>
									<?php 
									$thecattool = get_post_meta($post->ID, "tax", true);
									$toolterms = get_the_terms($post->ID, 'tools');
									$pluginterms = get_the_terms($post->ID, 'plugins');
									
									if ($toolterms != false) {
										echo get_the_term_list($post->ID, 'tools', '', ', ', '');
									} else if ($pluginterms != false) {
										echo get_the_term_list($post->ID, 'plugins', '', ', ', '');
									} else {
									}
									//echo get_the_term_list( $post->ID, $thecattool,'', ', ', '' ); ?>
                                    </span>
                                </p>
								<p class="tags">Tags: <span><a href="#">
									<?php
										$posttags = the_tags('', ', ', '<br />');
										$counttag=0;
										if ($posttags) {
											foreach($posttags as $tag) {
												$counttag++;
												echo $tag->name .', ';
												if( $counttag >3 ) break;
											}
										}
									?>
									</a></span>
								</p>		
								<p class="date">Upload date: <span><?php the_time(get_option('date_format')); ?></span></p>
							</div><!--end .featured-content-left-->
							<div class="featured-content-right">
								<p class="downloadwrap">
									<?php if( get_post_meta($post->ID, "download_from_repo", true) == "yes") : ?>
										<a class="download" href="<?php echo get_post_meta($post->ID,"repo",true) ?>">Download From Repo</a>
									<?php else : ?>
                                        <?php
                                        if( get_post_meta($post->ID, "upload_6", true) ):
                                            $url = str_replace('http:','https:',get_post_meta($post->ID, "upload_6", true))
                                            ?>
                                            <a class="download" href="<?php echo $url ?>">Download Now</a>
                                        <?php
                                        endif;
                                        ?>
									<?php endif; ?>
								</p>
							</div><!-- .featured-content-right -->
						</div><!-- .featured-content -->
					</div><!-- .featured-content-wrap  -->
				</li>
			</ul>
		</div><!-- .entry-summary -->
	<!--<footer class="entry-meta"></footer>--><!-- #entry-meta -->
</article>

<!-- end template: content-package.php -->