<?php
/**
* The template for displaying Author Archive pages.
*
* @package WordPress
* @subpackage Twenty_Eleven
* @since Twenty Eleven 1.0
*/

global $paged,$paged_two,$query_string;
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$paged_two = (get_query_var('paged')) ? get_query_var('paged') : 1;

get_header(); ?>


<section id="primary">
<!-- this is the left sidebar-->

	<?php include('sidebar-left.php'); ?>

	<div id="content" role="main">
		<?php if ( is_author() ) : $current_author = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author)); ?>

		<header class="entry-header">
        	<div class="contentwrap clearfix">
                <div id="author-avatar">
                    <?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'twentyeleven_author_bio_avatar_size', 94 ) ); ?>
                </div><!-- #author-avatar -->
                <div class="author-right">
                    <h1 class="page-title author"><?php echo $current_author->display_name; ?></h1>

                    <div id="author-description">
                        <h3 class="description">Bio</h3><p><?php echo $current_author->description; ?></p>
                        <h3 class="institution">Institution</h3><?php the_author_meta('institution', $current_author->ID); ?>
                        <h3 class="contact">Contact</h3><?php echo $current_author->user_email; ?>
                    </div><!-- #author-description	-->
                </div>
            </div><!-- .author-wrapper -->
        </header>

			<?php
				/* Since we called the_post() above, we need to
				* rewind the loop back to the beginning that way
				* we can run the loop properly, in full.
				*/
				//rewind_posts();

				// If a user has filled out their description, show a bio on their entries.
			?>
			<div class="clear"></div>

            <?php
            // Perform query to display the number of returned posts
			$querynum1 = query_posts($query_string . '&plugins=admin,datatype,display,other,pipeline&posts_per_page=-1&post_type=plugin');
			$searchk = 0;
			if ( $querynum1 ) : while ( have_posts() ) : the_post();
				$searchk++;
			endwhile; endif; wp_reset_query();

			$querynum2 = query_posts($query_string . '&tools=apps,integration,other,scripts&posts_per_page=-1&post_type=plugin');
			$searchj = 0;
			if ( $querynum2 ) : while ( have_posts() ) : the_post();
				$searchj++;
			endwhile; endif; wp_reset_query(); ?>

			<ul class="tabs" id="author-nav">
				<li><a href="#"	data-id="Plugins" class="plugins-tab" data-val="<?php echo $searchk; ?>">Plugins (<?php echo $searchk; ?>)</a></li>
				<li><a href="#"	data-id="Tools" class="tools-tab" data-val="<?php echo $searchj; ?>">Tools (<?php echo $searchj; ?>)</a></li>
			</ul>

			<?php /* Start the Loop */

				$query = query_posts($query_string . '&posts_per_page=100&post_type=plugin&plugins=admin,datatype,display,other,pipeline&paged='.$paged);

			?>
			<div class="search_results">
				<div>
					<?php
						if ( $query ) :

							while ( have_posts() ) :
								the_post();
								/* Include the Post-Format-specific template for the content.
								* If you want to overload this in a child theme then include a file
								* called content-___.php (where ___ is the Post Format name) and that will be used instead.
								*/
								get_template_part( 'content', get_post_format() );
							endwhile;

							twentyeleven_content_nav( 'nav-below' );
						endif;
					?>
				</div>
				<?php wp_reset_query(); ?>
				<!-- now query for the tools taxonomy -->

				<?php
					$query_two = query_posts($query_string . '&posts_per_page=100&post_type=plugin&tools=apps,integration,other,scripts&paged='.$paged_two);
				?>
				<div>

					<?php
						if ( $query_two ) :
							while ( have_posts() ) : the_post();

								/* Include the Post-Format-specific template for the content.
								* If you want to overload this in a child theme then include a file
								* called content-___.php (where ___ is the Post Format name) and that will be used instead.
								*/
								get_template_part( 'content', get_post_format() );
							endwhile;

							twentyeleven_content_nav( 'nav-below' );
						endif;
					?>
				</div>
			</div>
		<?php
			else:
				echo '... no posts';
			endif;
		?>

	</div><!-- #content -->
	<!-- this is the right sidebar-->

	<?php include('sidebar-right.php'); ?>
</section><!-- #primary -->


<?php get_footer(); ?>