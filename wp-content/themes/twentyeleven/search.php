<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>


<?php  
	global $query_string;
	$params = $_GET;
	unset($params['orderby']);
	unset($params['order']);		
	$url = get_pagenum_link();
	$url = strtok($url, '?');
	$url =$url.'?'.http_build_query($params);
	$i = 0;
	$z = 0;
	$searchk = 0;
	$searchj = 0;
	global $paged,$paged_two;
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$paged_two = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$bytype = $_GET['orderby'];
	
?>	

<?php 

 // Perform query to display the number of returned posts
	$querynum1 = query_posts($query_string . '&plugins=admin,datatype,display,other,pipeline&posts_per_page=-1&post_type=plugin'); ?>

	
	<?php if ( $querynum1 ) : ?>    
				
		<?php while ( have_posts() ) : the_post(); ?>
			<?php $searchk++; ?>
		 <?php 	 
			endwhile; endif; ?>		
			 
	<?php wp_reset_query(); ?> 
	
	<?php 
	$querynum2 = query_posts($query_string . '&tools=apps,integration,other,scripts&posts_per_page=-1&post_type=plugin'); ?>

	
	<?php if ( $querynum2 ) : ?>    
				
		<?php while ( have_posts() ) : the_post(); ?>
			<?php $searchj++; ?>
		 <?php 	 
			endwhile; endif; ?>	
			<?php wp_reset_query(); ?> 
	<!-- end query -->
	
		<section id="primary">
		  <!-- this is the left sidebar-->

		<?php include('sidebar-left.php'); ?>
			<div id="content" role="main">

			<?php if ( have_posts() ) : ?>
				
				<!--search results title field-->
				<header class="page-header">
					
				</header>

				
				<h1 class="page-title">
                    <?php echo __('Search results for ','')."<span>'</span>".get_search_query()."<span>'</span>"; ?>
                <!-- end of page title -->
                </h1>
				<ul class="tabs">					
						<li><a id ="t1" class="plugintab" href="#plugins"  data-id="Plugins" data-val="<?php  echo $searchk; ?>">Plugins (<?php  echo $searchk; ?>)</a></li>
						<li><a id ="t2"  class="tooltab" href="#tools"  data-id="Tools" data-val="<?php  echo $searchj; ?>">Tools (<?php echo $searchj; ?>)</a></li>
				</ul>
				  <div class="tabsubmenu">	
					<div class="sort_by">Sort:  <a href="<?php echo $url;?>&orderby=date&order=DESC" <?php if($bytype =='date' || $bytype== '') {echo 'class="currentlyActive"';} ?>>By Date</a><span>|</span><a href="<?php echo $url ;?>&orderby=title&order=ASC" <?php if($bytype =='title') {echo 'class = "currentlyActive"';} ?> >By Name</a></div>
						
					<div class="filter-tag">
					<label for="filter" class="filterlabel">Filter By:</label>					
					<select name='filter' id='filter-postform' class='postform'>
						<option></option>
						<option value="admin">Admin</option>
						<option value="datatype">Datatype</option>
						<option value="display">Display</option>
						<option value="other">Other</option>
						<option value="pipeline">Pipeline</option>
						
					</select>	
					</div>
					<div class="nrg-filter"><label for="nrgsorted">
					 <input type="checkbox" class="nrgsorted" value="nrg-approved" name="nrgsorted"> NRG Approved</label>
					</div>	
				  </div>
				  <div class="clear"></div>
								
				<?php /* Start the Loop */ ?>
				
				<?php				
				$query_one = query_posts($query_string . '&plugins=admin,datatype,display,other,pipeline&posts_per_page=100&post_type=plugin&order=DESC&paged='.$paged); 
				
				?>
				<div class="search_results">
							<div>						
				<?php if ( $query_one ) : ?>    
					
				<?php while ( have_posts() ) : the_post(); ?>
					
					
					<?php	/* Include the Post-Format-specific template for the content.
						 * If you want to overload this in a child theme then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
				
						get_template_part( 'content', get_post_format() ); ?>								
					
				<?php  endwhile; ?>	
				
				<?php twentyeleven_content_nav( 'nav-below' ); ?>
						
					
				
				

			<?php endif; ?></div>
			<?php wp_reset_query(); ?> 
			<!-- now query for the tools taxonomy -->
			
			<!---------------------------------->
			
			<?php 				
			$query_two = query_posts($query_string . '&tools=apps,integration,other,scripts&posts_per_page=100&post_type=plugin&order=DESC&paged='.$paged); ?>
				<div>
				
				<?php if ( $query_two ) : ?>    
					
				<?php while ( have_posts() ) : the_post(); ?>
					
					
					<?php	/* Include the Post-Format-specific template for the content.
						 * If you want to overload this in a child theme then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'content', get_post_format() ); ?>					

				<?php endwhile; ?>
				<?php twentyeleven_content_nav( 'nav-below' ); ?>
						
					  
				
				
			

			<?php endif;?> </div> 
			<?php wp_reset_query(); ?> 
			
			</div><?php endif;?>	
					
				
				
				<?php if ( !have_posts()) : ?>

				
					
						<h1 class="page-title">
                    <?php echo __('Search results for ','')."<span>'</span>".get_search_query()."<span>'</span>"; ?>
                <!-- end of page title -->
                </h1>
					

					<div class="entry-content">
						<p><?php _e( 'Sorry, there are no matches for that selection.', 'twentyeleven' ); ?></p>
						
					</div><!-- .entry-content -->
				
					
			<?php endif; ?>
				

				
			</div><!-- #content -->
			
		<!-- this is the right sidebar-->

		<?php include('sidebar-right.php'); ?>
		</section><!-- #primary -->
 

<?php get_footer(); ?>