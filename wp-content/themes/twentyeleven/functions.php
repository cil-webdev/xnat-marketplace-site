<?php
ini_set('error_reporting', E_ALL);
/*********************************************************************
/ URI shortcuts
/*********************************************************************/
define( 'INT_CSS', get_template_directory_uri() . '/css/', true ); // Shortcut to point to the /css/ URI
define( 'INT_IMAGES', get_template_directory_uri() . '/css/img', true ); // Shortcut to point to the /css/images/ URI
define( 'INT_JS', get_template_directory_uri() . '/js', true ); // Shortcut to point to the /js/ URI
define( 'INT_LIBRARY', TEMPLATEPATH ); // Shortcut to point to the root
define( 'embedded_path', get_template_directory_uri() .'/includes');

/*********************************************************************
/  Includes and requires
/*********************************************************************/
require_once STYLESHEETPATH . '/includes/slidermanager.php';
require_once STYLESHEETPATH . '/includes/Tax-meta-class/Tax-meta-class.php';
require_once STYLESHEETPATH . '/includes/featured-posts-admin.php';


/*********************************************************************
Define our rewrite rules
 *********************************************************************/
add_filter( 'rewrite_rules_array','xnat_insert_rewrite_rules' );
add_filter( 'query_vars','xnat_insert_query_vars' );
add_action( 'wp_loaded','xnat_flush_rules' );

// flush_rules() if our rules are not yet included
function xnat_flush_rules(){
    $rules = get_option( 'rewrite_rules' );

    if ( ! isset( $rules['tool/(.+)$'] ) ) {
        global $wp_rewrite;
        $wp_rewrite->flush_rules();
    }
}

function xnat_insert_rewrite_rules( $rules ){
    $newrules = array();
    $newrules['tool/(.+)$'] = 'index.php?plugin=$matches[1]&is_tool=y';
    return $newrules + $rules;
}

// Adding the id var so that WP recognizes it
function xnat_insert_query_vars( $vars ){
    array_push($vars, 'is_tool');
    return $vars;
}


/*********************************************************************
Update the canonical handler to match our custom rewrite above
 *********************************************************************/
remove_action('wp_head', 'rel_canonical');
add_action('wp_head', 'xnat_rel_canonical');
function xnat_rel_canonical() {
    $queried_post_type = get_query_var('post_type');
    $is_tool = get_query_var('is_tool');
    if(is_single() && $queried_post_type == 'plugin' && $is_tool == 'y'){
        global $post;
        $perma = get_permalink($post->ID);
        $perma = str_replace('plugin/', 'tool/', $perma);
        echo '<link rel="canonical" href="'.$perma.'" />'."\n";
    } else {
        rel_canonical();
    }
}

/*********************************************************************
/  Create Library URL
/*********************************************************************/
function library_url($path = '') {
    $url = embedded_path;
    if ( 0 === strpos($url, 'http') && is_ssl() )
        $url = str_replace( 'http://', 'https://', $url );

    if ( !empty($path) && is_string($path) && strpos($path, '..') === false )
        $url .= '/' . ltrim($path, '/');

    return apply_filters('library_url', $url, $path);
}

/**
 * Twenty Eleven functions and definitions
 *
 * Sets up the theme and provides some helper functions. Some helper functions
 * are used in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * The first function, twentyeleven_setup(), sets up the theme by registering support
 * for various features in WordPress, such as post thumbnails, navigation menus, and the like.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are instead attached
 * to a filter or action hook. The hook can be removed by using remove_action() or
 * remove_filter() and you can attach your own function to the hook.
 *
 * We can remove the parent theme's hook only after it is attached, which means we need to
 * wait until setting up the child theme:
 *
 * <code>
 * add_action( 'after_setup_theme', 'my_child_theme_setup' );
 * function my_child_theme_setup() {
 *	   // We are providing our own filter for excerpt_length (or using the unfiltered value)
 *	   remove_filter( 'excerpt_length', 'twentyeleven_excerpt_length' );
 *	   ...
 * }
 * </code>
 *
 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
    $content_width = 584;

/**
 * Tell WordPress to run twentyeleven_setup() when the 'after_setup_theme' hook is run.
 */
add_action( 'after_setup_theme', 'twentyeleven_setup' );

if ( ! function_exists( 'twentyeleven_setup' ) ):
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which runs
     * before the init hook. The init hook is too late for some features, such as indicating
     * support post thumbnails.
     *
     * To override twentyeleven_setup() in a child theme, add your own twentyeleven_setup to your child theme's
     * functions.php file.
     *
     * @uses load_theme_textdomain() For translation/localization support.
     * @uses add_editor_style() To style the visual editor.
     * @uses add_theme_support() To add support for post thumbnails, automatic feed links, and Post Formats.
     * @uses register_nav_menus() To add support for navigation menus.
     * @uses add_custom_background() To add support for a custom background.
     * @uses add_custom_image_header() To add support for a custom header.
     * @uses register_default_headers() To register the default custom header images provided with the theme.
     * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
     *
     * @since Twenty Eleven 1.0
     */
    function twentyeleven_setup() {

        /* Make Twenty Eleven available for translation.
         * Translations can be added to the /languages/ directory.
         * If you're building a theme based on Twenty Eleven, use a find and replace
         * to change 'twentyeleven' to the name of your theme in all the template files.
         */
        load_theme_textdomain( 'twentyeleven', TEMPLATEPATH . '/languages' );

        $locale = get_locale();
        $locale_file = TEMPLATEPATH . "/languages/$locale.php";
        if ( is_readable( $locale_file ) )
            require_once( $locale_file );

        // This theme styles the visual editor with editor-style.css to match the theme style.
        add_editor_style();

        // Load up our theme options page and related code.
        require( dirname( __FILE__ ) . '/inc/theme-options.php' );

        // Grab Twenty Eleven's Ephemera widget.
        require( dirname( __FILE__ ) . '/inc/widgets.php' );

        // Add default posts and comments RSS feed links to <head>.
        add_theme_support( 'automatic-feed-links' );

        // This theme uses wp_nav_menu() in one location.
        register_nav_menu( 'primary', __( 'Primary Menu', 'twentyeleven' ) );

        // Add support for a variety of post formats
        add_theme_support( 'post-formats', array( 'aside', 'link', 'gallery', 'status', 'quote', 'image' ) );

        // Add support for custom backgrounds
        add_theme_support('custom-background');

        // This theme uses Featured Images (also known as post thumbnails) for per-post/per-page Custom Header images
        add_theme_support( 'post-thumbnails' );

        // The next four constants set how Twenty Eleven supports custom headers.

        // The default header text color
        define( 'HEADER_TEXTCOLOR', '000' );

        // By leaving empty, we allow for random image rotation.
        define( 'HEADER_IMAGE', '' );

        // The height and width of your custom header.
        // Add a filter to twentyeleven_header_image_width and twentyeleven_header_image_height to change these values.
        define( 'HEADER_IMAGE_WIDTH', apply_filters( 'twentyeleven_header_image_width', 1000 ) );
        define( 'HEADER_IMAGE_HEIGHT', apply_filters( 'twentyeleven_header_image_height', 288 ) );

        // We'll be using post thumbnails for custom header images on posts and pages.
        // We want them to be the size of the header image that we just defined
        // Larger images will be auto-cropped to fit, smaller ones will be ignored. See header.php.
        set_post_thumbnail_size( HEADER_IMAGE_WIDTH, HEADER_IMAGE_HEIGHT, true );

        // Add Twenty Eleven's custom image sizes
        add_image_size( 'large-feature', HEADER_IMAGE_WIDTH, HEADER_IMAGE_HEIGHT, true ); // Used for large feature (header) images
        add_image_size( 'small-feature', 500, 300 ); // Used for featured posts if a large-feature doesn't exist

        // Turn on random header image rotation by default.
        add_theme_support( 'custom-header', array( 'random-default' => true,
            'wp-head-callback'       => 'twentyeleven_header_style',
            'admin-head-callback'    => 'twentyeleven_admin_header_style',
            'admin-preview-callback' => 'twentyeleven_admin_header_image') );

        // Add a way for the custom header to be styled in the admin panel that controls
        // custom headers. See twentyeleven_admin_header_style(), below.
        // add_custom_image_header( 'twentyeleven_header_style', 'twentyeleven_admin_header_style', 'twentyeleven_admin_header_image' );

        // ... and thus ends the changeable header business.

        // Default custom headers packaged with the theme. %s is a placeholder for the theme template directory URI.
        register_default_headers( array(
            'wheel' => array(
                'url' => '%s/images/headers/wheel.jpg',
                'thumbnail_url' => '%s/images/headers/wheel-thumbnail.jpg',
                /* translators: header image description */
                'description' => __( 'Wheel', 'twentyeleven' )
            ),
            'shore' => array(
                'url' => '%s/images/headers/shore.jpg',
                'thumbnail_url' => '%s/images/headers/shore-thumbnail.jpg',
                /* translators: header image description */
                'description' => __( 'Shore', 'twentyeleven' )
            ),
            'trolley' => array(
                'url' => '%s/images/headers/trolley.jpg',
                'thumbnail_url' => '%s/images/headers/trolley-thumbnail.jpg',
                /* translators: header image description */
                'description' => __( 'Trolley', 'twentyeleven' )
            ),
            'pine-cone' => array(
                'url' => '%s/images/headers/pine-cone.jpg',
                'thumbnail_url' => '%s/images/headers/pine-cone-thumbnail.jpg',
                /* translators: header image description */
                'description' => __( 'Pine Cone', 'twentyeleven' )
            ),
            'chessboard' => array(
                'url' => '%s/images/headers/chessboard.jpg',
                'thumbnail_url' => '%s/images/headers/chessboard-thumbnail.jpg',
                /* translators: header image description */
                'description' => __( 'Chessboard', 'twentyeleven' )
            ),
            'lanterns' => array(
                'url' => '%s/images/headers/lanterns.jpg',
                'thumbnail_url' => '%s/images/headers/lanterns-thumbnail.jpg',
                /* translators: header image description */
                'description' => __( 'Lanterns', 'twentyeleven' )
            ),
            'willow' => array(
                'url' => '%s/images/headers/willow.jpg',
                'thumbnail_url' => '%s/images/headers/willow-thumbnail.jpg',
                /* translators: header image description */
                'description' => __( 'Willow', 'twentyeleven' )
            ),
            'hanoi' => array(
                'url' => '%s/images/headers/hanoi.jpg',
                'thumbnail_url' => '%s/images/headers/hanoi-thumbnail.jpg',
                /* translators: header image description */
                'description' => __( 'Hanoi Plant', 'twentyeleven' )
            )
        ) );
    }
endif; // twentyeleven_setup

if ( ! function_exists( 'twentyeleven_header_style' ) ) :
    /**
     * Styles the header image and text displayed on the blog
     *
     * @since Twenty Eleven 1.0
     */
    function twentyeleven_header_style() {

        // If no custom options for text are set, let's bail
        // get_header_textcolor() options: HEADER_TEXTCOLOR is default, hide text (returns 'blank') or any hex value
        if ( HEADER_TEXTCOLOR == get_header_textcolor() )
            return;
        // If we get this far, we have custom styles. Let's do this.
        ?>
        <style type="text/css">
            <?php
                // Has the text been hidden?
                if ( 'blank' == get_header_textcolor() ) :
            ?>
            #site-title,
            #site-description {
                position: absolute !important;
                clip: rect(1px 1px 1px 1px); /* IE6, IE7 */
                clip: rect(1px, 1px, 1px, 1px);
            }
            <?php
                // If the user has set a custom color for the text use that
                else :
            ?>
            #site-title a,
            #site-description {
                color: #<?php echo get_header_textcolor(); ?> !important;
            }
            <?php endif; ?>
        </style>
        <?php
    }
endif; // twentyeleven_header_style

if ( ! function_exists( 'twentyeleven_admin_header_style' ) ) :
    /**
     * Styles the header image displayed on the Appearance > Header admin panel.
     *
     * Referenced via add_custom_image_header() in twentyeleven_setup().
     *
     * @since Twenty Eleven 1.0
     */
    function twentyeleven_admin_header_style() {
        ?>
        <style type="text/css">
            .appearance_page_custom-header #headimg {
                border: none;
            }
            #headimg h1,
            #desc {
                font-family: "Helvetica Neue", Arial, Helvetica, "Nimbus Sans L", sans-serif;
            }
            #headimg h1 {
                margin: 0;
            }
            #headimg h1 a {
                font-size: 32px;
                line-height: 36px;
                text-decoration: none;
            }
            #desc {
                font-size: 14px;
                line-height: 23px;
                padding: 0 0 3em;
            }
            <?php
                // If the user has set a custom color for the text use that
                if ( get_header_textcolor() != HEADER_TEXTCOLOR ) :
            ?>
            #site-title a,
            #site-description {
                color: #<?php echo get_header_textcolor(); ?>;
            }
            <?php endif; ?>
            #headimg img {
                max-width: 1000px;
                height: auto;
                width: 100%;
            }
            #wpbody-content .quick-edit-row-post .inline-edit-col-right {
                width: 20%!important;
            }
            #wpbody-content .quick-edit-row-post .inline-edit-col-left {
                width: 30%!important;
            }
        </style>
        <?php
    }
endif; // twentyeleven_admin_header_style

if ( ! function_exists( 'twentyeleven_admin_header_image' ) ) :
    /**
     * Custom header image markup displayed on the Appearance > Header admin panel.
     *
     * Referenced via add_custom_image_header() in twentyeleven_setup().
     *
     * @since Twenty Eleven 1.0
     */
    function twentyeleven_admin_header_image() { ?>
        <div id="headimg">
            <?php
            if ( 'blank' == get_theme_mod( 'header_textcolor', HEADER_TEXTCOLOR ) || '' == get_theme_mod( 'header_textcolor', HEADER_TEXTCOLOR ) )
                $style = ' style="display:none;"';
            else
                $style = ' style="color:#' . get_theme_mod( 'header_textcolor', HEADER_TEXTCOLOR ) . ';"';
            ?>
            <h1><a id="name"<?php echo $style; ?> onclick="return false;" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
            <div id="desc"<?php echo $style; ?>><?php bloginfo( 'description' ); ?></div>
            <?php $header_image = get_header_image();
            if ( ! empty( $header_image ) ) : ?>
                <img src="<?php echo esc_url( $header_image ); ?>" alt="" />
            <?php endif; ?>
        </div>
    <?php }
endif; // twentyeleven_admin_header_image

/**
 * Sets the post excerpt length to 40 words.
 *
 * To override this length in a child theme, remove the filter and add your own
 * function tied to the excerpt_length filter hook.
 */
function twentyeleven_excerpt_length( $length ) {
    return 40;
}
add_filter( 'excerpt_length', 'twentyeleven_excerpt_length' );

/**
 * Returns a "Continue Reading" link for excerpts
 */
function twentyeleven_continue_reading_link() {
    return ' <a href="'. esc_url( get_permalink() ) . '">' . __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentyeleven' ) . '</a>';
}

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and twentyeleven_continue_reading_link().
 *
 * To override this in a child theme, remove the filter and add your own
 * function tied to the excerpt_more filter hook.
 */
function twentyeleven_auto_excerpt_more( $more ) {
    return ' &hellip;' . twentyeleven_continue_reading_link();
}
add_filter( 'excerpt_more', 'twentyeleven_auto_excerpt_more' );

/**
 * Adds a pretty "Continue Reading" link to custom post excerpts.
 *
 * To override this link in a child theme, remove the filter and add your own
 * function tied to the get_the_excerpt filter hook.
 */
function twentyeleven_custom_excerpt_more( $output ) {
    if ( has_excerpt() && ! is_attachment() ) {
        $output .= twentyeleven_continue_reading_link();
    }
    return $output;
}
add_filter( 'get_the_excerpt', 'twentyeleven_custom_excerpt_more' );

/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 */
function twentyeleven_page_menu_args( $args ) {
    $args['show_home'] = true;
    return $args;
}
add_filter( 'wp_page_menu_args', 'twentyeleven_page_menu_args' );

/**
 * Register our sidebars and widgetized areas. Also register the default Epherma widget.
 *
 * @since Twenty Eleven 1.0
 */
function twentyeleven_widgets_init() {

    register_widget( 'Twenty_Eleven_Ephemera_Widget' );

    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'twentyeleven' ),
        'id' => 'sidebar-1',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => "</aside>",
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

    register_sidebar( array(
        'name' => __( 'Showcase Sidebar', 'twentyeleven' ),
        'id' => 'sidebar-2',
        'description' => __( 'The sidebar for the optional Showcase Template', 'twentyeleven' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => "</aside>",
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

    register_sidebar( array(
        'name' => __( 'Footer Area One', 'twentyeleven' ),
        'id' => 'sidebar-3',
        'description' => __( 'An optional widget area for your site footer', 'twentyeleven' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => "</aside>",
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

    register_sidebar( array(
        'name' => __( 'Home page sidebar', 'twentyeleven' ),
        'id' => 'sidebar-6',
        'description' => __( 'An optional widget area for your homepage sidebar', 'twentyeleven' ),
        'before_widget' => '<aside id="homepage_side_left" class="widget %2$s">',
        'after_widget' => "</aside>",
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'

    ) );

    register_sidebar( array(
        'name' => __( 'Home page sidebar-2', 'twentyeleven' ),
        'id' => 'sidebar-7',
        'description' => __( 'Optional sidebar right column', 'twentyeleven' ),
        'before_widget' => '<aside id="homepage_side_right" class="widget %2$s">',
        'after_widget' => "</aside>",
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'

    ) );

    register_sidebar( array(
        'name' => __( 'Home page featured sidebar', 'twentyeleven' ),
        'id' => 'featured-sidebar',
        'description' => __( 'Featured Sidebar', 'twentyeleven' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => "</aside>",
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'

    ) );

    register_sidebar( array(
        'name' => __( 'Featured-tools', 'twentyeleven' ),
        'id' => 'featured-tools',
        'description' => __( 'Featured tools sideabar', 'twentyeleven' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => "</aside>",
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'

    ) );
}
add_action( 'widgets_init', 'twentyeleven_widgets_init' );

/**
 *	Add custom field to each post
 */

function reg_tag() {
    register_taxonomy_for_object_type('post_tag', 'plugin');
}
add_action('init', 'reg_tag');

/**
 * Display navigation to next/previous pages when applicable
 */
function twentyeleven_content_nav( $nav_id ) {
    global $wp_query;

    if ( $wp_query->max_num_pages > 1 ) : ?>
        <nav id="<?php echo $nav_id; ?>">



            <?php if(function_exists('wp_pagenavi')) {	 wp_pagenavi(); } ?>

        </nav><!-- #nav-above -->
    <?php endif;
}

/**
 * Return the URL for the first link found in the post content.
 *
 * @since Twenty Eleven 1.0
 * @return string|bool URL or false when no link is present.
 */
function twentyeleven_url_grabber() {
    if ( ! preg_match( '/<a\s[^>]*?href=[\'"](.+?)[\'"]/is', get_the_content(), $matches ) )
        return false;

    return esc_url_raw( $matches[1] );
}

/**
 * Count the number of footer sidebars to enable dynamic classes for the footer
 */
function twentyeleven_footer_sidebar_class() {
    $count = 0;

    if ( is_active_sidebar( 'sidebar-3' ) )
        $count++;

    if ( is_active_sidebar( 'sidebar-4' ) )
        $count++;

    if ( is_active_sidebar( 'sidebar-5' ) )
        $count++;

    $class = '';

    switch ( $count ) {
        case '1':
            $class = 'one';
            break;
        case '2':
            $class = 'two';
            break;
        case '3':
            $class = 'three';
            break;
    }

    if ( $class )
        echo 'class="' . $class . '"';
}

if ( ! function_exists( 'twentyeleven_comment' ) ) :
    /**
     * Template for comments and pingbacks.
     *
     * To override this walker in a child theme without modifying the comments template
     * simply create your own twentyeleven_comment(), and that function will be used instead.
     *
     * Used as a callback by wp_list_comments() for displaying the comments.
     *
     * @since Twenty Eleven 1.0
     */

    function xnat_admin_styles() {
        ?>
        <style type="text/css">
            #wpbody-content .quick-edit-row-post .inline-edit-col-right {
                width: 20%!important;
            }
            #wpbody-content .quick-edit-row-post .inline-edit-col-left {
                width: 30%;
            }
            fieldset.featured-sliderpost-col-right label,fieldset.featured-sliderpost-col-right p{
                display:inline-block;
                margin-right:8px;
            }
            fieldset.featured-sliderpost-col-right{
                width:270px!important;
                padding:20px!important;
                background-color: #F1F1F1;
                background-image: -ms-linear-gradient(top,#F9F9F9,#ECECEC);
                background-image: -moz-linear-gradient(top,#F9F9F9,#ECECEC);
                background-image: -o-linear-gradient(top,#F9F9F9,#ECECEC);
                background-image: -webkit-gradient(linear,left top,left bottom,from(#F9F9F9),to(#ECECEC));
                background-image: -webkit-linear-gradient(top,#F9F9F9,#ECECEC);
                background-image: linear-gradient(top,#F9F9F9,#ECECEC);

                margin: 20px 84px 20px 110px!important;
                border: 1px solid #CCC!important;

            }

        </style>
    <?php }
    add_action('admin_head','xnat_admin_styles');

    function twentyeleven_comment( $comment, $args, $depth ) {
        $GLOBALS['comment'] = $comment;
        switch ( $comment->comment_type ) :
            case 'pingback' :
            case 'trackback' :
                ?>
                <li class="post pingback">
                <p><?php _e( 'Pingback:', 'twentyeleven' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?></p>
                <?php
                break;
            default :
                ?>
                <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
                    <article id="comment-<?php comment_ID(); ?>" class="comment">
                        <?php
                        $avatar_size = 68;
                        if ( '0' != $comment->comment_parent )
                            $avatar_size = 39;

                        //echo get_avatar( $comment, $avatar_size );?>
                        <?php //echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'twentyeleven_author_bio_avatar_size', 94 ) ); ?>

                        <footer class="comment-meta">
                            <?php	/* translators: 1: comment author, 2: date and time */
                            printf( __( '%1$s on %2$s <span class="says">said:</span>', 'twentyeleven' ),
                                sprintf( '<span class="fn">%s</span>', get_comment_author_link() ),
                                sprintf( '<a href="%1$s"><time pubdate datetime="%2$s">%3$s</time></a>',
                                    esc_url( get_comment_link( $comment->comment_ID ) ),
                                    get_comment_time( 'c' ),
                                    /* translators: 1: date, 2: time */
                                    sprintf( __( '%1$s at %2$s', 'twentyeleven' ), get_comment_date(), get_comment_time() )
                                )
                            );
                            ?>
                            <div class="comment-author vcard">
                                <?php edit_comment_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?>
                            </div><!-- .comment-author .vcard -->

                            <?php if ( $comment->comment_approved == '0' ) : ?>
                                <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'twentyeleven' ); ?></em>
                                <br />
                            <?php endif; ?>

                        </footer>

                        <div class="comment-content"><?php comment_text(); ?></div>

                        <div class="reply">
                            <?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply <span>&darr;</span>', 'twentyeleven' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
                        </div><!-- .reply -->
                    </article><!-- #comment-## -->
                </li>
                <div class="comment-divider"></div>
                <?php
                break;
        endswitch;
    }
endif; // ends check for twentyeleven_comment()

if ( ! function_exists( 'twentyeleven_posted_on' ) ) :
    /**
     * Prints HTML with meta information for the current post-date/time and author.
     * Create your own twentyeleven_posted_on to override in a child theme
     *
     * @since Twenty Eleven 1.0
     */
    function twentyeleven_posted_on() {
        printf( __( '<span class="sep">Posted on </span><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s" pubdate>%4$s</time></a><span class="by-author"> <span class="sep"> by </span> <span class="author vcard"><a class="url fn n" href="%5$s" title="%6$s" rel="author">%7$s</a></span></span>', 'twentyeleven' ),
            esc_url( get_permalink() ),
            esc_attr( get_the_time() ),
            esc_attr( get_the_date( 'c' ) ),
            esc_html( get_the_date() ),
            esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
            sprintf( esc_attr__( 'View all posts by %s', 'twentyeleven' ), get_the_author() ),
            esc_html( get_the_author() )
        );
    }
endif;

/**
 * Adds two classes to the array of body classes.
 * The first is if the site has only had one author with published posts.
 * The second is if a singular post being displayed
 *
 * @since Twenty Eleven 1.0
 */
function twentyeleven_body_classes( $classes ) {

    if ( ! is_multi_author() ) {
        $classes[] = 'single-author';
    }

    if ( is_singular() && ! is_home() && ! is_page_template( 'showcase.php' ) && ! is_page_template( 'sidebar-page.php' ) )
        $classes[] = 'singular';

    return $classes;
}
add_filter( 'body_class', 'twentyeleven_body_classes' );


// Remove WP ADMIN BAR
add_filter( 'show_admin_bar', '__return_false' );


/*********************************************************************
/  This code handles the uploads from the submit Add-on form
/  http://integrity-plugins.xnat.org/add-plugin/
/*********************************************************************/
function insert_attachment($file_handler,$post_id,$name,$setthumb='false') {

    // check to make sure its a successful upload
    if ($_FILES[$file_handler]['error'] !== UPLOAD_ERR_OK) __return_false();

    require_once(ABSPATH . "wp-admin" . '/includes/image.php');
    require_once(ABSPATH . "wp-admin" . '/includes/file.php');
    require_once(ABSPATH . "wp-admin" . '/includes/media.php');

    $attach_id = media_handle_upload( $file_handler, $post_id );
    /*wp_update_post( array(
                'ID' => $attach_id,
                'post_title' => ,
                'post_excerpt' => "New image caption",
                'post_content' => "New image description" )
              );*/
    $attach_key = 'document_file_id';

    $url = wp_get_attachment_url($attach_id);
    foreach($_FILES as $key => $file) {
        $attach_key = $key . '_id';

    }

    add_post_meta($post_id,'upload_'.$name,$url);



    return $attach_id;
}


/*********************************************************************
/ Initialize plugin post type
/  1. create the lable that handles the admin menus
/  2. register the custom post type
/*********************************************************************/
add_action( 'init', 'post_downloadable_item' );


function post_downloadable_item() {
    $labels = array(
        'name' => _x('Add-Ons', 'post type general name'),
        'singular_name' => _x('Add-Ons', 'post type singular name'),
        'add_new' => _x('Add New Add-On', 'New Add-On'),
        'add_new_item' => __('Add New Add-On'),
        'edit_item' => __('Edit Add-On'),
        'new_item' => __('New Add-On'),
        'view_item' => __('View Add-On'),
        'search_items' => __('Search Add-Ons'),
        'not_found' =>	__('No Add-On'),
        'not_found_in_trash' => __('No Add-Ons found in Trash'),
        'parent_item_colon' => ''
    );

    $supports = array('title', 'author', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'page-attributes','tags','comments');

    /*******************************************************************
    / wordpress function to register post type						   *
    / http://codex.wordpress.org/Function_Reference/register_post_type *
    /******************************************************************/

    register_post_type( 'plugin',
        array(
            'labels' => $labels,
            'public' => true,
            'has_archive' => true,
            'publicly_queryable' => true,
            'query_var' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            "supports"	=> array("title", "editor", "author", "custom-fields", "comments",'page-attributes','tags'),
            'taxonomies' => array('post_tag'),
            'rewrite'  => array("slug" => "plugin")

        )
    );
}

/*********************************************************************
/ Build Taxonomies that will be used with the plugin custom post type
/  Taxonomies are plugins,tools and packages
/*********************************************************************/
add_action( 'init', 'build_taxonomies', 0 );

function build_taxonomies() {

    $labels = array(
        'name' => _x( 'Plugins', 'taxonomy general name' ),
        'singular_name' => _x( 'Plugin Addon', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Plugin Addons' ),
        'all_items' => __( 'All Plugin Addons' ),
        'parent_item' => __( 'Plugins' ),
        'parent_item_colon' => __( 'Plugins:' ),
        'edit_item' => __( 'Edit plugin Addons' ),
        'update_item' => __( 'Update Plugin Addons' ),
        'add_new_item' => __( 'Add New Plugin Addon' ),
        'new_item_name' => __( 'New Plugin Addon' ),
        'menu_name' => __( 'Plugins' ),
    );

    $labels_two = array(
        'name' => _x( 'XNAT Tools', 'taxonomy general name' ),
        'singular_name' => _x( 'Tools Addon', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Tools Addons' ),
        'all_items' => __( 'All Tools Addons' ),
        'parent_item' => __( 'Plugins' ),
        'parent_item_colon' => __( 'Plugins:' ),
        'edit_item' => __( 'Edit Tools Addons' ),
        'update_item' => __( 'Update Tools Addons' ),
        'add_new_item' => __( 'Add New Tools Addon' ),
        'new_item_name' => __( 'New Tools Addon' ),
        'menu_name' => __( 'XNAT Tools' ),
    );

    $labels_three = array(
        'name' => _x( 'Packages', 'taxonomy general name' ),
        'singular_name' => _x( 'Packages', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Packages' ),
        'all_items' => __( 'All Packages' ),
        'parent_item' => __( 'Plugin' ),
        'parent_item_colon' => __( 'Plugin:' ),
        'edit_item' => __( 'Edit Package' ),
        'update_item' => __( 'Update Package' ),
        'add_new_item' => __( 'Add New Package' ),
        'new_item_name' => __( 'New Package' ),
        'menu_name' => __( 'Packages' ),
    );
    register_taxonomy(
        'plugins',
        'plugin',
        array(
            'hierarchical' => true,
            'label' => 'Plugins',
            'show_ui' => true,
            'has_archive' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'plugins'),
            'labels' => array('name' => "Plugins", 'singular_name' => 'Plugin', 'menu_name' => 'Plugins')
        )
    );

    register_taxonomy(
        'tools',
        'plugin',
        array(
            'hierarchical' => true,
            'label' => 'XNAT Tools',
            'show_ui' => true,
            'has_archive' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'tools')
        )
    );

    register_taxonomy(
        'packages',
        'plugin',
        array(
            'hierarchical' => true,
            'label' => 'Package',
            'show_ui' => true,
            'has_archive' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'package')
        )
    );
}

/*********************************************************************
/  Allow custom post types to be a part of wordpress category and
/  tag searches query var
/*********************************************************************/
add_filter('pre_get_posts', 'query_post_type');
function query_post_type($query) {
    if(is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] )) {
        $post_type = get_query_var('post_type');
        if($post_type)
            $post_type = $post_type;
        else
            $post_type = array('post','plugin'); // replace cpt to your custom post type
        $query->set('post_type',$post_type);
        return $query;
    }
}

function any_ptype_on_tag($request) {
    if ( isset($request['tag']) )
        $request['post_type'] = 'any';

    return $request;
}
add_filter('request', 'any_ptype_on_tag');

/*********************************************************************
/  Add the plugin custom post type to the default search function
/
/*********************************************************************/
function filter_search($query) {
    if ($query->is_search) {
        $query->set('post_type', array('post', 'plugin'));
    };
    return $query;
};
add_filter('pre_get_posts', 'filter_search');

/*****************************************************************
/	WP CUSTOM SEARCH FILTER FUNCTION
/	This is the search filter select dropdown
/*****************************************************************/
function make_terms_dropdown($tax, $taxs, $taxonomies) {
    $args = array( 'orderby' => 'name', 'hide_empty' => false);
    $myterms = get_terms($taxonomies, $args);
    $output = "<select name='$tax' id='$tax' class='postform'>";
    $output .= "<option value='0'>Filter by: $taxs</option>";
    foreach($myterms as $term){
        $term_taxonomy = $term->taxonomy;
        $term_slug = $term->slug;
        $term_name = $term->name;
        $output .= "<option value='".$term_slug."'>".$term_name."</option>";
    }
    $output .="</select>";
    return $output;
}



/******************************************************************
/ Define constants for SMOF
/ Admin panel
/******************************************************************/
/*------------------------------------------*/
/* Options Framework
/*------------------------------------------*/
// Paths to admin functions
$themedata = wp_get_theme();
// $themedata = wp_get_theme(STYLESHEETPATH . '/style.css');
define('ADMIN_PATH', STYLESHEETPATH . '/includes/admin/');
define('ADMIN_DIR', get_bloginfo('stylesheet_directory') . '/includes/admin/');
define('LAYOUT_PATH', ADMIN_PATH . '/layouts/');
define('THEMENAME', $themedata['Name']);
define('OPTIONS', 'of_options'); // Name of the database row where your options are stored

// Build Options
require_once (ADMIN_PATH . 'admin-interface.php'); // Admin Interfaces
require_once (ADMIN_PATH . 'theme-options.php'); // Options panel settings and custom settings
require_once (ADMIN_PATH . 'admin-functions.php'); // Theme actions based on options settings
require_once (ADMIN_PATH . 'medialibrary-uploader.php'); // Media Library Uploader




add_action( 'init', 'registerScripts' );
function registerScripts() {
    if ( !is_admin() ) {
        wp_enqueue_style( 'colorbox', get_template_directory_uri() . '/css/example1/colorbox.css');
        if (!is_page('75')) {
            /*wp_enqueue_style( 'validation', get_template_directory_uri() . '/css/validationEngine.jquery.css');
            wp_enqueue_script( 'validation-en', get_template_directory_uri() . '/js/languages/jquery.validationEngine-en.js', array( 'jquery' ));
            wp_enqueue_script( 'validation', get_template_directory_uri() . '/js/jquery.validationEngine.js', array( 'jquery' ));*/
            wp_enqueue_script( 'metadata', get_template_directory_uri() . '/js/jquery.metadata.js', array( 'jquery' ));
            wp_enqueue_script( 'validation', get_template_directory_uri() . '/js/jquery.validate.min.js', array( 'jquery' ));
        }
        wp_enqueue_script( 'colorbox', get_template_directory_uri() . '/js/jquery.colorbox-min.js', array( 'jquery' ));
        wp_enqueue_script( 'easing', get_template_directory_uri() . '/js/jquery.easing.1.3.js', array( 'jquery'));
        wp_enqueue_script( 'jquerytools', get_template_directory_uri() . '/js/jquery.tools.min.js', array('jquery'));
        // wp_enqueue_script( 'uniform', get_template_directory_uri() . '/js/jquery.uniform.min.js', array('jquery'));
        wp_enqueue_script( 'cookie', get_template_directory_uri() . '/js/jquery.cookie.js', array('jquery'));
        wp_enqueue_script( 'global_scripts', get_template_directory_uri() . '/js/scripts.js?cache=busted', array('jquery'));

    }
}

//enable scripts/styles on the edit posts page
add_action( "admin_head-post.php", 'plugin_admin_head_script' );
function plugin_admin_head_script() {
    wp_enqueue_style('jquerycustomstyle', get_template_directory_uri() . '/css/plugin-meta-tabs.css');
    wp_enqueue_script( 'post_custom_tabs', get_template_directory_uri() . '/js/plugin-meta-tabs.js', array('jquery'));
}

//enable the registration modal
add_filter('simplemodal_registration_form', 'xnat_registration_form');

//customize the texyt shown at registration
add_filter( 'gettext', 'ts_edit_password_email_text' );
function ts_edit_password_email_text ( $text ) {
    if ( $text == 'A password will be e-mailed to you.' ) {
        $text = 'If you leave password fields empty one will be generated for you. Password must be at least eight characters long.';
    }
    return $text;
}

/*********************************************************************
/  This code handles grabbing the field values on the registration page
/  and populating the user profile with their values
/*********************************************************************/
//add_action( 'user_register', 'xnat_register_extra_fields');
//function xnat_register_extra_fields( $user_id, $password = '', $meta = array() )  {
//    require_once(ABSPATH . 'wp-admin/includes/admin.php');
//    require_once(ABSPATH . "wp-admin" . '/includes/image.php');
//    include_once(ABSPATH . "wp-admin" . '/includes/file.php');
//    require_once(ABSPATH . "wp-admin" . '/includes/media.php');
//
//    //fix jQuery cycle call
//    wp_enqueue_script( 'cycle', get_template_directory_uri() . '/js/jquery.cycle.all.js', array( 'jquery'));
//
//    $userdata = array();
//    $userdata['ID'] = $user_id;
//    $userdata['description'] = $_POST['user_description'];
//    $userdata['display_name'] = $_POST['user_nickname'];
//    $userdata['user_nicename'] = $_POST['user_nickname'];
//    if ( $_POST['password'] !== '' ) {
//        $userdata['user_pass'] = $_POST['user_password'];
//    }
//    if ( $_POST['user_login'] !== '' ) {
//        $userdata['user_email'] = $_POST['user_login'];
//    }
//
//    //update user data
//    $new_user_id = wp_update_user( $userdata );
//    update_user_meta($user_id, 'institution',$_POST['institution']);
//
//    if (isset( $_FILES['avatar'] ) ) {
//        update_user_meta_data($user_id, 'testurl', true); }
//    else {
//        add_user_meta($user_id, 'newitem',$_FILES['avatar']);
//    }
//
//}

// Add recaptcha to the registration page
add_action( 'register_form', 'add_recaptcha_register_form' );
function add_recaptcha_register_form() {
    ?>
    <script>
        var onloadCallback = function() {
            grecaptcha.render('explicit-recaptcha', {
                'sitekey' : '6LfumJEUAAAAADU7izQRsKZRJFuZ1ucqitkjYQJr'
            });
        };
    </script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
    <p>
        <label>Help Us Prevent Bot Registrations</label>
        <div id="explicit-recaptcha"></div>
<!--        <div class="g-recaptcha" data-sitekey="6LfumJEUAAAAADU7izQRsKZRJFuZ1ucqitkjYQJr"></div>-->
    </p>
    <style>
        #login { width: 350px }
    </style>
    <?php
}

// Validate recaptcha.
add_filter( 'registration_errors', 'add_recaptcha_registration_errors', 10, 3 );
function add_recaptcha_registration_errors( $errors, $sanitized_user_login, $user_email ) {

    if (empty($_POST['g-recaptcha-response']) || $_POST['g-recaptcha-response'] == '') {
        $errors->add( 'recaptcha_error', sprintf('<strong>%s</strong>: %s',__( 'ERROR', 'grecaptcha' ),__( 'You must not be a bot.', 'grecaptcha' ) ) );
    }

    return $errors;
}

// Save extra registration user meta.
add_action( 'user_register', 'add_recaptcha_user_register' );
function add_recaptcha_user_register( $user_id ) {
    // no additional data expected
}

/*********************************************************************
/  GRAB THE AVATAR
/
/*********************************************************************/
function update_user_meta_data($id, $data_key, $is_file = false) {
    if ($is_file) {
        if (!empty($_FILES[$data_key]['name'])) {
            $upload = wp_handle_upload($_FILES[$data_key], array('test_form' => false));

            if (isset($upload['error'])) {
                wp_die('There was an error uploading your file. Please try again.');
            } else {
                add_user_meta($id, $data_key, $upload['url']);
                update_user_meta($id, $data_key, $upload['url']);
            } // end if/else

        }
    } else {
        $data = $_POST[$data_key];
        add_user_meta($id, $data_key, $data);
        update_user_meta($id, $data_key, $data);
    }
}



/*********************************************************************
/  OVERRIDE THE DEFAULT REGISTRATION WITH THIS ONE BELOW
/
/*********************************************************************/
function xnat_registration_form($form) {
    $options = get_option('simplemodal_login_options');
    $output = '
	<form  name="registerform" id="registerform" enctype="multipart/form-data" action="/wp-login.php?action=register" method="post"  autocomplete="off">

	<div class="title">Create a new account</div>
	<p class="reg-description"> Accounts are only required to contribute new plugins and tools. Account information will not be shared with third parties, as per our <b><a href="https://xnat.org/privacy/" target="_blank">privacy statement</a></b>.
	</p>
	<div class="simplemodal-login-fields">
	<p>
	  <strong>All fields marked with an "*" are required</strong>
	</p>
	<ul id="register_left">
	  <li>
		<p class="form_input display-name">
			<label>Display Name *
			<span>(name of individual or group, e.g. "Dan\'s group." "Jane Doe")</span>
			<input type="text" name="user_nickname" class="user_login input" value="" size="20" tabindex="1" /></label>
		</p>
	  </li>

	  <li>
		<p class="form_input">
			<label>Institution *<br />
			<input type="text" name="institution" class="institution input" value="" size="50" tabindex="2" /></label>
		</p>
	  </li>


	  <li>
		<p class="form_input">
			<label>Description
			<span>(Describe the types of projects you work on, about yourself, etc.)</span>
			<textarea type="text" name="user_description" class="user_description input" value="" size="20" tabindex="3"> </textarea></label>
		</p>
	   </li>
	  </ul>

	 <ul id="register_right">

		<li>
			<p class="form_input">
				<label>Email *
				<span>(this will be your username)</span>
				<input type="text" name="user_login" class="user_login input" value="" size="25" tabindex="4" /></label>

			</p>
		</li>
		<li>
		<p class="form_input">
			<label>Password *<br />
			<input type="password" name="user_password" class="user_login input" value="" size="20" tabindex="5" /></label>
		</p>
	  </li>

   <li>
		<p class="form_input">
			<label>RE-enter Password *<br />
			<input type="password" name="user_pass_again" class="user_login_two input" value="" size="20" tabindex="6" /></label>
		</p>
	  </li>


	<li style="display:none;">
		<p class="form_input">
			<p id="avatartext">
			 <strong>Upload Avatar</strong><br />
			 If you do not upload one, a default image will be used.
			</p>
			<label>
			<input type="file" name="avatar" class="user_avatar input"	 />
			<span class="avatarspan">jpg, png, gif, max of 2 MB</span></label>
		</p>
	  </li>
		<li style="display:none;">

	<p>
		<label>%s<br />
		<input type="hidden" name="user_email" class="user_email input" value="" size="25" tabindex="3" />
	</p>
		</li>

		';

    ob_start();
    $output .= ob_get_clean();

    $output .= '


		<p class="reg_passmail">%s</p>
		<p class="submit">
			<input type="submit" name="wp-submit" value="Create Account" tabindex="100" />
			<input type="button" class="simplemodal-close" value="%s" tabindex="101" />
		</p>
	</li>
	<li>
		<p class="nav">
			<a class="simplemodal-login" href="%s">%s</a>';/*,
			//__('test', 'simplemodal-login'),
			__('Register', 'simplemodal-login'),
			__('Cancel', 'simplemodal-login'),
			site_url('wp-login.php', 'login'),
			__('Log in', 'simplemodal-login')
		);*/

    if ($options['reset']) {
        $output .= sprintf(' | <a class="simplemodal-forgotpw" href="%s" title="%s">%s</a>',
            site_url('wp-login.php?action=lostpassword', 'login'),
            __('Password Lost and Found', 'simplemodal-login'),
            __('Lost your password?', 'simplemodal-login')
        );
    }

    $output .= '
	</li>
		</p>
		</div>
		<div class="simplemodal-login-activity" style="display:none;"></div>
	</form>';

    return $output;
}


/*********************************************************************
/  deregister contact form 7 for all pages except where neccessary
/  Only show on contact page
/*********************************************************************/
add_action('wp_print_scripts', 'my_deregister_javascript', 100);
function my_deregister_javascript() {
    if (!is_page('Contact Us')) {
        wp_deregister_script( 'contact-form-7' );
    }
}


//**************************************
// SimpleModal Login Override
//**************************************/
add_filter('simplemodal_login_form', 'xnat_login_form');

function xnat_login_form($form) {
    $users_can_register = get_option('users_can_register') ? true : false;
    $options = get_option('simplemodal_login_options');

    $output = sprintf('
<form name="loginform" id="loginform" action="%s" method="post">
	<div class="title">Log In to Your Account</div>
	<div class="simplemodal-login-fields">
	<p>
		<label>Email<br />
		<input type="text" name="log" class="user_login input" value="" size="50" tabindex="10" placeholder="Enter your email address" /></label>
	</p>
	<p>
		<label>Password<br />
		<input type="password" name="pwd" class="user_pass input" value="" placeholder="Enter your password" size="20" tabindex="50" /></label>
	</p>',
        site_url('wp-login.php', 'login_post'),
        __('Login', 'simplemodal-login'),
        __('Username', 'simplemodal-login'),
        __('Password', 'simplemodal-login')
    );

    ob_start();
    do_action('login_form');
    $output .= ob_get_clean();

    $output .= sprintf('
	<p class="forgetmenot"><label><input name="rememberme" type="checkbox" id="rememberme" class="rememberme" value="forever" tabindex="90" />%s</label></p>
	<p class="submit">
		<input type="submit" name="wp-submit" value="%s" tabindex="100" />
		<input type="button" class="simplemodal-close" value="%s" tabindex="101" />
		<input type="hidden" name="testcookie" value="1" />
	</p>
	<p class="nav">',
        __('Remember Me', 'simplemodal-login'),
        __('Log In', 'simplemodal-login'),
        __('Cancel', 'simplemodal-login')
    );

    if ($users_can_register && $options['registration']) {
        $output .= sprintf('<a class="simplemodal-register" href="%s">%s</a>',
            site_url('wp-login.php?action=register', 'login'),
            __('Register', 'simplemodal-login')
        );
    }

    if (($users_can_register && $options['registration']) && $options['reset']) {
        $output .= ' | ';
    }

    if ($options['reset']) {
        $output .= sprintf('<a class="simplemodal-forgotpw" href="%s" title="%s">%s</a>',
            site_url('wp-login.php?action=lostpassword', 'login'),
            __('Password Lost and Found', 'simplemodal-login'),
            __('Lost your password?', 'simplemodal-login')
        );
    }

    $output .= '
	</p>
	</div>
	<div class="simplemodal-login-activity" style="display:none;"></div>
</form>';

    return $output;
}

//**************************************************
// SIMPLE MODAL RESET PASSWORD HOOK
//**************************************************/
add_filter('simplemodal_reset_form', 'xnat_reset_form');
function xnat_reset_form($form) {
    $users_can_register = get_option('users_can_register') ? true : false;
    $options = get_option('simplemodal_login_options');

    $output = sprintf('
	<form name="lostpasswordform" id="lostpasswordform" action="%s" method="post">
		<div class="title">%s</div>
		<div class="simplemodal-login-fields">
		<p>
			<label>%s<br />
			<input type="text" name="user_login" class="user_login input" value="" size="20" tabindex="10" /></label>
		</p>',
        site_url('wp-login.php?action=lostpassword', 'login_post'),
        __('Reset Password', 'simplemodal-login'),
        __('E-mail:', 'simplemodal-login')
    );

    ob_start();
    do_action('lostpassword_form');
    $output .= ob_get_clean();

    $output .= sprintf('
		<p class="submit">
			<input type="submit" name="wp-submit" value="%s" tabindex="100" />
			<input type="button" class="simplemodal-close" value="%s" tabindex="101" />
		</p>
		<p class="nav">
			<a class="simplemodal-login" href="%s">%s</a>',
        __('Get New Password', 'simplemodal-login'),
        __('Cancel', 'simplemodal-login'),
        site_url('wp-login.php', 'login'),
        __('Log in', 'simplemodal-login')
    );

    if ($users_can_register && $options['registration']) {
        $output .= sprintf('| <a class="simplemodal-register" href="%s">%s</a>', site_url('wp-login.php?action=register', 'login'), __('Register', 'simplemodal-login'));
    }

    $output .= '
		</p>
		</div>
		<div class="simplemodal-login-activity" style="display:none;"></div>
	</form>';

    return $output;
}


//**************************************
// Primary BreadCrumbs
//**************************************/
function dimox_breadcrumbs() {
    $delimiter = '<div class="delimeter"></div>'; // delimiter between crumbs
    $home = 'Home'; // text for the 'Home' link
    $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
    $before = '<span class="current">'; // tag before the current crumb
    $after = '</span>'; // tag after the current crumb

    global $post;
    $homeLink = get_bloginfo('url');

    if (!is_home() && !is_front_page()) {

        echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter;

        if ( is_category() ) {
            global $wp_query;
            $cat_obj = $wp_query->get_queried_object();
            $thisCat = $cat_obj->term_id;
            $thisCat = get_category($thisCat);
            $parentCat = get_category($thisCat->parent);
            if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
            echo $before . 'Archive by category "' . single_cat_title('', false) . '"' . $after;

        } elseif ( is_day() ) {
            echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
            echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
            echo $before . get_the_time('d') . $after;

        } elseif ( is_month() ) {
            echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
            echo $before . get_the_time('F') . $after;

        } elseif ( is_year() ) {
            echo $before . get_the_time('Y') . $after;

        }
        elseif ( is_single() && !is_attachment() ) {
            if ( get_post_type() == 'plugin' ) { // Post type name
                $post_type = get_post_type_object(get_post_type());
                $slug = $post_type->rewrite;

                $is_tool = get_query_var('is_tool');
                if($is_tool == 'y'){
                    if ($showCurrent == 1) echo '<a href="'.$homeLink.'/tools/">Tools</a> ' . $delimiter . $before . get_the_title() . $after;
                } else {
                    if ($showCurrent == 1) echo '<a href="'.$homeLink.'/plugins/">Plugins</a> ' . $delimiter . $before . get_the_title() . $after;
                }
            } else {
                $cat = get_the_category(); $cat = $cat[0];
                echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
                if ($showCurrent == 1) echo $before . get_the_title() . $after;
            }


        }
        elseif ( is_single() && !is_attachment() ) {
            if ( get_post_type() != 'post' ) {
                $post_type = get_post_type_object(get_post_type());
                $slug = $post_type->rewrite;
                echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a> ' . $delimiter . ' ';
                if ($showCurrent == 1) echo $before . get_the_title() . $after;
            } else {
                $cat = get_the_category(); $cat = $cat[0];
                echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
                if ($showCurrent == 1) echo $before . get_the_title() . $after;
            }

        } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() && !is_search()	 ) {
            $post_type = get_post_type_object(get_post_type());
            global $wp_query;
            $cat_obj = $wp_query->get_queried_object();
            $thisCat = $cat_obj->term_id;
            $thisCat = get_category($thisCat);
            $parentCat = get_category($thisCat->parent);
            $parent;
            if (is_tax('plugins')) {
                $parent = '<a href="'.$homeLink.'/plugins/">Plugins</a> '.$delimiter;
            }
            if (is_tax('tools')) {
                $parent = '<a href="'.$homeLink.'/tools/">Tools</a> '.$delimiter;
            }
            if (is_tax ('packages')) {
                $parent = 'Packages '.$delimiter;
            }
            $package = 'package';
            echo $parent . $before . single_cat_title('', false) . $after;

        } elseif ( is_attachment() ) {
            $parent = get_post($post->post_parent);
            $cat = get_the_category($parent->ID); $cat = $cat[0];
            echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
            echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> ' . $delimiter . ' ';
            if ($showCurrent == 1) echo $before . get_the_title() . $after;

        } elseif ( is_page() && !$post->post_parent ) {
            if ($showCurrent == 1) echo $before . get_the_title() . $after;

        } elseif ( is_page() && $post->post_parent ) {
            $parent_id  = $post->post_parent;
            $breadcrumbs = array();
            while ($parent_id) {
                $page = get_page($parent_id);
                $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
                $parent_id	= $page->post_parent;
            }
            $breadcrumbs = array_reverse($breadcrumbs);
            foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
            if ($showCurrent == 1) echo $before . get_the_title() . $after;

        } elseif ( is_search() ) {
            echo $before . 'Search results for "' . get_search_query() . '"' . $after;

        } elseif ( is_tag() ) {
            echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;

        }
        elseif ( is_404() ) {
            echo $before . 'Error 404' . $after;
        }

        if ( get_query_var('paged') ) {
            echo '<div class="delimeter"></div>';
            if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';

            echo __('Page') . ' ' . get_query_var('paged');
            if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
        }
        if ( is_author() ) {
            global $author;
            $userdata = get_userdata($author);
            echo $before . 'Add-Ons Uploaded by ' . $userdata->display_name . $after;
        }

        echo '</div>';
    } else {
        echo '<div id="crumbs"></div>';
    }
}


//***************************************************************
// Add filter to gather attachments when the_content() is called.
// Not actively used - purely for testing purposes
//***************************************************************/
function my_the_content_filter( $content ) {
    global $post;
    //$args =  array( 'post_type' => 'attachment', 'numberposts' => , 'post_status' => null, 'post_parent' => $post->ID, 'post_mime_type' => 'application/zip' );

    if ( is_single() && $post->post_type == 'plugin' && $post->post_status == 'publish' ) {
        $attachments = $attachments = get_posts( array(
            'post_type' => 'attachment',
            'posts_per_page' => 0,
            'post_mime_type' => 'application/zip',
            'post_parent' => $post->ID,
            'numberposts' => 1
        ) );

        if ( $attachments ) {

            foreach ( $attachments as $attachment ) {
                $class = "post-attachment mime-" . sanitize_title( $attachment->post_mime_type );
                $title = wp_get_attachment_link( $attachment->ID,'', false,false,"Download Now" );
                //$strip = array('<a href=','</a>',"'");
                //$content .= str_replace($strip,'',$title);
                $title  = str_replace('http:','https:',$title);
                $content = ($title);

            }

        }
    }

    return $content;
}

//***********
// temporary function ... was never defined
//***********

function plugin_filesize($id) {
    return '(Size of post ID '.$id.')';
}

function getSizeFile($url) {
    if (substr($url,0,4)=='http') {
        $x = array_change_key_case(get_headers($url, 1),CASE_LOWER);
        if ( strcasecmp($x[0], 'HTTP/1.1 200 OK') != 0 ) { $x = $x['content-length'][1]; }
        else { $x = $x['content-length']; }
    }
    else { $x = @filesize($url); }

    return $x;
}


//**************************************************
// Filter to pull out documentation pdf
// Not actively used - purely for testing purposes
//**************************************************/

function my_documentation_filter( $content ) {
    global $post;
    //$args =  array( 'post_type' => 'attachment', 'numberposts' => , 'post_status' => null, 'post_parent' => $post->ID, 'post_mime_type' => 'application/zip' );

    if ( is_single() && $post->post_type == 'plugin' && $post->post_status == 'publish' ) {
        $attachments = $attachments = get_posts( array(
            'post_type' => 'attachment',
            'posts_per_page' => 0,
            'post_mime_type' => 'application/msword, application/vnd.ms-excel, application/pdf, application/txt, text/plain',
            'post_parent' => $post->ID,
            'numberposts' => 1
        ) );

        if ( $attachments ) {

            foreach ( $attachments as $attachment ) {
                $class = "post-attachment mime-" . sanitize_title( $attachment->post_mime_type );
                $title = wp_get_attachment_link( $attachment->ID,'', false,false,"PDF" );
                //$strip = array('<a href=','</a>',"'");
                //$content .= str_replace($strip,'',$title);
                $content = ($title);


            }

        }
    }

    return $content;
}
//*************************************************
// Filter to grab icon image
// Not actively used - purely for testing purposes
//*************************************************/
function my_the_icon_filter( $content ) {
    global $post;

    if ( is_single() && $post->post_type == 'plugin' && $post->post_status == 'publish' ) {
        $attachments = get_posts( array(
            'post_type' => 'attachment',
            'posts_per_page' => 0,
            'post_parent' => $post->ID,
            'numberposts' => 1,
            'post_mime_type' => 'image'
        ) );

        if ( $attachments ) {

            foreach ( $attachments as $attachment ) {
                $class = "post-attachment mime-" . sanitize_title( $attachment->post_mime_type );
                $title = wp_get_attachment_link( $attachment->ID, false, '' );
                $strip = array('href=',"'","<a");
                $title = str_replace($strip,'',$title);
                $content .= '<img src =' . $title;

            }

        }
    }

    return $content;
}
//***************************************************
// Filter to loop through Screenshots
// Not actively used - purely for testing purposes
//***************************************************/
function my_the_screenshot_filter( $content ) {
    global $post;

    if ( is_single() && $post->post_type == 'plugin' && $post->post_status == 'publish' ) {
        $attachments = get_posts( array(
            'post_type' => 'attachment',
            'posts_per_page' => 0,
            'post_parent' => $post->ID,
            'order'=> 'DESC',
            'numberposts' => 5,
            'post_mime_type' => 'image',
            'offset' => 1
        ) );

        if ( $attachments ) {

            foreach ( $attachments as $attachment ) {
                $class = "post-attachment mime-" . sanitize_title( $attachment->post_mime_type );
                $title = wp_get_attachment_link( $attachment->ID, false, '' );
                $strip = array('href=',"'","<a");
                $title = str_replace($strip,'',$title);
                $content .= '<li><li>
			  <div class="mosaic-block circle">
			<a href="#" class="mosaic-overlay">&nbsp;</a>
			<div class="mosaic-backdrop"><img class="lightbox" src =' . $title. '></div></div></li>';

            }

        }
    }

    return $content;
}

//**********************************************
// Custom Slide Description Length
//**********************************************/

function new_slide_length($content,$length) {
    $text = substr($content, 0,$length);
    return $text;
}

//*******************************************
// Custom Excerpt Length
//*******************************************/

// Variable & intelligent excerpt length.
function print_excerpt($length) { // Max excerpt length. Length is set in characters
    global $post;
    $text = $post->post_excerpt;
    if ( '' == $text ) {
        $text = get_the_content('');
        $text = apply_filters('the_content', $text);
        $text = str_replace(']]>', ']]>', $text);
    }
    $text = strip_shortcodes($text); // optional, recommended
    $text = strip_tags($text,'<p class="excerpt">'); // use ' $text = strip_tags($text,'<p><a>'); ' if you want to keep some tags

    $text = substr($text,0,$length);
    $excerpt = reverse_strrchr($text, '.', 1);
    if( $excerpt ) {
        echo apply_filters('the_excerpt',$excerpt);
    } else {
        echo apply_filters('the_excerpt',$text);
    }
}

// Custom title length

function print_title($length) { // Max excerpt length. Length is set in characters
    global $post;
    $text = $post->post_title;
    if ( '' == $text ) {
        $text = the_title('');
        $text = apply_filters('the_title', $text);
        $text = str_replace(']]>', ']]>', $text);
    }
    $text = strip_shortcodes($text); // optional, recommended
    $text = strip_tags($text,'<p class="title">'); // use ' $text = strip_tags($text,'<p><a>'); ' if you want to keep some tags

    $text = substr($text,0,$length);

    $title = reverse_strrchr($text, '.', 1);
    if( $title ) {
        echo apply_filters('the_title',$title);
    } else {
        echo apply_filters('the_title',$text);
    }
}
//add_filter( 'print_title', 'add_nrg_icon' );
//add_filter( 'the_title' , 'add_nrg_icon');

function add_nrg_icon($before = '[html code here]', $after = '', $echo = false) {
    $title = get_the_title();

    if ( strlen($title) == 0 )
        return;
    $imgname = get_template_directory_uri().'/images/approved.png';

    $title = $title.'<img src="'.$imgname.'" />';


    $title = $before . $title . $after;

    if ( $echo )
        echo  $title;
    else
        return	$title;
}
// Returns the portion of haystack which goes until the last occurrence of needle
function reverse_strrchr($haystack, $needle, $trail) {
    return strrpos($haystack, $needle) ? substr($haystack, 0, strrpos($haystack, $needle) + $trail) : false;
}
//*******************************************
// Trim Title Function
//*******************************************/
function ShortenText($text)
{
    // Change to the number of characters you want to display
    $chars = 4;
    $text = $text." ";
    $text = substr($text,0,$chars);
    $text = substr($text,0,strrpos($text,' '));
    $text = $text."...";
    return $text;
}

//********************************************
// Get total number of custom posts
//********************************************/
function wt_get_custom_taxonomy_count($ptype,$pterm) {
    global $wpdb;
    $SQL  = "SELECT count(*) FROM $wpdb->posts posts, $wpdb->terms terms, $wpdb->term_relationships rels ";
    $SQL .= "WHERE posts.post_status = 'publish' ";
    $SQL .= "AND post_type = '".$ptype."' ";
    $SQL .= "AND posts.ID = rels.object_id ";
    if($pterm!=""):
        $SQL .= "AND terms.slug = '".$pterm."' ";
    endif;
    $SQL .= "AND terms.term_id = rels.term_taxonomy_id";
    return $wpdb->get_var($SQL);
}

//*******************************************
// Add extra fields to the user profile page
//*******************************************/
function fb_add_custom_user_profile_fields( $user ) {
    ?>
    <h3><?php _e('Extra Profile Information', 'your_textdomain'); ?></h3>
    <table class="form-table">
        <tr>
            <th>
                <label for="institution"><?php _e('Institution', 'your_textdomain'); ?>
                </label></th>
            <td>
                <input type="text" name="institution" id="institution" value="<?php echo esc_attr( get_the_author_meta( 'institution', $user->ID ) ); ?>" class="regular-text" /><br />
                <span class="institution"><?php _e('Please enter your institution.', 'your_textdomain'); ?></span>
            </td>
        </tr>
        <tr>
            <th width="100%">
                <label for="avatar"><?php _e('avatar', 'your_textdomain'); ?>
                </label></th>
            <td width="250" align="left">
                <input type="file" name="avatar" id="avatar" value="<?php echo esc_attr( get_the_author_meta( 'avatar', $user->ID ) ); ?>" class="regular-text" /><br />
                <span class="avatar"><?php _e('avatar', 'your_textdomain'); ?></span>
            </td>
            <td width="100" align="left">
                <img src="<?php echo esc_attr( get_the_author_meta( 'avatar', $user->ID ) ); ?>">
            </td>
        </tr>
    </table>


<?php }
function fb_save_custom_user_profile_fields( $user_id ) {
    if ( !current_user_can( 'edit_user', $user_id ) )
        return FALSE;
    $avatar = wp_handle_upload( $_FILES['avatar'], array( 'test_form' => false) );
    update_usermeta( $user_id, 'institution', $_POST['institution'] );
    update_usermeta( $user_id, 'avatar', $avatar['url'] );
}
add_action( 'show_user_profile', 'fb_add_custom_user_profile_fields' );
add_action( 'edit_user_profile', 'fb_add_custom_user_profile_fields' );
add_action( 'personal_options_update', 'fb_save_custom_user_profile_fields' );
add_action( 'edit_user_profile_update', 'fb_save_custom_user_profile_fields' );

//*********************************************************
// Custom Search for taxonomy
/**********************************************************/

add_filter('relevanssi_hits_filter', 'separate_result_types');
function separate_result_types($hits) {
    $types = array();

    // Split the post types in array $types
    if (!empty($hits)) {
        foreach ($hits[0] as $hit) {
            if (!is_array($types[$hit->post_type])) $types[$hit->post_type] = array();
            array_push($types[$hit->post_type], $hit);
        }
    }

    // Merge back to $hits in the desired order
    $hits[0] = array_merge($types['mycustomtypethatgoesfirst'], $types['thesecondmostimportanttype'], $types['post'], $types['pages']);
    return $hits;
}
//******************************************
// TAX TO CLASS FUNCTION Plugins tax
//*****************************************/
// Add custom taxonomies to the post class

add_filter( 'post_class', 'custom_taxonomy_post_class', 10, 3 );

if( !function_exists( 'custom_taxonomy_post_class' ) ) {

    function custom_taxonomy_post_class( $classes, $class, $ID ) {

        $taxonomy = 'plugins';

        $terms = get_the_terms( (int) $ID, $taxonomy );

        if( !empty( $terms ) ) {

            foreach( (array) $terms as $order => $term ) {

                if( !in_array( $term->slug, $classes ) ) {

                    $classes[] = $term->slug;

                }

            }

        }

        return $classes;

    }

}

//Add custom taxonomies to the post class
add_filter('post_class', 'custom_taxonomy_post_class_two', 10, 3);
if (!function_exists('custom_taxonomy_post_class_two')) {
    function custom_taxonomy_post_class_two($classes, $class, $ID) {
        $taxonomy = 'tools';
        $terms = get_the_terms((int) $ID, $taxonomy);
        if (!empty($terms)) {
            foreach ((array) $terms as $order => $term) {
                if (!in_array( $term->slug, $classes)) {
                    $classes[] = $term->slug;
                }
            }
        }
        return $classes;
    }
}

//if user is not an admin, make sure they can only see their own posts in the admin section
function posts_for_current_author($query) {
    if($query->is_admin && !current_user_can('administrator')) {
        global $user_ID;
        $query->set('author',  $user_ID);
    }
    return $query;
}
add_filter('pre_get_posts', 'posts_for_current_author');



//********************************************************
// Count terms for Plugin tax
//*******************************************************/
function termcountplugin($terms) {
    $count = 0;
    $argsone = array(
        'plugins' => 'admin,datatype,display,other,pipeline',
        'posts_per_page' => -1,
        'post_type' => 'plugin',
        'tag_slug__in' => $terms
    );

    $the_query_two = query_posts($argsone);
    if ($the_query_two) {
        while (have_posts()) {
            the_post();
            $count++;
        }
    }

    return $count;
    wp_reset_query();
}


//********************************************************
// Count terms for Plugins also in Packages
//*******************************************************/
function term_count_package_plugin($terms) {
    $count = 0;
    $myquery['tax_query'] = array(
        array(
            'taxonomy' => 'plugins',
            'terms' => array('admin','datatype','display','other','pipeline'),
            'field' => 'slug',
        ),
        array(
            'taxonomy' => 'packages',
            'terms' => array($terms),
            'field' => 'slug',
        ),
    );
    $the_query_two = query_posts($myquery);

    if ( $the_query_two ) :
        while ( have_posts() ) : the_post();
            $count++;
        endwhile; endif;


    return $count;

    wp_reset_query();
}

//********************************************************
// Count terms for Tools also in Packages
//*******************************************************/
function term_count_package_tool($terms) {
    $count = 0;


    $myquery['tax_query'] = array(
        array(
            'taxonomy' => 'tools',
            'terms' => array('apps','integration','scripts','other'),
            'field' => 'slug',
        ),
        array(
            'taxonomy' => 'packages',
            'terms' => array($terms),
            'field' => 'slug',
        ),
    );
    $the_query_two = query_posts($myquery);

    if ( $the_query_two ) :
        while ( have_posts() ) : the_post();
            $count++;
        endwhile; endif;


    return $count;

    wp_reset_query();
}

//********************************************************
// Count terms for Tools tax
//*******************************************************/
function termcounttool($term) {
    $counts = 0;
    $args = array(
        'tools' => 'apps,integration,other,scripts',
        'showposts' => -1,
        'post_type' => 'plugin',
        'tag_slug__in' => $term
    );
    $the_query = query_posts($args);
    //$the_query = new WP_Query( $args );

    if ( $the_query ) :
        while ( have_posts() ) : the_post();
            $counts++;
        endwhile; endif;


    return $counts;
    wp_reset_query();
}

//********************************************************
// Count search results for Tools tax
//*******************************************************/
function searchcounttool($term) {
    $counts = 0;
    $args = array(
        'tools' => 'apps,integration,other,scripts',
        'showposts' => -1,
        'post_type' => 'plugin',
        'tag_slug__in' => $term
    );
    $the_query = query_posts($args);
    //$the_query = new WP_Query( $args );

    if ( $the_query ) :
        while ( have_posts() ) : the_post();
            $counts++;
        endwhile; endif;


    return $counts;
    wp_reset_query();
}

//***********************************************
// Spit out size in clean format
//**********************************************/
function cleansize($size) {
    $sz_fmt = size_format($size, 2);
    return $sz_fmt;
}

//*********************************************
// WP Get cat id and url
//********************************************/
function getcaturl($tax) {
    global $post;
    $category_id = get_cat_ID( $tax );
    $category_link = get_category_link( $category_id );
    return $category_id;
}

//*********************************************
// Get logged in user's name
//********************************************/
function theusername() {
    global $current_user;
    get_currentuserinfo();
    $the_user = $current_user->display_name;
    return $the_user;
}

//*********************************************
// Get logged in user's url
//********************************************/
function theuserurl() {
    global $current_user;
    get_currentuserinfo();
    $the_user_url = $current_user->url;
    return $the_user_url;
}


//********************************************
//Close our addon metabox by default
//********************************************/
function closed_meta_boxes( $closed ) {
    if ( false === $closed )
        $closed = array( 'postcustom','authordiv','commentsdiv' );
    return $closed;
}
add_filter( 'get_user_option_closedpostboxes', 'closed_meta_boxes' );


//*******************************************
//Redirect users on login
//************************************************/
// Redirect admins to the dashboard and other users elsewhere
add_filter('login_redirect', 'my_login_redirect', 10, 3);
function my_login_redirect( $redirect_to, $request, $user ) {
    return home_url('/wp-admin/admin.php?page=xnat-welcome');
}


//**********************************
//Remove unused dashboard widgets
//**********************************
function remove_dashboard_meta() {
//        remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
//        remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
//       remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
//        remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
//        remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
//        remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
//        remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
//        remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
//        remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');//since 3.8
}
add_action( 'admin_init', 'remove_dashboard_meta' );


//*******************************************
//Redirect users on logout
//************************************************/
function get_current_logout( $logout_url ){
    if ( !is_admin() ) {
        $logout_url = add_query_arg('redirect_to', urlencode(( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']), $logout_url);
    }

    return $logout_url;
}

add_filter('logout_url', 'get_current_logout');

function change_submenus() {
    global $submenu;
    unset($submenu['edit.php?post_type=plugin'][10]); // Removes 'Add New'.
    unset($submenu['post-new.php?post_type=plugin'][10]);
    $submenu['profile.php'][5][0] = "Personal Profile";
}
add_action('admin_menu', 'change_submenus');

//*******************************************
//Hide add new
//************************************************/
function hide_add_new_addon() {
    if (get_post_type() == 'plugin')
        echo '<style type="text/css">
	#favorite-actions {display:none;}
	.add-new-h2{display:none;}
	.tablenav{display:none;}
	</style>';
}
add_action('admin_head', 'hide_add_new_addon');

//*********************************************
// Limit contributors view in dashboard
//***********************************************/
function mine_published_only($views) {
    if (!current_user_can('promote_users')) {
        unset($views['all']);
        unset($views['publish']);
        unset($views['trash']);
        unset($views['draft']);
    }

    return $views;
}


add_filter('views_edit-plugin', 'mine_published_only');

//*********************************************
// Feauturd post metabox
//***********************************************/
add_action( 'add_meta_boxes', 'cd_meta_box_add' );
function cd_meta_box_add()
{
    global $current_user;
    get_currentuserinfo();
    if(is_admin()) {
        if (current_user_can('administrator')) {
            add_meta_box( 'featured-homepage-addon', 'Featured Homepage Addon', 'cd_meta_box_cb', 'plugin', 'normal', 'high' );
        }
        add_meta_box( 'edit-plugin', 'Edit Add-On Information', 'edit_plugin_meta_box_cb', 'plugin', 'normal', 'high' );
    }
}
//*********************************************
//Create the box
//********************************************/
function cd_meta_box_cb( $post )
{
    $values = get_post_custom( $post->ID );

    $selected = isset( $values['my_meta_box_select'] ) ? esc_attr( $values['my_meta_box_select'][0] ) : "";
    wp_nonce_field( 'my_meta_box_nonce', 'meta_box_nonce' );
    ?>

    <p>
        <label for="my_meta_box_select">Featured Homepage Addon ?</label>
        <select name="my_meta_box_select" id="my_meta_box_select">
            <option value="notfeatured" <?php selected( $selected, 'notfeatured' ); ?>>Not Featured</option>
            <option value="featured" <?php selected( $selected, 'featured' ); ?>>Featured</option>
        </select>
    </p>
    <?php
}
//***************************************
// Save the box
//***************************************/

add_action( 'save_post', 'featured_plugin_meta_box_save' );
function featured_plugin_meta_box_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'my_meta_box_nonce' ) ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;



    // Make sure your data is set before trying to save it


    if( isset( $_POST['my_meta_box_select'] ) )
        update_post_meta( $post_id, 'my_meta_box_select', esc_attr( $_POST['my_meta_box_select'] ) );


}
//*********************************************
//Create the backend metabox that allows
//the user to update plugin items
//********************************************/
function edit_plugin_meta_box_cb($post)
{
    $values = get_post_custom( $post->ID );
    $icon = get_post_meta($post->ID, "upload_5", true);
    $credit = get_post_meta($post->ID, "credits", true);
    $download = get_post_meta($post->ID, "upload_6", true);
    $compatible = explode(',', get_post_meta($post->ID, "compatible", true));
    $nrgapproved = get_post_meta($post->ID, "nrgapproved", true);
    $version = get_post_meta($post->ID, "version", true);
    $license = get_post_meta($post->ID, "license", true);
    $licenseCustom = get_post_meta($post->ID, "license_custom", true);
    $documentation = get_post_meta($post->ID, "upload_4", true);
    $repo = get_post_meta($post->ID, "repo", true);
    $downloadFromRepo = get_post_meta($post->ID, "download_from_repo", true);
    $accesses = explode(',', get_post_meta($post->ID, "item", true));
    $defaulticon = get_post_meta($post->ID, "defaulticon", true);
    $alpha = get_post_meta($post->ID, "alpha", true);
    $environment = get_post_meta($post->ID, "env_description", true);
    $alphatype = get_post_meta($post->ID, "alpha", true);
    $screenshot1 = get_post_meta($post->ID, "upload_1", true);
    $screenshot2 = get_post_meta($post->ID, "upload_2", true);
    $screenshot3 = get_post_meta($post->ID, "upload_3", true);

    wp_nonce_field( 'plugin_meta_box_nonce', 'edit-plugin_meta_box_nonce' );
    ?>

    <!-- NRG Approved -->
    <fieldset class="nrgapproved">
        <label>
            <input type="checkbox" value="nrg-approved" name="nrgapproved" <?php echo ($nrgapproved) ? "checked" : "" ?> />
            NRG Approved
        </label>
    </fieldset>

    <!-- plugin version -->
    <fieldset name="version">
        <label for="version">Add-On Version*</label>
        <input type="text" id="version" class="validate[required]" value="<?php echo $version;?>" tabindex="5" name="version" />
    </fieldset>

    <!-- license agreement -->
    <fieldset name="license">
        <label for="license">License Agreement</label>
        <select name="license" id="license" title="Please select a license agreement">
            <option>--Select--</option>
            <option value="Apache License 2.0" <?php echo ($license=="Apache License 2.0") ? "selected" : '' ?>>Apache License 2.0</option>
            <option value="BSD 2-Clause" <?php echo ($license=="BSD 2-Clause") ? "selected" : '' ?>>BSD 3-Clause "Simplified" or "FreeBSD"</option>
            <option value="BSD 3-Clause" <?php echo ($license=="BSD 3-Clause") ? "selected" : '' ?>>BSD 3-Clause "New" or "Revised"</option>
            <option value="GPL" <?php echo ($license=="GPL") ? "selected" : '' ?>>GNU General Public License (GPL)</option>
            <option value="LGPL" <?php echo ($license=="LGPL") ? "selected" : '' ?>>GNU Library / "Lesser" General Public License (LGPL)</option>
            <option value="MIT" <?php echo ($license=="MIT") ? "selected" : '' ?>>MIT License</option>
            <option value="Mozilla Public License 2.0" <?php echo ($license=="Mozilla Public License 2.0") ? "selected" : '' ?>>Mozilla Public License 2.0</option>
            <option value="CDDL" <?php echo ($license=="CDDL") ? "selected" : '' ?>>Common Development and Distribution License (CDDL)</option>
            <option value="Eclipse Public License" <?php echo ($license=="Eclipse Public License") ? "selected" : '' ?>>Eclipse Public License</option>
        </select>
    </fieldset>

    <fieldset name="license_custom">
        <label for="license_custom">License Agreement (Custom) <span>Overrides value of License Agreement</span></span></label>
        <input type="text" id="license_custom" name="license_custom" value="<?php echo $licenseCustom ?>" tabindex="6" />
    </fieldset>

    <!-- Alpha/Beta -->
    <fieldset name="alpha">
        <label for="alpha">Release State</label>
        <select name="alpha" id="alpha">
            <option value="">--Select--</option>
            <option value="production" <?php echo ($alpha == "production") ? "selected" : ""; ?>>Production</option>
            <option value="alpha" <?php echo ($alpha == "alpha") ? "selected" : ""; ?>>Alpha</option>
            <option value="beta" <?php echo ($alpha == "beta") ? "selected" : ""; ?>>Beta</option>
        </select>
    </fieldset>

    <!-- Credits -->
    <fieldset class="credits">
        <label for="credits">Credits <span class="clarify">- Enter names of others who helped build this add-on</span><span></span></label>
        (separate multiple names with a comma)<br>
        <input type="text" class="longest" value="<?php echo $credit; ?>" class="validate[required]" id="credits" tabindex="20" name="credits" />
    </fieldset>


    <!-- post Content -->
    <fieldset class="env_description">
        <label for="env_description">Describe the environment where the add-on is currently running:</label>
        <textarea id="env_description" class="validate[required]" tabindex="15" name="env_description" cols="80" rows="10"><?php echo $environment; ?></textarea>
    </fieldset>

    <?php //if ($screenshot1) : ?>

    <!-- post Screen Captures-->

    <fieldset class="captures">
        <label class="fileupload" for="captures">Screen Capture 1</label>
        <input type="file" class="text-input id" id="screen_captures" accept="image/png, image/jpg, image/bmp"	value="Choose file" name="upload_1"/>
        <img class=""  src="<?php if( get_post_meta($post->ID, "upload_1", true) ): echo get_post_meta($post->ID, "upload_1", true); endif; ?>">
    </fieldset>

    <?php //endif; ?>

    <?php //if ($screenshot2) : ?>

    <!-- post Screen Captures-->

    <fieldset class="captures">
        <label class="fileupload" for="captures">Screen Capture 2</label>
        <input type="file" class="text-input id" id="screen_captures_two" accept="image/png, image/jpg, image/bmp"	value="Choose file" name="upload_2"/>
        <img class=""	 src="<?php if( get_post_meta($post->ID, "upload_2", true) ): echo get_post_meta($post->ID, "upload_2", true); endif; ?>">

    </fieldset>

    <?php //endif; ?>

    <?php //if ($screenshot3) : ?>

    <!-- post Screen Captures-->

    <fieldset class="captures">
        <label class="fileupload" for="captures">Screen Capture 3</label>
        <input type="file" class="text-input id" id="screen_captures_three" accept="image/png, image/jpg, image/bmp"  value="Choose file" name="upload_3"/>
        <img class=""  src="<?php if( get_post_meta($post->ID, "upload_3", true) ): echo get_post_meta($post->ID, "upload_3", true); endif; ?>">


    </fieldset>

    <?php //endif; ?>

    <!-- post Documentation-->
    <fieldset class="documentation fileupload">
        <label class="fileupload" for="documentation">Documentation</label><span>(acceptable formats:.pdf,.doc,.xls,.txt)</span>
        <input type="file" class="text-input" id="add_docs"	 accept="application/msword, application/vnd.ms-excel, application/pdf, application/txt, text/plain" tabindex="16" value="Choose file" name="upload_4"/>
        <p><label>Or, enter URL to file</label></p>
        <input type="text" id="documentation_input" class="longer" tabindex="15" name="upload_4"/>
        <?php if ($documentation): ?><a href="<?php echo $documentation; ?>">View Current Documentation</a><?php endif; ?>
    </fieldset>

    <fieldset class="repo">
        <div>
            <label for="repo">Source Repository</label>
            <input type="text" class="longest" value="<?php echo $repo; ?>" class="" id="repo" name="repo" />
        </div>
        <div>
            <label for="download_from_repo">Direct users to source repository for downloads</label>
            <select name="download_from_repo">
                <option value="yes" <?php if ($downloadFromRepo == "yes") echo "selected" ?>>Yes</option>
                <option value="no" <?php if (!$downloadFromRepo || $downloadFromRepo !== "yes") echo "selected" ?>>No</option>
            </select>
        </div>
    </fieldset>

    <fieldset class="icon fileupload">
        <label class="fileupload" for="icon">Icon</label><div>(acceptable formats:.png,.jpg)</div>
        <input type="file" class="text-input" id="add_icon" tabindex="16" accept="image/png, image/jpg, image/bmp"	value="Choose file" name="upload_5"/>
        <input type="hidden" class="text-input" id="default_icon" name="defaulticon" value="">
        <p>Or, select a default icon from the list below</p>
        <div id="default-iconset">
            <ul>
                <li id="icon-brain"><img src="<?php bloginfo('template_directory'); ?>/images/icons/Brain.png" alt="Brian" title="Brian" /></li>
                <li id="icon-heart"><img src="<?php bloginfo('template_directory'); ?>/images/icons/Heart.png" alt="Heart" title="Heart" /></li>
                <li id="icon-internal"><img src="<?php bloginfo('template_directory'); ?>/images/icons/Internal.png" alt="Internal" title="Internal" /></li>
                <li id="icon-lungs"><img src="<?php bloginfo('template_directory'); ?>/images/icons/Lungs.png" alt="Lungs" title="Lungs" /></li>
                <li id="icon-mental"><img src="<?php bloginfo('template_directory'); ?>/images/icons/Mental-Health.png" alt="Mental" title="Mental" /></li>
                <li id="icon-MRI"><img src="<?php bloginfo('template_directory'); ?>/images/icons/MRI.png" alt="MRI" title="MRI" /></li>
                <div style="clear: both;"></div>
            </ul>
        </div>
        <p>
            <span><label>Current Icon:</label></span><img class="current-icon"  src="<?php if($icon !== ""){ echo $icon;} else { echo $defaulticon; } ?>">
        </p>

    </fieldset>

    <fieldset class="compatability" name="compatablility">
        <label class="fileupload" for="compatability">Compatible with XNAT version*</label><div>(select all that apply)</div><br>
        <label class="checkboxlabel" for="plugin">
            <input type="checkbox" class="checkbox" value="1.5" name="compatible[]" <?php if (in_array('1.5', $compatible)) echo "checked" ?> />XNAT 1.5</a></label>
        <br><span>(XNAT 1.5 does not support modules. Please select this only for appropriate tools and scripts.)</span>
        <br><br><span><strong>XNAT Modules (1.6.x only)</strong></span><br>
        <label class="checkboxlabel">
            <input type="checkbox" class="checkbox" value="1.6" name="compatible[]" <?php if (in_array('1.6', $compatible)) echo "checked" ?> />XNAT 1.6.x</label>
        <label class="checkboxlabel">
            <input type="checkbox" class="checkbox" value="1.6.2.1" name="compatible[]" <?php if (in_array("1.6.2.1", $compatible)) echo "checked" ?>/>XNAT 1.6.2.1</label>
        <label class="checkboxlabel">
            <input type="checkbox" class="checkbox" value="1.6.3" name="compatible[]" <?php if (in_array('1.6.3', $compatible)) echo "checked" ?> />XNAT 1.6.3</label>
        <label class="checkboxlabel">
            <input type="checkbox" class="checkbox" value="1.6.4" name="compatible[]" <?php if (in_array("1.6.4", $compatible)) echo "checked" ?>/>XNAT 1.6.4</label>
        <label class="checkboxlabel">
            <input type="checkbox" class="checkbox" value="1.6.5" name="compatible[]" <?php if (in_array("1.6.5", $compatible)) echo "checked" ?>/>XNAT 1.6.5</label>
        <br><br><span><strong>XNAT Plugins (1.7.x and later)</strong></span><br>
        <label class="checkboxlabel">
            <input type="checkbox" class="checkbox" value="1.7" name="compatible[]" <?php if (in_array("1.7", $compatible)) echo "checked" ?>/>XNAT 1.7</label>
        <label class="checkboxlabel">
            <input type="checkbox" class="checkbox" value="1.7.1" name="compatible[]" <?php if (in_array("1.7.1", $compatible)) echo "checked" ?>/>XNAT 1.7.1</label>
        <label class="checkboxlabel">
            <input type="checkbox" class="checkbox" value="1.7.2" name="compatible[]" <?php if (in_array("1.7.2", $compatible)) echo "checked" ?>/>XNAT 1.7.2</label>
        <label class="checkboxlabel"><input type="checkbox" class="checkbox" value="1.7.3" name="compatible[]" <?php if (in_array("1.7.3", $compatible)) echo "checked" ?>/>XNAT 1.7.3</label>
        <label class="checkboxlabel"><input type="checkbox" class="checkbox" value="1.7.4" name="compatible[]" <?php if (in_array("1.7.4", $compatible)) echo "checked" ?>/>XNAT 1.7.4</label>
    </fieldset>

    <fieldset class="fileupload" name="addon_access" id="addon_access">
        <label for="addon_access">This Add-On Affects:*</label><div>(check all that apply)</div><br>
        <label class="checkboxlabel">
            <input type="checkbox" class="checkbox" value="velocity-html" tabindex="19" name="item[]" <?php if (in_array('velocity-html', $accesses)) echo "checked" ?> />
            Velocity/HTML Template</label><br>
        <label class="checkboxlabel">
            <input type="checkbox" class="checkbox" value="database" tabindex="19" name="item[]" <?php if (in_array('database', $accesses)) echo "checked" ?> />
            Database Schema</label><br>
        <label class="checkboxlabel">
            <input type="checkbox" class="checkbox" value="java" tabindex="19" name="item[]" <?php if (in_array('java', $accesses)) echo "checked" ?> />
            Java Classes</label><br>
        <label class="checkboxlabel">
            <input type="checkbox" class="checkbox" value="javascipt" tabindex="19" name="item[]" <?php if (in_array('javascript', $accesses)) echo "checked" ?> />
            Javascript Functions</label><br>
        <label class="checkboxlabel">
            <input type="checkbox" class="checkbox" value="css" tabindex="19" name="item[]" <?php if (in_array('css', $accesses)) echo "checked" ?> />
            CSS Style Sheet</label><br>
        <label class="checkboxlabel">
            <input type="checkbox" class="checkbox" value="project-dependencies" tabindex="19" name="item[]" <?php if (in_array('project-dependencies', $accesses)) echo "checked" ?> />
            Project Dependencies</label><br>
        <label class="checkboxlabel">
            <input type="checkbox" class="checkbox" value="spring" tabindex="19" name="item[]" <?php if (in_array('spring', $accesses)) echo "checked" ?> />
            Spring Configuration Files (advanced)</label><br>
        <label class="checkboxlabel">
            <input type="checkbox" class="checkbox" value="rest" tabindex="19" name="item[]" <?php if (in_array('rest', $accesses)) echo "checked" ?> />
            REST API</label>
    </fieldset>

    <fieldset class="upload-addon">
        <div>
            <label for="upload_6">Media File Location</label>
            <input type="text" readonly value="<?php echo get_post_meta($post->ID,'upload_6',true); ?>" style="width: 700px;" />
        </div>
        <div>
            <label for="submit_addon" class="fileupload">Upload Add-On File*</label><div>(acceptable format: .zip)</div>
            <input type="file" accept="application/zip"  value="Choose file" class="validate[required] text-input" tabindex="40" id="upload_addon" name="upload_addon" />
            <input type="hidden" value="<?php echo $upload; ?>" name="fileupload"/>
        </div>

    </fieldset>


    <?php
}



//***************************************
// Save the metabox values
//***************************************/

add_action( 'save_post', 'plugin_meta_box_save' );
function plugin_meta_box_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['edit-plugin_meta_box_nonce'] ) || !wp_verify_nonce( $_POST['edit-plugin_meta_box_nonce'], 'plugin_meta_box_nonce' ) ) return;

    if (isset( $_POST['credits'] ) ) {
        update_post_meta( $post_id, 'credits', esc_attr( $_POST['credits'] ) );
    }

    if (isset( $_POST['env_description'] ) ) {
        update_post_meta( $post_id, 'env_description', esc_attr( $_POST['env_description'] ) );
    }

    if (isset( $_POST['version'] ) ) {
        update_post_meta( $post_id, 'version', esc_attr($_POST['version']));
    }

    if (isset( $_POST['license'] ) ) {
        update_post_meta( $post_id, 'license', esc_attr($_POST['license']));
    }
    if (isset($_POST['license_custom'])) {
        update_post_meta( $post_id, 'license_custom', esc_attr($_POST['license_custom']));
    }

    if (isset( $_POST['nrgapproved'] )) {
        update_post_meta( $post_id, 'nrgapproved', esc_attr( $_POST['nrgapproved'] ));
        wp_set_post_terms( $post_id, 'nrgapproved', 'packages', true );
    }
    else {
        update_post_meta( $post_id, 'nrgapproved', '');
        wp_remove_object_terms( $post_id, 'nrgapproved', 'packages');
    }

    if (isset($_POST['compatible'])) {
        $compatible = implode(',', $_POST['compatible']);
        update_post_meta($post_id, 'compatible', $compatible);
    }

    if (isset($_POST['item'])) {
        $accesses = implode(',', $_POST['item']);
        update_post_meta($post_id, 'item', $accesses);
    }

    if (isset( $_POST['alpha'] ) ) {
        update_post_meta( $post_id, 'alpha', esc_attr( $_POST['alpha'] ) );
    }
    if (isset( $_POST['documentation'])) {
        update_post_meta ($post_id, 'documentation', esc_attr( $_POST['documentation']));
    }
    if (isset( $_POST['repo'])) {
        update_post_meta ($post_id, 'repo', esc_attr( $_POST['repo']));
    }
    if (isset( $_POST['download_from_repo'])) {
        update_post_meta ($post_id, 'download_from_repo', esc_attr( $_POST['download_from_repo'] ));
    }

    if (!empty($_FILES['upload_addon']['tmp_name'])) {
        //print_r($_FILES); print_r($_POST); exit;
        delete_post_meta($_POST['post_ID'], 'upload_6');
        insert_attachment('upload_addon', $_POST['post_ID'], 6);
    }

    if (isset( $_FILES['upload_1'] ) ) {
        update_custom_meta_data($post_id, 'upload_1', true);
    }

    if (isset( $_FILES['upload_2'] ) ) {
        update_custom_meta_data($post_id, 'upload_2', true);
    }

    if (isset( $_FILES['upload_3'] ) ) {
        update_custom_meta_data($post_id, 'upload_3', true);
    }

    if (isset( $_FILES['upload_4'] ) ) {
        update_custom_meta_data($post_id, 'upload_4', true);
    } elseif (isset( $_POST['upload_4'])) {
        update_post_meta( $post_id, 'upload_4', esc_attr( $_POST['upload_4'] ) );
    }

    if (!empty($_FILES['upload_5']['tmp_name']) && $_FILES["icon"] !== '' ) {
        update_custom_meta_data($post_id, 'upload_5', true);
        delete_post_meta( $post_id, 'defaulticon', $defaulticon );
    }

    if (isset( $_POST['defaulticon']) && $_POST["defaulticon"] !== '') {
        update_post_meta( $post_id, 'defaulticon', $_POST['defaulticon'] );
        delete_post_meta( $post_id, 'upload_5', $icon );
    }


}

//************************************************
// handle metabox uploads
//************************************************/
function update_custom_meta_data($id, $data_key, $is_file = false) {

    if($is_file) {

        if(!empty($_FILES[$data_key]['name'])) {

            $upload = wp_handle_upload($_FILES[$data_key], array('test_form' => false));

            if(isset($upload['error'])) {
                wp_die('There was an error uploading your file. Please try again.');
            } else {
                add_post_meta($id, $data_key, $upload['url']);
                update_post_meta($id, $data_key, $upload['url']);
            } // end if/else

        } // end if

    } else {

        $data = $_POST[$data_key];
        add_post_meta($id, $data_key, $data);
        update_post_meta($id, $data_key, $data);

    } // end if/else

}

function register_admin_scripts() {
    wp_register_script('custom-admin-script', get_stylesheet_directory_uri() . '/js/custom-admin-script.js');
    // wp_enqueue_script( 'uniform', get_template_directory_uri() . '/js/jquery.uniform.min.js', array('jquery'));
    wp_enqueue_script('custom-admin-script');
}
add_action('admin_enqueue_scripts', 'register_admin_scripts');

//************************************************
// Remove the shaking effect from the search box
//************************************************/
function my_no_login_shake() {
    remove_action( 'login_head', 'wp_shake_js', 12 );
}
add_action('login_head','my_no_login_shake');

//***********************************************
// Add capabilities to contributor user role
//**********************************************/
function contri_up() {
    $role = get_role( 'contributor' ); //gets the contributor role
    $role->add_cap( 'edit_published_posts' ); //would allow the contributor to upload files
}
add_action( 'admin_init', 'contri_up');

//***********************************************
//Force contributors updated post to be re reviewed
//***********************************************/
add_filter('wp_insert_post_data','re_aprove', '99', 2);
function re_aprove($data , $postarr){

    //check if current user is not admin
    if (!current_user_can('promote_users') && $postarr['post_type'] == "plugin" ){
        if ($data['post_status'] = "publish"){
            $data['post_status'] = "pending";
        }
    }
    return $data;
}

//****************************************************
// Add extra field to category page for category icon
//****************************************************/

/*
* configure taxonomy custom fields
*/
$config = array(
    'id' => 'icon_meta_box',							// meta box id, unique per meta box
    'title' => 'Add an Icon to your category',					   // meta box title
    'pages' => array('packages'),					// taxonomy name, accept categories, post_tag and custom taxonomies
    'context' => 'normal',							// where the meta box appear: normal (default), advanced, side; optional
    'fields' => array(),								// list of meta fields (can be added by field arrays)
    'local_images' => true,						   // Use local or hosted images (meta box images for add/remove)
    'use_with_theme' => true						   //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
);

/*
* Initiate your taxonomy custom fields
*/
$my_meta = new Tax_Meta_Class($config);

/*
* Add fields
*/

//text field
$my_meta->addImage('image_field_id',array('name'=> 'Taxonomy Icon '));
//textarea field

/*
* Don't Forget to Close up the meta box deceleration
*/
//Finish Taxonomy Extra fields Deceleration
$my_meta->Finish();

//display new column for image on columns page
function categoriesColumnsHeader($columns) {
    $columns['catImage'] = __('Category Icon');
    return $columns;
}
add_filter( 'manage_edit-packages_columns', 'categoriesColumnsHeader' );

function manage_category_custom_fields($deprecated,$column_name,$term_id)
{
    if ($column_name == 'catImage') {
        $saved_data = get_tax_meta($term_id,'image_field_id');
        echo '<img src="'. $saved_data['src']. '">';
    }
}
add_filter('manage_packages_custom_column','manage_category_custom_fields',10,3);
//*****************************************************
// End cusom taxonomy category icon
//*****************************************************/


//***************************************************
// HIDE METABOXES THAT CONTRIBUTORS SHOULD NOT SEE
//***************************************************/
if (is_admin()) {

    function my_remove_meta_boxes() {
        if (!current_user_can('administrator')) {
            remove_meta_box('postexcerpt', 'plugin', 'normal');
            remove_meta_box('trackbacksdiv', 'plugin', 'normal');
            remove_meta_box('commentstatusdiv', 'plugin', 'normal');
            remove_meta_box('postcustom', 'plugin', 'normal');
            remove_meta_box('commentstatusdiv', 'plugin', 'normal');
            remove_meta_box('commentsdiv', 'plugin', 'normal');
            remove_meta_box('revisionsdiv', 'plugin', 'normal');
            remove_meta_box('authordiv', 'plugin', 'normal');
            remove_meta_box('sqpt-meta-tags', 'plugin', 'normal');
            //  remove_meta_box( 'toolsdiv', 'plugin', 'side' );
            //  remove_meta_box( 'pluginsdiv', 'plugin', 'side' );
            remove_meta_box( 'packagesdiv', 'plugin', 'side' );
            remove_meta_box( 'pageparentdiv', 'plugin', 'side' );
            remove_meta_box( 'postimagediv', 'plugin', 'side' );
            //remove_meta_box( 'featured-homepage-addon', 'plugin', 'normal' );
        }
    }
    add_action( 'admin_menu', 'my_remove_meta_boxes' );

}


//Admin section welcome page
function register_admin_welcome() {
    add_menu_page('Welcome to the XNAT Marketplace Admin Dashboard', 'Welcome', 'edit_posts', 'xnat-welcome', 'xnat_admin_welcome_page', null, 0);
}
add_action('admin_menu', 'register_admin_welcome');
function xnat_admin_welcome_page() { ?>
    <div class="wrap">
        <div id="icon-index" class="icon32"><br></div>
        <h2>Welcome to the XNAT Marketplace Admin Dashboard</h2>
        <p><img src="<?php site_url(); ?>/wp-content/themes/twentyeleven/images/xnat_logo.png" alt="XNAT Marketplace"></p>
        <p>Welcome to the XNAT Marketplace Admin Dashboard!<p>
        <p><strong>To edit or delete an add-on</strong>, or to see its published status, select "Add-Ons" from the left menu.</p>
        <p><strong>To edit your profile information</strong>, select "Your Profile" from the left menu.</p>
        <p><br>Thank you for contributing to the XNAT community!</p>
    </div>
<?php }


//disable password nag for auto-generated passwords
function remove_default_password_nag() {
    global $user_ID;
    delete_user_setting('default_password_nag', $user_ID);
    update_user_option($user_ID, 'default_password_nag', false, true);
}
add_action('admin_init', 'remove_default_password_nag');


//Hide fields for website, AIM, Yahoo IM, jabber, avatar from normal users
function my_remove_contactmethods($contactmethods) {
    unset($contactmethods['aim']);
    unset($contactmethods['jabber']);
    unset($contactmethods['yim']);
    return $contactmethods;
}
add_filter('user_contactmethods','my_remove_contactmethods',10,1);


//Disable rich text editor for plugins
add_filter( 'user_can_richedit', 'disable_for_cpt' );
function disable_for_cpt( $default ) {
    global $post;
    if ( 'plugin' == get_post_type( $post ) )
        return false;
    return $default;
}

function myposttype_admin_css($hook_suffix) {
    global $typenow; if ($typenow == "plugin") { ?>
        <script>
            jQuery(document).ready(function($) {
                $('#wp-content-editor-tools').replaceWith('<p><strong>Please provide a description for this Add-On:</strong></p>');
            });
        </script>
    <?php }
}
add_action('admin_head', 'myposttype_admin_css');

//allow contributors to upload files
if (current_user_can('contributor') && !current_user_can('upload_files'))
    add_action('init', 'allow_contributor_uploads');
function allow_contributor_uploads() {
    $contributor = get_role('contributor');
    $contributor->add_cap('upload_files');
}

//properly make a username possessive (used in header.php)
function possessive($string) {
    return $string.'\''.($string[strlen($string) - 1] != 's' ? 's' : '');
}





