<?php 
/* Template Name: Homepage */
get_header();
wp_enqueue_script( 'cycle', get_template_directory_uri() . '/js/jquery.cycle.all.js', array( 'jquery')); ?>

<!-- Query the plugin post type -->
<!-- End Query for plugin => plugins -->
<div id="primary">			
    <!-- this is the left sidebar-->
    <?php include('sidebar-left.php'); ?>
    
	<div id="content" role="main">
    	<!-- spotlight slider area -->
        <div class="clearfix">
            <div id="slider">
                <a id="prev2" class="leftbtn" href="#"></a>
                <a id="next2" class="rightbtn" href="#"></a>
                <div id="slides-wrap">	
                    <ul id="slides">
                    <?php $slides = $data['primary-slider']; if (!empty($slides)): foreach ($slides as $slide): ?>
                        <li> 
                            <img src="<?php echo $slide['url'];?>" alt="<?php echo $slide['title'];?>"/>
                            <div id="slides-overlay">	
                                <h3 style="line-height:26px;margin: 5px 0px;"><a href="<?php echo $slide['link'];?>"><?php /*echo new_slide_length($slide['title'],17);*/ echo $slide['title']; ?></a></h3>
                                <p style="line-height:20px;"><?php echo new_slide_length($slide['description'],148); ?></p>
                                <?php if($slide['link'] !== '' ){} ?>

                            </div><!-- #slides-overlay -->
                        </li>
                    <?php endforeach; endif; ?>
                    </ul><!-- #slides -->
                </div><!-- #slides-wrap-->	
            </div><!-- #slider -->
            <div id="slider_shadow"></div>
		</div><!-- .clearfix -->
		<!-- this is the right sidebar-->

        <!-- this is the  featured plugins section -->
        <div class="clearfix">
            <div id="featured_plugins" class="featured_posts">	
                <h2>Featured Plugins </h2> 
                <div class="featured_container">
                    <?php
                         $plugins_query = new WP_Query( array(
                            'post_type' => 'plugin',
                            'posts_per_page' => 3,
                            'plugins' => 'admin,datatype,display,other,pipeline',
                            'post_child' => 0,	
                            'meta_key' => 'my_meta_box_select',
                            'meta_value' => 'featured',
                            'order' => 'DESC',
                            'orderby' => 'date'
                            
                        ) ); ?>		
                      
                     <!-- loop through posts for the plugins taxonomy -->	 
                        
                    <ul class="featured_postlist">
                        <?php if ( $plugins_query->have_posts() ): while ( $plugins_query->have_posts() ) : $plugins_query->the_post(); ?>
                        <li>
                            <div class="featured_post_wrap">
                                <div id="featured-image" style="float:left;width:70px;">
                                    <?php
                                    $icon = get_post_meta($post->ID, "upload_5", true);
                                    $defaulticon = get_post_meta($post->ID, "defaulticon", true); ?>
                                    
                                    <img class="icons" src="<?php if($icon !==''){ echo $icon;} elseif ($defaulticon !== ''){ echo $defaulticon;} ?>" /> 			
                                    <div id="compatible-with">
                                        Works with: XNAT <?php if( get_post_meta($post->ID, "compatible", true) ): echo str_replace(',', ', ', get_post_meta($post->ID, "compatible", true)); endif; ?>
                                    </div><!-- #compatible-with -->
                                </div><!-- #featured_image -->	
                                <div id="featured-content" style="padding-left:75px;">
                                    <a class="featured_title" href="<?php the_permalink(); ?>" rel="bookmark"><?php $title = get_the_title(); echo mb_strimwidth($title, 0, 30, '...'); ?><?php if (has_tag(array('nrg','Nrg','NRG'))) {echo '<img src="'.get_template_directory_uri().'/images/approved.png">';}?></a>
                                    <p><?php $excerpt = get_the_excerpt(); echo mb_strimwidth($excerpt, 0, 25, '...'); ?></p>
                                    <p class="author">
                                        Uploaded By:
                                        <span><?php the_author_posts_link(); ?></span>
                                    </p><!-- .author -->
                                    <p class="category">
                                        Category: 
                                        <span><?php echo get_the_term_list($post->ID, 'plugins', '', ', ', ''); ?></span>
                                    </p><!-- .category -->
                                    <p class="tags">
                                        Tags:
                                        <span><a href="#"><?php if( the_tags() ): echo  the_tags('', ', ', '<br />'); endif; ?></a></span>
                                    </p><!-- .tags -->
                                    <p class="date">
                                        Upload Date:
                                        <span><?php the_time(get_option('date_format')); ?></span>
                                    </p><!-- .date -->
                                </div><!-- #featured_content -->
                            </div><!-- .featured_post_wrap -->
                        </li>
                        <?php endwhile; endif; wp_reset_postdata(); ?>
                    </ul><!-- .featured_postlist -->
                    <div class="featured_foot">
                        <p><a href="<?php bloginfo('url'); ?>/?page_id=298">View all Plugins</a></p>
                    </div><!-- .featured_foot -->
                </div><!-- .featured_container -->
            </div><!-- #featured_plugins -->
        
            <!-- this is the  featured tools section -->
            <div id="featured_tools" class="featured_posts">
                <h2>Featured Tools</h2> 
                <div class="featured_container">
                <!-- Query the tools post type -->
                 <?php $tools_query = new WP_Query( array(
                            'post_type' => 'plugin',
                            'posts_per_page' => 3,
                            'tools' => 'apps,integration,other,scripts',
                            'post_child' => 0,
                            'meta_key' => 'my_meta_box_select',
                            'meta_value' => 'featured',	
                            'order' => 'DESC',
                            'orderby' => 'date'
                            
                    ));
                 ?>
                    <!-- loop through posts for the plugins taxonomy -->	 
                    <ul class="featured_postlist">
                        <?php if ( $tools_query->have_posts() ): while ( $tools_query->have_posts() ) : $tools_query->the_post(); ?>
                        <li>
                            <div class="featured_post_wrap">
                                <div id="featured-image" style="float:left;width:70px;">
                                    <?php
                                    $icon = get_post_meta($post->ID, "upload_5", true);
                                    $defaulticon = get_post_meta($post->ID, "defaulticon", true); ?>
                                    
                                    <img class="icons" src="<?php if($icon !==''){ echo $icon;} elseif ($defaulticon !== ''){ echo $defaulticon;} ?>" /> 			
                                    <div id="compatible-with">
                                        Works with: XNAT <?php if( get_post_meta($post->ID, "compatible", true) ): echo str_replace(',', ', ', get_post_meta($post->ID, "compatible", true)); endif; ?>
                                    </div><!-- #compatible-with -->
                                </div><!-- #featured_image -->	
                                <div id="featured-content" style="padding-left:75px;">
                                    <a class="featured_title" href="<?php the_permalink(); ?>" rel="bookmark"><?php $title = get_the_title(); echo mb_strimwidth($title, 0, 30, '...'); ?><?php if (has_tag(array('nrg','Nrg','NRG'))) {echo '<img src="'.get_template_directory_uri().'/images/approved.png">';}?></a>
                                    <p><?php $excerpt = get_the_excerpt(); echo mb_strimwidth($excerpt, 0, 35, '...'); ?></p>
                                    <p class="author">
                                        Uploaded By:
                                        <span><?php the_author_posts_link(); ?></span>
                                    </p><!-- .author -->
                                    <p class="category">
                                        Category: 
                                        <span><?php echo get_the_term_list($post->ID, 'tools', '', ', ', ''); ?></span>
                                    </p><!-- .category -->
                                    <p class="tags">
                                        Tags:
                                        <span><a href="#"><?php if( the_tags() ): echo  the_tags('', ', ', '<br />'); endif; ?></a></span>
                                    </p><!-- .tags -->
                                    <p class="date">
                                        Upload Date:
                                        <span><?php the_time(get_option('date_format')); ?></span>
                                    </p><!-- .date -->
                                </div><!-- #featured_content -->
                            </div><!-- .featured_post_wrap -->
                        </li>
                        <?php endwhile; endif; wp_reset_postdata(); ?>
                    </ul><!--// end .featured_postlist -->
                    <div class="featured_foot">
                        <p><a href="<?php bloginfo('url'); ?>/?page_id=302">View all Tools</a></p>
                    </div><!-- .featured_foot -->
                </div><!-- .featured_container -->
            </div> <!-- #featured_tools --> 
        </div><!-- .clearfix -->
   
        <div id="tag-cloud">
            <h3>Tag Cloud</h3>
            <div id="tag-cloud-wrap">
                <div>
                    <?php wp_tag_cloud( array( 'taxonomy' => array('post_tag'), 'format' => '' ) ); ?>
            	</div>
            </div><!-- #tag-cloud-wrap -->
        </div><!-- #tag-cloud -->
    </div><!-- #content -->
<?php include('sidebar-right.php'); ?>
</div><!-- #primary -->

<?php get_footer(); ?>