<?php
/**
 * The default template for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>
<?php 
// disabled because file path redirects cause the file size function to fail
/* 
$filesize = get_post_meta($post->ID, "upload_6", true); 
$kbsize = strlen(file_get_contents($filesize)); 
$truesize = plugin_filesize($post->ID); 
*/
?>
<?php if( has_term( 'nrg-approved','packages')) { $has_term = 'nrg-approved';} else {$has_term = "not-nrg";} ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class($has_term); ?>>	
		<?php if ( is_search() || is_author() || is_tag()) : // Only display Excerpts for Search ?>	
		<div class="search-divider"></div>
	<?php endif;?>
	
		<header class="entry-header">
			<?php if ( is_sticky() ) : ?>
				<hgroup>
					
					<h3 class="entry-format"><?php _e( 'Featured', 'twentyeleven' ); ?></h3>
				</hgroup>
			<?php else : ?>
			
			<?php endif; ?>

			<?php if ( 'post' == get_post_type()  ) : ?>
			<div class="entry-meta">
				<?php twentyeleven_posted_on(); ?>
			</div><!-- .entry-meta -->
			<?php endif; ?>

			<?php if ( comments_open() && ! post_password_required() ) : ?>
			<div class="comments-link">
				<?php comments_popup_link( '<span class="leave-reply">' . __( 'Reply', 'twentyeleven' ) . '</span>', _x( '1', 'comments number', 'twentyeleven' ), _x( '%', 'comments number', 'twentyeleven' ) ); ?>
			</div>
			<?php endif; ?>
		</header><!-- .entry-header -->
           
		<?php if ( is_search() || is_author() || is_page('254') || is_tag() ) : // Only display Excerpts for Search ?>		
		<div class="entry-summary">
		
		   <ul class="clearfix"><li> 
			<div class="featured_post_wrap">
				  <div class="featured-image">
			     	<?php
                    $icon = get_post_meta($post->ID, "upload_5", true);
					$defaulticon = get_post_meta($post->ID, "defaulticon", true);
                    $avatar = ($icon !== '')? $icon : $defaulticon;
                    $avatar = str_replace('http:','https:',$avatar);
                    ?>
			       <img class="icons" src="<?php echo $avatar ?>">
			      </div><!-- #author-avatar -->	
				   <div class="featured-content">
					  <a class="featured_title" href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?> <?php if( has_term( 'nrg-approved','packages')) {echo '<img src="'.get_template_directory_uri().'/images/approved.png">';}?></a>
					  <?php print_excerpt(140); ?>
					  <div class="featured-content-left">
					  <?php if(!is_author()){?>
					  <p class="author">Uploaded By: <span>   <?php the_author(); ?></span></p><?php } ?>
					  <p class="category">Category:
                        <span>
                        <?php 
                        $thecattool = get_post_meta($post->ID, "tax", true);
                        $toolterms = get_the_terms($post->ID, 'tools');
                        $pluginterms = get_the_terms($post->ID, 'plugins');
                        
                        if ($toolterms != false) {
                            echo get_the_term_list($post->ID, 'tools', '', ', ', '');
                        } else if ($pluginterms != false) {
                            echo get_the_term_list($post->ID, 'plugins', '', ', ', '');
                        }
                        ?>
                        </span> 
                       </p>
					   <p class="tags">Tags:<span> <?php $posttags = the_tags('', ', ', '<br />');
						$counttag=0;
						if ($posttags) {
							foreach($posttags as $tag) {
								$counttag++;
								echo $tag->name .', ';
								if( $counttag >3 ) break;
							}  }?></span></p>		
					  <p class="date">Upload date: <span><?php the_time(get_option('date_format')); ?></span></p>
					  </div>
					  <div class="featured-content-right">
						  <p class="downloadwrap">
							  <?php if( get_post_meta($post->ID, "download_from_repo", true) == "yes") : ?>
								  <a class="download" href="<?php echo get_post_meta($post->ID,"repo",true) ?>">Download From Repo</a>
							  <?php else : ?>
                                  <?php
                                  if( get_post_meta($post->ID, "upload_6", true) ):
                                      $url = str_replace('http:','https:',get_post_meta($post->ID, "upload_6", true))
                                  ?>
								  <a class="download" href="<?php echo $url ?>">Download Now</a>
                                  <?php
                                  endif;
                                  ?>
							  <?php endif; ?>
						  </p>
					  </div>
				  </div>
               </div>			   
		</div><!-- .entry-summary -->
		  </li></ul>
		<?php else : ?>
		<div class="entry-content">
			<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentyeleven' ) ); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>
		</div><!-- .entry-content -->
		<?php endif; ?>

		<footer class="entry-meta">
			<?php $show_sep = false; ?>
			<?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search ?>
			<?php
				/* translators: used between list items, there is a space after the comma */
				$categories_list = get_the_category_list( __( ', ', 'twentyeleven' ) );
				if ( $categories_list ):
			?>
			<span class="cat-links">
				<?php printf( __( '<span class="%1$s">Posted in</span> %2$s', 'twentyeleven' ), 'entry-utility-prep entry-utility-prep-cat-links', $categories_list );
				$show_sep = true; ?>
			</span>
			<?php endif; // End if categories ?>
			<?php
				/* translators: used between list items, there is a space after the comma */
				$tags_list = get_the_tag_list( '', __( ', ', 'twentyeleven' ) );
				if ( $tags_list ):
				if ( $show_sep ) : ?>
			<span class="sep"> | </span>
				<?php endif; // End if $show_sep ?>
			<span class="tag-links">
				<?php printf( __( '<span class="%1$s">Tagged</span> %2$s', 'twentyeleven' ), 'entry-utility-prep entry-utility-prep-tag-links', $tags_list );
				$show_sep = true; ?>
			</span>
			<?php endif; // End if $tags_list ?>
			<?php endif; // End if 'post' == get_post_type() ?>

			<?php if ( comments_open() ) : ?>
			<?php if ( $show_sep ) : ?>
			<span class="sep"> | </span>
			<?php endif; // End if $show_sep ?>
			<span class="comments-link"><?php comments_popup_link( '<span class="leave-reply">' . __( 'Leave a reply', 'twentyeleven' ) . '</span>', __( '<b>1</b> Reply', 'twentyeleven' ), __( '<b>%</b> Replies', 'twentyeleven' ) ); ?></span>
			<?php endif; // End if comments_open() ?>
			
		</footer><!-- #entry-meta -->
	</article><!-- #post-<?php the_ID(); ?> -->
