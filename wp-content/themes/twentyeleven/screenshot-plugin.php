		<!-- Add-On Screenshots -->
		
	<div id="addon_shots">
	<h1 class="title page-title single-plugin-title">Add-on Screenshots</h1>
		<ul>			
		<?php if( get_post_meta($post->ID, "upload_1", true) ):?>
            <?php $url = str_replace('http:','https:',get_post_meta($post->ID, "upload_1", true)); ?>
            <li>
                <div class="mosaic-block circle">
                <a href="<?php echo $url ?>" class="mosaic-overlay inline" rel="group2">&nbsp;</a>
                <div class="mosaic-backdrop">
                    <img class="lightbox"  src="<?php echo $url ?>">
                </div>
                </div>
            </li>
		<?php endif; ?>
		<?php if( get_post_meta($post->ID, "upload_2", true) ): ?>
            <?php $url = str_replace('http:','https:',get_post_meta($post->ID, "upload_2", true)); ?>
            <li>
                <div class="mosaic-block circle">
                <a href="<?php echo $url ?>" class="mosaic-overlay inline" rel="group2">&nbsp;</a>
                <div class="mosaic-backdrop">
                    <img class="lightbox"  src="<?php echo $url ?>">			</div>
                </div>
            </li>
		<?php endif; ?>
		<?php if( get_post_meta($post->ID, "upload_3", true) ):?>
            <?php $url = str_replace('http:','https:',get_post_meta($post->ID, "upload_3", true)); ?>
            <li>
                <div class="mosaic-block circle">
                <a href="<?php echo $url ?>" class="mosaic-overlay inline" rel="group2">&nbsp;</a>
                <div class="mosaic-backdrop">
                    <img class="lightbox"  src="<?php echo $url ?>">			</div>
                </div>
            </li>
			<?php endif; ?>
			 
		</ul>

		<?php //echo my_the_screenshot_filter(); ?>
	</div>	